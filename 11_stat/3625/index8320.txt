Les forces

Modéliser les actions mécaniques
Actions réciproques
Propriété
Interactions
Lorsque deux objets A et B sont en interaction, il
existe toujours deux actions mécaniques : A agit
sur B et B agit sur A.

NOTION Actions-réciproques
Les forces

Modéliser les actions mécaniques
Modélisation des actions
Propriété
On modélise les actions par des forces
Pour simpliﬁer les études des actions, on les
modélise par des forces possédant quatre
caractéristiques :
• un point d’application ;
• une direction ;
• un sens ;
• une

valeur

(aussi

appelée

intensité)

exprimée en newton (N ).
Propriété
Les forces sont représentées par des ﬂèches
La ﬂèche commence au point d’application de
la force, suit la direction et le sens d’action de
cette force et sa longueur est proportionnelle à
l’intensité de la force.

NOTION Modélisation-des-actions
Les forces

Les forces s’exerçant sur un système
Mesurer l’intensité d’une force
Propriété
On mesure l’intensité des forces avec un
dynamomètre
Les dynamomètres sont composés d’un ressort
dont l’allongement est proportionnel à l’intensité
de la force exercée sur lui.

NOTION Mesurer-l’intensité-d’une-force
Les forces

Les forces s’exerçant sur un système
Les forces négligeables
Propriété
Des forces peuvent être négligeables par
rapport à d’autres
Dans le cas d’un système soumis à plusieurs
forces, si l’intensité d’une force est très faible
devant celle des autres, on peut considérer que
cette force n’a pas d’effet sur le système : elle est
négligeable.

NOTION Les-forces-négligeables
Les forces

Les forces s’exerçant sur un système
Systèmes en équilibre
Propriété
Un système est en équilibre si l’ensemble des
forces auxquelles il est soumis se compensent
Des

forces

s’annulent.

se

compensent

si

leurs

effets

NOTION Systèmes-en-équilibre
Les forces

L’essent

Tracer un DOI permet de faire l’inventaire des
interactions en jeu.
Modéliser les actions par des forces permet d’analyser
la situation pour prévoir le mouvement d’un objet.

SCHEMA Lessent
Les forces

Modéliser les forces
Une valise est en équilibre à côté de son propriétaire. La
force exercée par la valise sur le sol est de 120 N .
On va représenter les forces qui agissent sur la valise.

Etape 1 : Trouver les interactions
• La valise étant en équilibre, il y a une interaction.
• Ici seul le sol est en interaction avec la valise.
– C’est une action de contact.
• Les deux intensités des forces sont donc égales.

Etape 2 : Se rappeler les caractéristiques
d’une force
• On modélise une force avec quatre caractéristiques
:
– un point d’application ;
– une direction ;
– un sens ;
– une valeur (aussi appelée intensité) exprimée
en newton (N ).
• On modélise donc le poids par une ﬂèche.

Etape 3 : Associer les caractéristiques
• Action de la valise sur le sol :
– point d’application :

le centre de gravité

(approximativement le centre de la valise)
– direction : verticale
– sens : vers le bas
– intensité : 120 N
• Action du sol sur la valise :
– point d’application :

le point de contact

(centre de la zone de contact)
– direction : verticale
– sens : vers le haut
– intensité : 120 N

Etape 4 :

Associer une échelle pour

l’intensité
• Pour que le schéma soit complet, il faut lui
adjoindre une échelle qui permet de faire le lien
entre la longueur des ﬂèches et l’intensité des
forces.
• On peut choisir 50 N = 1 cm.
• Ce qui fait une longueur de 6 cm.

Etape 5 : Dessiner
En reprenant toutes les informations précédentes, on
peut tracer les forces sur un schéma.

DEMO Modéliser-les-forces
