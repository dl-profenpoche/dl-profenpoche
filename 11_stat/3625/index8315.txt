De l’Univers aux atomes

L’origine de la matière dans l’Univers
L’origine des atomes
Propriété
Les atomes les plus légers ont été formés au
début de notre Univers
Les premiers atomes, les plus légers (hydrogène
et hélium), sont apparus dans les premiers temps
qui ont suivi le Big Bang, il y a environ 14 milliards
d’années.
Propriété
Les autres atomes ont été formés au cœur des
étoiles
Les conditions extrêmes qui ont lieu au cœur
des étoiles ont permis de former les atomes plus
lourds par des réactions de fusion nucléaire.

NOTION L’origine-des-atomes
De l’Univers aux atomes

L’origine de la matière dans l’Univers
Les molécules
Propriété
Les molécules sont des assemblages d’atomes
Lorsqu’ils se lient,

les atomes forment des

molécules, que l’on note à l’aide d’une formule
chimique (ex. : dioxygène O2 , eau H2 O, etc.)

NOTION Les-molécules
De l’Univers aux atomes

Les charges électriques dans l’atome
La charge électrique
Déﬁnition
La charge électrique est une propriété de la
matière
La charge électrique peut être soit positive
soit négative.

Les charges de mêmes signes

se repoussent alors que les charges de signes
opposés s’attirent.
Propriété
Des charges opposées s’annulent
Les effets de deux charges opposées s’annulent.

NOTION La-charge-électrique
De l’Univers aux atomes

Les charges électriques dans l’atome
La charge électrique dans les atomes
Propriété
On

trouve

des

particules

chargées

électriquement dans les atomes
Les

particules

chargées

appelées électrons.

négativement

sont

Les particules chargées

positivement sont appelées protons.
Propriété
L’atome est électriquement neutre
Il est composé de charges électriques opposées
qui s’annulent.

NOTION La-charge-électrique-dans-les-atomes
De l’Univers aux atomes

La structure des atomes
Noyau et électrons
Propriété
Les

atomes

sont

électriquement

composés

positif

et

d’un

noyau

d’électrons

électriquement négatifs
Les électrons sont en mouvement rapide autour
du noyau, on dit qu’ils « gravitent » autour du
noyau.

NOTION Noyau-et-électrons
De l’Univers aux atomes

La structure des atomes
Les particules du noyau
Propriété
Le noyau est composé de neutrons et de
protons
Les protons possèdent une charge électrique
positive.
Dans un atome il y a autant d’électrons que de
protons.
Les neutrons ne possèdent pas de charge
électrique (ils sont électriquement neutres) :
ils servent à « cimenter » les protons qui se
repoussent entre eux.
Déﬁnition
Élément
Les atomes possédant le même nombre de
protons au sein de leur noyau font partie du
même élément.
Déﬁnition
Numéro atomique
Le numéro atomique Z, marqué dans le tableau
périodique des éléments, correspond au nombre
de protons présents dans le noyau des atomes
d’un élément.

NOTION Les-particules-du-noyau
De l’Univers aux atomes

L’essentiel en schéma

Toute la matière présente dans l’Univers provient du
Big Bang. Les différents atomes sont formés des trois
mêmes constituants : les protons, les neutrons et les
électrons.

SCHEMA Lessentiel-en-schéma
