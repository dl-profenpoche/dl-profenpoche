Résistance et loi d’Ohm

Effets d’une résistance dans un
circuit électrique
Effets sur l’intensité
Propriété
La résistance électrique
La résistance électrique d’un dipôle indique sa
capacité à s’opposer au passage du courant
électrique en limitant son intensité.

Plus la

résistance totale d’un dipôle est grande, plus
l’intensité traversant ce dipôle sera faible.
Remarque
Un matériau de résistance très faible est un bon
conducteur.
Un bon isolant possède,
résistance très élevée.

au contraire,

une

NOTION Effets-sur-l’intensité
Résistance et loi d’Ohm

Effets d’une résistance dans un
circuit électrique
Effet Joule
Propriété
Tous les dipôles traversés par un courant
subissent l’effet Joule
L’effet Joule est la conversion d’une partie de
l’énergie électrique transmise au dipôle en
énergie thermique.
Propriété
L’effet Joule peut être souhaité dans certains
cas et être un problème dans d’autres
Dans

le

cas

des

appareils

de

chauffage,

(grille-pain, radiateur électrique, etc.)

l’effet

Joule est souhaité.
Dans les circuits électroniques, l’effet Joule peut
endommager les constituants du circuit.

NOTION Effet-Joule
Résistance et loi d’Ohm

Résistance et loi d’Ohm
Mesurer une résistance
Propriété
La résistance d’un dipôle se mesure avec un
ohmmètre
L’unité de mesure de la résistance est l’ohm (Ω).
On utilise également des multiples de l’ohm : le
kiloohm (kΩ = 103 Ω), le mégaohm (M Ω = 106 Ω),
etc.

NOTION Mesurer-une-résistance
Résistance et loi d’Ohm

Résistance et loi d’Ohm
La loi d’Ohm
U =R×I
La loi d’Ohm montre la proportionnalité entre la
tension U en volt (V ) aux bornes d’un résistor et
l’intensité I en ampère (A) traversant ce résistor de
résistance R en ohm (Ω).
Propriété
Un dipôle obéissant à la loi d’Ohm est appelé
dipôle ohmique
On peut vériﬁer si un dipôle est un dipôle
ohmique en traçant la tension à ses bornes en
fonction de l’intensité qui le traverse. Si la courbe
obtenue est une fonction linéaire (droite qui passe
par l’origine), c’est que le dipôle est un dipôle
ohmique.

NOTION La-loi-d’Ohm
Résistance et loi d’Ohm

L’essentiel en schéma

Les résistors sont des dipôles dont la forte résistance
électrique permet de diminuer l’intensité du courant au
sein d’un circuit. Le passage du courant au sein d’un
dipôle possèdant une résistance électrique provoque un
échauffement de ce dipôle.

SCHEMA Lessentiel-en-schéma
Résistance et loi d’Ohm

Exploiter l’expression de la
résistance
Sur un chargeur de téléphone est indiqué 6 V et 1200 mA.
On va calculer la résistance du chargeur.

Etape 1 : Comprendre les données
• 1200 mA correspond à l’intensité I passant par le
chargeur.
• 6 V correspond à la tension U aux bornes du
chargeur.
• L’expression de la loi d’Ohm est : U = R × I.
• On cherche R.

Etape 2 : Vériﬁer les unités et le convertir si
besoin
• L’intensité doit être en ampère, ce qui n’est pas le
cas : I = 1200 mA = 1, 2 A.
• La tension doit être en volt, ce qui est le cas : U =
6V.
• La résistance est en ohm Ω.

Etape 3 : Jongler avec l’expression
• U =R×I
– d’où

U
I

=

– et R =

U
I.

R×I
I

Etape 4 : Faire l’application numérique
• R=

U
I

• R=

6
1,2

=5Ω

• Le chargeur de téléphone a une résistance de 5 Ω.

DEMO Exploiter-l’expression-de-la-résistance
