Vocabulaire - Le quotidien

Les actions quotidiennes
La routine : le matin
Despertarse (ie)

Se réveiller

Levantarse

Se lever

Vestirse (i)

S’habiller

Desayunar

Prendre le petit-déjeuner

Ducharse

Se doucher

Ir al colegio

Aller au collège

Exemple
Todos los días, me despierto, me levanto, me
ducho y desayuno, para ir al colegio.

NOTION La-routine-le-matin
Vocabulaire - Le quotidien

Les actions quotidiennes
La routine : l’après-midi et la soirée
Almorzar (ue)

Déjeuner

Ir al recreo

Aller en récréation

Regresar a casa

Rentrer à la maison

Merendar (ie)

Gouter

Jugar (ue)

Jouer

Estudiar

Étudier

Hacer los deberes

Faire ses devoirs

Ver la tele

Regarder la télévision

Cenar

Diner

Acostarse (ue)

Se coucher

Exemple
Un padre y su hijo
Padre: ¡Hola Juanito! ¿Qué tal el día?
Juan: Muy bien, papá. Esta tarde, he almorzado
en el comedor estudiantil, luego he estudiado en
clase. He podido disfrutar del recreo y al acabar
las clases, he regresado a casa para merendar.
Después, he jugado un poco antes de hacer los
deberes y de ver un poco la tele.
Padre: ¡Estupendo!

NOTION La-routine-l’après-midi-et-la-soirée
Vocabulaire - Le quotidien

L’expression de l’heure et les
moments de la journée
Les différents moments de la journée
El amanecer

Le lever du jour

La mañana

Le matin

La tarde

L’après-midi

El atardecer

La tombée du jour

La noche

Le soir, la nuit

Exemple
Cuando

me

levanto

por

la

mañana,

la

primera cosa que hago es desayunar pan con
mantequilla.
Remarque
Pour indiquer le temps (une période), on utilise la
préposition por .
• Ex. : Vamos al cine por la tarde.

NOTION Les-différents-moments-de-la-journée
Vocabulaire - Le quotidien

L’expression de l’heure et les
moments de la journée
L’expression de l’heure
Un reloj

Une montre

Una hora

Une heure

¿Qué hora es?

Quelle heure est-il ?

Son las dos y cuarto.

Il est deux heures et quart.

Son las dos en punto.

Il est deux heures pile.

Son las dos menos diez.

Il est deux heures moins dix.

Son las dos y cinco.

Il est deux heures cinq.

Son las dos y media.

Il est deux heures et demie.

Es la una.

Il est une heure.

Mediodía

Midi

Medianoche

Minuit

Exemple
En la estación
Viajero: Buenos días, quisiera ir a Madrid por
favor, este lunes.
Taquillero: Buenos días. ¿Por la mañana o por la
tarde?
Viajero: Por la mañana.
Taquillero: El primer tren sale a las cinco y cuarto.
Viajero: Bueno, es un poco temprano… ¿Y el
siguiente?
Taquillero: Después hay otro a las siete menos
diez y el siguiente es a las ocho en punto.
Remarque
Pour exprimer l’heure, on utilise toujours le verbe
ser. On le conjugue à la 3e personne du pluriel
(ex. : Son las tres), sauf pour dire « Il est une heure
» (Es la una).

NOTION L’expression-de-l’heure
Vocabulaire - Le quotidien

Le calendrier
Les jours de la semaine
Lunes

Lundi

Martes

Mardi

Miércoles

Mercredi

Jueves

Jeudi

Viernes

Vendredi

Sábado

Samedi

Domingo

Dimanche

El ﬁn de semana

Le week-end

Un día

Un jour

Los días de la semana

Les jours de la semaine

Exemple
De lunes a viernes, tengo clase. El ﬁn de semana
cae el sábado y domingo.

NOTION Les-jours-de-la-semaine
Vocabulaire - Le quotidien

Le calendrier
Les mois de l’année et les saisons
Un mes

Un mois

El año

L’année

Los meses del año

Les mois de l’année

Enero

Janvier

Febrero

Février

Marzo

Mars

Abril

Avril

Mayo

Mai

Junio

Juin

Julio

Juillet

Agosto

Aout

Septiembre

Septembre

Octubre

Octobre

Noviembre

Novembre

Diciembre

Décembre

Las estaciones

Les saisons

El verano

L’été

El otoño

L’automne

El invierno

L’hiver

La primavera

Le printemps

Exemple
Hoy, es sábado, once de noviembre de dos mil
diecisiete.
Exemple
Pour exprimer la date en espagnol, on utilise la
préposition de avant le mois et avant l’année.

NOTION Les-mois-de-l’année-et-les-saisons
Vocabulaire - Le quotidien

La météo
Les éléments naturels
El cielo

Le ciel

El sol

Le soleil

La nube

Le nuage

La lluvia

La pluie

La tormenta

L’orage

El viento

Le vent

La nieve

La neige

Exemple
Dos amigas
Ana: ¿Sabes? ¡En noviembre me voy de viaje a
Bolivia!
Carla: ¡Increíble! Pero ¿por qué en este mes? Hace
frío, y no hace buen tiempo…
Ana: No, en absoluto, ¡es lo contrario!

En el

otro hemisferio en noviembre, no llueve tanto y
el tiempo está soleado. Las temperaturas son las
más agradables.

NOTION Les-éléments-naturels
Vocabulaire - Le quotidien

La météo
Pour parler du temps
Llover (ue)

Pleuvoir

Nevar (ie)

Neiger

Estar nublado

Être nuageux

Estar soleado

Être ensoleillé

Hacer buen tiempo

Faire beau

Hacer frío

Faire froid

Hacer calor

Faire chaud

Exemple
Hoy hace frío pero seguro que mañana va a hacer
calor.

NOTION Pour-parler-du-temps
Vocabulaire - Le quotidien

Les actions quotidiennes
La routine : le matin
Despertarse (ie)

Se réveiller

Levantarse

Se lever

Vestirse (i)

S’habiller

Desayunar

Prendre le petit-déjeuner

Ducharse

Se doucher

Ir al colegio

Aller au collège

Exemple
Todos los días, me despierto, me levanto, me
ducho y desayuno, para ir al colegio.

NOTION La-routine-le-matin
Vocabulaire - Le quotidien

Les actions quotidiennes
La routine : l’après-midi et la soirée
Almorzar (ue)

Déjeuner

Ir al recreo

Aller en récréation

Regresar a casa

Rentrer à la maison

Merendar (ie)

Gouter

Jugar (ue)

Jouer

Estudiar

Étudier

Hacer los deberes

Faire ses devoirs

Ver la tele

Regarder la télévision

Cenar

Diner

Acostarse (ue)

Se coucher

Exemple
Un padre y su hijo
Padre: ¡Hola Juanito! ¿Qué tal el día?
Juan: Muy bien, papá. Esta tarde, he almorzado
en el comedor estudiantil, luego he estudiado en
clase. He podido disfrutar del recreo y al acabar
las clases, he regresado a casa para merendar.
Después, he jugado un poco antes de hacer los
deberes y de ver un poco la tele.
Padre: ¡Estupendo!

NOTION La-routine-l’après-midi-et-la-soirée
Vocabulaire - Le quotidien

L’expression de l’heure et les
moments de la journée
Les différents moments de la journée
El amanecer

Le lever du jour

La mañana

Le matin

La tarde

L’après-midi

El atardecer

La tombée du jour

La noche

Le soir, la nuit

Exemple
Cuando

me

levanto

por

la

mañana,

la

primera cosa que hago es desayunar pan con
mantequilla.
Remarque
Pour indiquer le temps (une période), on utilise la
préposition por .
• Ex. : Vamos al cine por la tarde.

NOTION Les-différents-moments-de-la-journée
Vocabulaire - Le quotidien

L’expression de l’heure et les
moments de la journée
L’expression de l’heure
Un reloj

Une montre

Una hora

Une heure

¿Qué hora es?

Quelle heure est-il ?

Son las dos y cuarto.

Il est deux heures et quart.

Son las dos en punto.

Il est deux heures pile.

Son las dos menos diez.

Il est deux heures moins dix.

Son las dos y cinco.

Il est deux heures cinq.

Son las dos y media.

Il est deux heures et demie.

Es la una.

Il est une heure.

Mediodía

Midi

Medianoche

Minuit

Exemple
En la estación
Viajero: Buenos días, quisiera ir a Madrid por
favor, este lunes.
Taquillero: Buenos días. ¿Por la mañana o por la
tarde?
Viajero: Por la mañana.
Taquillero: El primer tren sale a las cinco y cuarto.
Viajero: Bueno, es un poco temprano… ¿Y el
siguiente?
Taquillero: Después hay otro a las siete menos
diez y el siguiente es a las ocho en punto.
Remarque
Pour exprimer l’heure, on utilise toujours le verbe
ser. On le conjugue à la 3e personne du pluriel
(ex. : Son las tres), sauf pour dire « Il est une heure
» (Es la una).

NOTION L’expression-de-l’heure
Vocabulaire - Le quotidien

Le calendrier
Les jours de la semaine
Lunes

Lundi

Martes

Mardi

Miércoles

Mercredi

Jueves

Jeudi

Viernes

Vendredi

Sábado

Samedi

Domingo

Dimanche

El ﬁn de semana

Le week-end

Un día

Un jour

Los días de la semana

Les jours de la semaine

Exemple
De lunes a viernes, tengo clase. El ﬁn de semana
cae el sábado y domingo.

NOTION Les-jours-de-la-semaine
Vocabulaire - Le quotidien

Le calendrier
Les mois de l’année et les saisons
Un mes

Un mois

El año

L’année

Los meses del año

Les mois de l’année

Enero

Janvier

Febrero

Février

Marzo

Mars

Abril

Avril

Mayo

Mai

Junio

Juin

Julio

Juillet

Agosto

Aout

Septiembre

Septembre

Octubre

Octobre

Noviembre

Novembre

Diciembre

Décembre

Las estaciones

Les saisons

El verano

L’été

El otoño

L’automne

El invierno

L’hiver

La primavera

Le printemps

Exemple
Hoy, es sábado, once de noviembre de dos mil
diecisiete.
Exemple
Pour exprimer la date en espagnol, on utilise la
préposition de avant le mois et avant l’année.

NOTION Les-mois-de-l’année-et-les-saisons
Vocabulaire - Le quotidien

La météo
Les éléments naturels
El cielo

Le ciel

El sol

Le soleil

La nube

Le nuage

La lluvia

La pluie

La tormenta

L’orage

El viento

Le vent

La nieve

La neige

Exemple
Dos amigas
Ana: ¿Sabes? ¡En noviembre me voy de viaje a
Bolivia!
Carla: ¡Increíble! Pero ¿por qué en este mes? Hace
frío, y no hace buen tiempo…
Ana: No, en absoluto, ¡es lo contrario!

En el

otro hemisferio en noviembre, no llueve tanto y
el tiempo está soleado. Las temperaturas son las
más agradables.

NOTION Les-éléments-naturels
Vocabulaire - Le quotidien

La météo
Pour parler du temps
Llover (ue)

Pleuvoir

Nevar (ie)

Neiger

Estar nublado

Être nuageux

Estar soleado

Être ensoleillé

Hacer buen tiempo

Faire beau

Hacer frío

Faire froid

Hacer calor

Faire chaud

Exemple
Hoy hace frío pero seguro que mañana va a hacer
calor.

NOTION Pour-parler-du-temps
Vocabulaire - Le quotidien

Les actions quotidiennes
La routine : le matin
Despertarse (ie)

Se réveiller

Levantarse

Se lever

Vestirse (i)

S’habiller

Desayunar

Prendre le petit-déjeuner

Ducharse

Se doucher

Ir al colegio

Aller au collège

Exemple
Todos los días, me despierto, me levanto, me
ducho y desayuno, para ir al colegio.

NOTION La-routine-le-matin
Vocabulaire - Le quotidien

Les actions quotidiennes
La routine : l’après-midi et la soirée
Almorzar (ue)

Déjeuner

Ir al recreo

Aller en récréation

Regresar a casa

Rentrer à la maison

Merendar (ie)

Gouter

Jugar (ue)

Jouer

Estudiar

Étudier

Hacer los deberes

Faire ses devoirs

Ver la tele

Regarder la télévision

Cenar

Diner

Acostarse (ue)

Se coucher

Exemple
Un padre y su hijo
Padre: ¡Hola Juanito! ¿Qué tal el día?
Juan: Muy bien, papá. Esta tarde, he almorzado
en el comedor estudiantil, luego he estudiado en
clase. He podido disfrutar del recreo y al acabar
las clases, he regresado a casa para merendar.
Después, he jugado un poco antes de hacer los
deberes y de ver un poco la tele.
Padre: ¡Estupendo!

NOTION La-routine-l’après-midi-et-la-soirée
Vocabulaire - Le quotidien

L’expression de l’heure et les
moments de la journée
Les différents moments de la journée
El amanecer

Le lever du jour

La mañana

Le matin

La tarde

L’après-midi

El atardecer

La tombée du jour

La noche

Le soir, la nuit

Exemple
Cuando

me

levanto

por

la

mañana,

la

primera cosa que hago es desayunar pan con
mantequilla.
Remarque
Pour indiquer le temps (une période), on utilise la
préposition por .
• Ex. : Vamos al cine por la tarde.

NOTION Les-différents-moments-de-la-journée
Vocabulaire - Le quotidien

L’expression de l’heure et les
moments de la journée
L’expression de l’heure
Un reloj

Une montre

Una hora

Une heure

¿Qué hora es?

Quelle heure est-il ?

Son las dos y cuarto.

Il est deux heures et quart.

Son las dos en punto.

Il est deux heures pile.

Son las dos menos diez.

Il est deux heures moins dix.

Son las dos y cinco.

Il est deux heures cinq.

Son las dos y media.

Il est deux heures et demie.

Es la una.

Il est une heure.

Mediodía

Midi

Medianoche

Minuit

Exemple
En la estación
Viajero: Buenos días, quisiera ir a Madrid por
favor, este lunes.
Taquillero: Buenos días. ¿Por la mañana o por la
tarde?
Viajero: Por la mañana.
Taquillero: El primer tren sale a las cinco y cuarto.
Viajero: Bueno, es un poco temprano… ¿Y el
siguiente?
Taquillero: Después hay otro a las siete menos
diez y el siguiente es a las ocho en punto.
Remarque
Pour exprimer l’heure, on utilise toujours le verbe
ser. On le conjugue à la 3e personne du pluriel
(ex. : Son las tres), sauf pour dire « Il est une heure
» (Es la una).

NOTION L’expression-de-l’heure
Vocabulaire - Le quotidien

Le calendrier
Les jours de la semaine
Lunes

Lundi

Martes

Mardi

Miércoles

Mercredi

Jueves

Jeudi

Viernes

Vendredi

Sábado

Samedi

Domingo

Dimanche

El ﬁn de semana

Le week-end

Un día

Un jour

Los días de la semana

Les jours de la semaine

Exemple
De lunes a viernes, tengo clase. El ﬁn de semana
cae el sábado y domingo.

NOTION Les-jours-de-la-semaine
Vocabulaire - Le quotidien

Le calendrier
Les mois de l’année et les saisons
Un mes

Un mois

El año

L’année

Los meses del año

Les mois de l’année

Enero

Janvier

Febrero

Février

Marzo

Mars

Abril

Avril

Mayo

Mai

Junio

Juin

Julio

Juillet

Agosto

Aout

Septiembre

Septembre

Octubre

Octobre

Noviembre

Novembre

Diciembre

Décembre

Las estaciones

Les saisons

El verano

L’été

El otoño

L’automne

El invierno

L’hiver

La primavera

Le printemps

Exemple
Hoy, es sábado, once de noviembre de dos mil
diecisiete.
Exemple
Pour exprimer la date en espagnol, on utilise la
préposition de avant le mois et avant l’année.

NOTION Les-mois-de-l’année-et-les-saisons
Vocabulaire - Le quotidien

La météo
Les éléments naturels
El cielo

Le ciel

El sol

Le soleil

La nube

Le nuage

La lluvia

La pluie

La tormenta

L’orage

El viento

Le vent

La nieve

La neige

Exemple
Dos amigas
Ana: ¿Sabes? ¡En noviembre me voy de viaje a
Bolivia!
Carla: ¡Increíble! Pero ¿por qué en este mes? Hace
frío, y no hace buen tiempo…
Ana: No, en absoluto, ¡es lo contrario!

En el

otro hemisferio en noviembre, no llueve tanto y
el tiempo está soleado. Las temperaturas son las
más agradables.

NOTION Les-éléments-naturels
Vocabulaire - Le quotidien

La météo
Pour parler du temps
Llover (ue)

Pleuvoir

Nevar (ie)

Neiger

Estar nublado

Être nuageux

Estar soleado

Être ensoleillé

Hacer buen tiempo

Faire beau

Hacer frío

Faire froid

Hacer calor

Faire chaud

Exemple
Hoy hace frío pero seguro que mañana va a hacer
calor.

NOTION Pour-parler-du-temps
Vocabulaire - Le quotidien

Les actions quotidiennes
La routine : le matin
Despertarse (ie)

Se réveiller

Levantarse

Se lever

Vestirse (i)

S’habiller

Desayunar

Prendre le petit-déjeuner

Ducharse

Se doucher

Ir al colegio

Aller au collège

Exemple
Todos los días, me despierto, me levanto, me
ducho y desayuno, para ir al colegio.

NOTION La-routine-le-matin
Vocabulaire - Le quotidien

Les actions quotidiennes
La routine : l’après-midi et la soirée
Almorzar (ue)

Déjeuner

Ir al recreo

Aller en récréation

Regresar a casa

Rentrer à la maison

Merendar (ie)

Gouter

Jugar (ue)

Jouer

Estudiar

Étudier

Hacer los deberes

Faire ses devoirs

Ver la tele

Regarder la télévision

Cenar

Diner

Acostarse (ue)

Se coucher

Exemple
Un padre y su hijo
Padre: ¡Hola Juanito! ¿Qué tal el día?
Juan: Muy bien, papá. Esta tarde, he almorzado
en el comedor estudiantil, luego he estudiado en
clase. He podido disfrutar del recreo y al acabar
las clases, he regresado a casa para merendar.
Después, he jugado un poco antes de hacer los
deberes y de ver un poco la tele.
Padre: ¡Estupendo!

NOTION La-routine-l’après-midi-et-la-soirée
Vocabulaire - Le quotidien

L’expression de l’heure et les
moments de la journée
Les différents moments de la journée
El amanecer

Le lever du jour

La mañana

Le matin

La tarde

L’après-midi

El atardecer

La tombée du jour

La noche

Le soir, la nuit

Exemple
Cuando

me

levanto

por

la

mañana,

la

primera cosa que hago es desayunar pan con
mantequilla.
Remarque
Pour indiquer le temps (une période), on utilise la
préposition por .
• Ex. : Vamos al cine por la tarde.

NOTION Les-différents-moments-de-la-journée
Vocabulaire - Le quotidien

L’expression de l’heure et les
moments de la journée
L’expression de l’heure
Un reloj

Une montre

Una hora

Une heure

¿Qué hora es?

Quelle heure est-il ?

Son las dos y cuarto.

Il est deux heures et quart.

Son las dos en punto.

Il est deux heures pile.

Son las dos menos diez.

Il est deux heures moins dix.

Son las dos y cinco.

Il est deux heures cinq.

Son las dos y media.

Il est deux heures et demie.

Es la una.

Il est une heure.

Mediodía

Midi

Medianoche

Minuit

Exemple
En la estación
Viajero: Buenos días, quisiera ir a Madrid por
favor, este lunes.
Taquillero: Buenos días. ¿Por la mañana o por la
tarde?
Viajero: Por la mañana.
Taquillero: El primer tren sale a las cinco y cuarto.
Viajero: Bueno, es un poco temprano… ¿Y el
siguiente?
Taquillero: Después hay otro a las siete menos
diez y el siguiente es a las ocho en punto.
Remarque
Pour exprimer l’heure, on utilise toujours le verbe
ser. On le conjugue à la 3e personne du pluriel
(ex. : Son las tres), sauf pour dire « Il est une heure
» (Es la una).

NOTION L’expression-de-l’heure
Vocabulaire - Le quotidien

Le calendrier
Les jours de la semaine
Lunes

Lundi

Martes

Mardi

Miércoles

Mercredi

Jueves

Jeudi

Viernes

Vendredi

Sábado

Samedi

Domingo

Dimanche

El ﬁn de semana

Le week-end

Un día

Un jour

Los días de la semana

Les jours de la semaine

Exemple
De lunes a viernes, tengo clase. El ﬁn de semana
cae el sábado y domingo.

NOTION Les-jours-de-la-semaine
Vocabulaire - Le quotidien

Le calendrier
Les mois de l’année et les saisons
Un mes

Un mois

El año

L’année

Los meses del año

Les mois de l’année

Enero

Janvier

Febrero

Février

Marzo

Mars

Abril

Avril

Mayo

Mai

Junio

Juin

Julio

Juillet

Agosto

Aout

Septiembre

Septembre

Octubre

Octobre

Noviembre

Novembre

Diciembre

Décembre

Las estaciones

Les saisons

El verano

L’été

El otoño

L’automne

El invierno

L’hiver

La primavera

Le printemps

Exemple
Hoy, es sábado, once de noviembre de dos mil
diecisiete.
Exemple
Pour exprimer la date en espagnol, on utilise la
préposition de avant le mois et avant l’année.

NOTION Les-mois-de-l’année-et-les-saisons
Vocabulaire - Le quotidien

La météo
Les éléments naturels
El cielo

Le ciel

El sol

Le soleil

La nube

Le nuage

La lluvia

La pluie

La tormenta

L’orage

El viento

Le vent

La nieve

La neige

Exemple
Dos amigas
Ana: ¿Sabes? ¡En noviembre me voy de viaje a
Bolivia!
Carla: ¡Increíble! Pero ¿por qué en este mes? Hace
frío, y no hace buen tiempo…
Ana: No, en absoluto, ¡es lo contrario!

En el

otro hemisferio en noviembre, no llueve tanto y
el tiempo está soleado. Las temperaturas son las
más agradables.

NOTION Les-éléments-naturels
Vocabulaire - Le quotidien

La météo
Pour parler du temps
Llover (ue)

Pleuvoir

Nevar (ie)

Neiger

Estar nublado

Être nuageux

Estar soleado

Être ensoleillé

Hacer buen tiempo

Faire beau

Hacer frío

Faire froid

Hacer calor

Faire chaud

Exemple
Hoy hace frío pero seguro que mañana va a hacer
calor.

NOTION Pour-parler-du-temps
