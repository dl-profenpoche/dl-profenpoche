Vocabulaire - Les loisirs

Le sport
Les différents sports
Le sport

El deporte

El fútbol

Le football

El baloncesto

Le basketball

La natación

La natation

El tenis

Le tennis

El atletismo

L’athlétisme

El balonmano

Le handball

El ciclismo

Le cyclisme

La gimnasia

La gymnastique

La halteroﬁlia

L’haltérophilie

El paracaidismo

Le parachutisme

El piragüismo

Le canoë

El rugby

Le rugby

El senderismo

La randonnée

El voleibol

Le volley-ball

El esquí

Le ski

La equitación

L’équitation

El triatlón

Le triathlon

El patinaje

Le patinage

El remo

L’aviron

Exemple
¿Qué deporte quieres practicar?
• Hija:

Papá, ya no quiero seguir con la

gimnasia el año próximo.

Preﬁero hacer

baloncesto.
• Padre:

¿Ah sí?

¡Pero cada año es algo

distinto!
• Hija:

Es que me gustaría practicar un

deporte colectivo, como el baloncesto, o el
fútbol.

NOTION Les-différents-sports
Vocabulaire - Les loisirs

Le sport
Le lexique lié au sport
El balón

Le ballon

Los esquís

Les skis

Los patines

Les patins

La raqueta

La raquette

La bicicleta

Le vélo

La pesa

L’haltère

La pelota

La balle, le ballon

El partido

Le match

La competencia, la competición

La compétition

La carrera

La course

El trofeo

Le trophée

El árbitro

L’arbitre

El estadio

Le stade

El equipo

L’équipe

El hincha

Le supporter

Exemple
Para jugar al fútbol o al rugby se necesita un
balón.

NOTION Le-lexique-lié-au-sport
Vocabulaire - Les loisirs

La musique
Les différents instruments
La guitarra

La guitare

El piano

Le piano

La batería

La batterie

La ﬂauta

La ﬂute

El arpa

La harpe

El violín

Le violon

La trompeta

La trompette

El saxofón

Le saxophone

El acordeón

L’accordéon

Las castañuelas

Les castagnettes

El violonchelo

Le violoncelle

El tambor

Le tambour

La armónica

L’harmonica

El teclado

Le clavier

El órgano

L’orgue

El clarinete

La clarinette

Exemple
Toco la guitarra y el piano.
Remarque
Pour dire « jouer d’un instrument » en espagnol,
on utilise le verbe tocar.

NOTION Les-différents-instruments
Vocabulaire - Les loisirs

La musique
Le lexique lié à la musique
El concierto

Le concert

La coral

La chorale

El ballet

Le ballet

La danza

La danse

La ópera

L’opéra

El recital

Le récital

La audición

L’audition

Tocar un instrumento

Jouer d’un instrument

Las notas musicales

Les notes de musique

La partitura

La partition

Cantar

Chanter

El canto

Le chant

Exemple
En la ópera
• Julia: Lola, ¿te apetece salir esta noche?
¡Se estrena El sombrero de tres picos en la
ópera de Madrid!
• Lola: Sí, y además, los intérpretes del ballet
mencionados son verdaderos artistas.
• Julia:

¡Qué bien!

magníﬁca! ¡Vamos!

Y ¡la danza ﬁnal es

NOTION Le-lexique-lié-à-la-musique
Vocabulaire - Les loisirs

Les activités extra-scolaires
Les activités sportives et artistiques
Practicar deporte

Faire du sport

El taller de lectura

L’atelier de lecture

Tocar música

Jouer de la musique

Dibujar

Dessiner

Hacer teatro

Faire du théâtre

Practicar danza (moderna)

Faire de la danse (moderne)

Jugar (ue) ajedrez

Jouer aux échecs

La lectura

La lecture

Exemple
Cada miércoles por la tarde practico deporte:
juego al baloncesto. El ﬁn de semana, voy a clases
de dibujo.

NOTION Les-activités-sportives-et-artistiques
Vocabulaire - Les loisirs

Les activités extra-scolaires
Les loisirs
Les loisirs

Los ocios

Ir al cine

Aller au cinéma

Navegar en internet

Surfer sur internet

Jugar (ue) con videojuegos

Jouer aux jeux vidéos

Salir con amigos

Sortir avec des amis

Ir a la piscina

Aller à la piscine

Ir de paseo

Se promener

Ir al parque

Aller au parc

NOTION Les-loisirs
Vocabulaire - Les loisirs

Les fêtes et leur organisation
Les différentes fêtes
El cumpleaños

L’anniversaire

La Navidad

Noël

El Año Nuevo

Le Nouvel An

Los Reyes Magos

Les Rois Mages

El día de San Valentín

La Saint-Valentin

El día de la Madre

La fête des mères

El día del Padre

La fête des pères

El día del Niño

La fête des enfants

La Nochebuena

La nuit de Noël

La Nochevieja

La nuit du 31 décembre

La Pascua

Pâques

NOTION Les-différentes-fêtes
Vocabulaire - Les loisirs

Les fêtes et leur organisation
L’organisation d’une fête
Hacer una lista de invitados

Faire la liste des invités

Hacer las compras

Faire des courses

Decorar la casa

Décorer la maison

Comprar un regalo

Acheter un cadeau

Enviar las invitaciones

Envoyer les invitations

El pastel, la torta

Le gâteau

El vestido

La robe

El traje

Le costume

Exemple
Organizar su quinceañera
• Esperanza:

¿Has pensado en enviar las

invitaciones para tu quinceañera?
• Rita: Sí, claro, también me he comprado el
vestido, he reservado la torta y he preparado
un discurso.
• Esperanza: ¡Guay! ¿Todo listo, entonces?
• Rita: Bueno...

La decoración de la casa

queda por hacer. ¿Me ayudas?

NOTION L’organisation-d’une-fête
Vocabulaire - Les loisirs

Le sport
Les différents sports
Le sport

El deporte

El fútbol

Le football

El baloncesto

Le basketball

La natación

La natation

El tenis

Le tennis

El atletismo

L’athlétisme

El balonmano

Le handball

El ciclismo

Le cyclisme

La gimnasia

La gymnastique

La halteroﬁlia

L’haltérophilie

El paracaidismo

Le parachutisme

El piragüismo

Le canoë

El rugby

Le rugby

El senderismo

La randonnée

El voleibol

Le volley-ball

El esquí

Le ski

La equitación

L’équitation

El triatlón

Le triathlon

El patinaje

Le patinage

El remo

L’aviron

Exemple
¿Qué deporte quieres practicar?
• Hija:

Papá, ya no quiero seguir con la

gimnasia el año próximo.

Preﬁero hacer

baloncesto.
• Padre:

¿Ah sí?

¡Pero cada año es algo

distinto!
• Hija:

Es que me gustaría practicar un

deporte colectivo, como el baloncesto, o el
fútbol.

NOTION Les-différents-sports
Vocabulaire - Les loisirs

Le sport
Le lexique lié au sport
El balón

Le ballon

Los esquís

Les skis

Los patines

Les patins

La raqueta

La raquette

La bicicleta

Le vélo

La pesa

L’haltère

La pelota

La balle, le ballon

El partido

Le match

La competencia, la competición

La compétition

La carrera

La course

El trofeo

Le trophée

El árbitro

L’arbitre

El estadio

Le stade

El equipo

L’équipe

El hincha

Le supporter

Exemple
Para jugar al fútbol o al rugby se necesita un
balón.

NOTION Le-lexique-lié-au-sport
Vocabulaire - Les loisirs

La musique
Les différents instruments
La guitarra

La guitare

El piano

Le piano

La batería

La batterie

La ﬂauta

La ﬂute

El arpa

La harpe

El violín

Le violon

La trompeta

La trompette

El saxofón

Le saxophone

El acordeón

L’accordéon

Las castañuelas

Les castagnettes

El violonchelo

Le violoncelle

El tambor

Le tambour

La armónica

L’harmonica

El teclado

Le clavier

El órgano

L’orgue

El clarinete

La clarinette

Exemple
Toco la guitarra y el piano.
Remarque
Pour dire « jouer d’un instrument » en espagnol,
on utilise le verbe tocar.

NOTION Les-différents-instruments
Vocabulaire - Les loisirs

La musique
Le lexique lié à la musique
El concierto

Le concert

La coral

La chorale

El ballet

Le ballet

La danza

La danse

La ópera

L’opéra

El recital

Le récital

La audición

L’audition

Tocar un instrumento

Jouer d’un instrument

Las notas musicales

Les notes de musique

La partitura

La partition

Cantar

Chanter

El canto

Le chant

Exemple
En la ópera
• Julia: Lola, ¿te apetece salir esta noche?
¡Se estrena El sombrero de tres picos en la
ópera de Madrid!
• Lola: Sí, y además, los intérpretes del ballet
mencionados son verdaderos artistas.
• Julia:

¡Qué bien!

magníﬁca! ¡Vamos!

Y ¡la danza ﬁnal es

NOTION Le-lexique-lié-à-la-musique
Vocabulaire - Les loisirs

Les activités extra-scolaires
Les activités sportives et artistiques
Practicar deporte

Faire du sport

El taller de lectura

L’atelier de lecture

Tocar música

Jouer de la musique

Dibujar

Dessiner

Hacer teatro

Faire du théâtre

Practicar danza (moderna)

Faire de la danse (moderne)

Jugar (ue) ajedrez

Jouer aux échecs

La lectura

La lecture

Exemple
Cada miércoles por la tarde practico deporte:
juego al baloncesto. El ﬁn de semana, voy a clases
de dibujo.

NOTION Les-activités-sportives-et-artistiques
Vocabulaire - Les loisirs

Les activités extra-scolaires
Les loisirs
Les loisirs

Los ocios

Ir al cine

Aller au cinéma

Navegar en internet

Surfer sur internet

Jugar (ue) con videojuegos

Jouer aux jeux vidéos

Salir con amigos

Sortir avec des amis

Ir a la piscina

Aller à la piscine

Ir de paseo

Se promener

Ir al parque

Aller au parc

NOTION Les-loisirs
Vocabulaire - Les loisirs

Les fêtes et leur organisation
Les différentes fêtes
El cumpleaños

L’anniversaire

La Navidad

Noël

El Año Nuevo

Le Nouvel An

Los Reyes Magos

Les Rois Mages

El día de San Valentín

La Saint-Valentin

El día de la Madre

La fête des mères

El día del Padre

La fête des pères

El día del Niño

La fête des enfants

La Nochebuena

La nuit de Noël

La Nochevieja

La nuit du 31 décembre

La Pascua

Pâques

NOTION Les-différentes-fêtes
Vocabulaire - Les loisirs

Les fêtes et leur organisation
L’organisation d’une fête
Hacer una lista de invitados

Faire la liste des invités

Hacer las compras

Faire des courses

Decorar la casa

Décorer la maison

Comprar un regalo

Acheter un cadeau

Enviar las invitaciones

Envoyer les invitations

El pastel, la torta

Le gâteau

El vestido

La robe

El traje

Le costume

Exemple
Organizar su quinceañera
• Esperanza:

¿Has pensado en enviar las

invitaciones para tu quinceañera?
• Rita: Sí, claro, también me he comprado el
vestido, he reservado la torta y he preparado
un discurso.
• Esperanza: ¡Guay! ¿Todo listo, entonces?
• Rita: Bueno...

La decoración de la casa

queda por hacer. ¿Me ayudas?

NOTION L’organisation-d’une-fête
Vocabulaire - Les loisirs

Le sport
Les différents sports
Le sport

El deporte

El fútbol

Le football

El baloncesto

Le basketball

La natación

La natation

El tenis

Le tennis

El atletismo

L’athlétisme

El balonmano

Le handball

El ciclismo

Le cyclisme

La gimnasia

La gymnastique

La halteroﬁlia

L’haltérophilie

El paracaidismo

Le parachutisme

El piragüismo

Le canoë

El rugby

Le rugby

El senderismo

La randonnée

El voleibol

Le volley-ball

El esquí

Le ski

La equitación

L’équitation

El triatlón

Le triathlon

El patinaje

Le patinage

El remo

L’aviron

Exemple
¿Qué deporte quieres practicar?
• Hija:

Papá, ya no quiero seguir con la

gimnasia el año próximo.

Preﬁero hacer

baloncesto.
• Padre:

¿Ah sí?

¡Pero cada año es algo

distinto!
• Hija:

Es que me gustaría practicar un

deporte colectivo, como el baloncesto, o el
fútbol.

NOTION Les-différents-sports
Vocabulaire - Les loisirs

Le sport
Le lexique lié au sport
El balón

Le ballon

Los esquís

Les skis

Los patines

Les patins

La raqueta

La raquette

La bicicleta

Le vélo

La pesa

L’haltère

La pelota

La balle, le ballon

El partido

Le match

La competencia, la competición

La compétition

La carrera

La course

El trofeo

Le trophée

El árbitro

L’arbitre

El estadio

Le stade

El equipo

L’équipe

El hincha

Le supporter

Exemple
Para jugar al fútbol o al rugby se necesita un
balón.

NOTION Le-lexique-lié-au-sport
Vocabulaire - Les loisirs

La musique
Les différents instruments
La guitarra

La guitare

El piano

Le piano

La batería

La batterie

La ﬂauta

La ﬂute

El arpa

La harpe

El violín

Le violon

La trompeta

La trompette

El saxofón

Le saxophone

El acordeón

L’accordéon

Las castañuelas

Les castagnettes

El violonchelo

Le violoncelle

El tambor

Le tambour

La armónica

L’harmonica

El teclado

Le clavier

El órgano

L’orgue

El clarinete

La clarinette

Exemple
Toco la guitarra y el piano.
Remarque
Pour dire « jouer d’un instrument » en espagnol,
on utilise le verbe tocar.

NOTION Les-différents-instruments
Vocabulaire - Les loisirs

La musique
Le lexique lié à la musique
El concierto

Le concert

La coral

La chorale

El ballet

Le ballet

La danza

La danse

La ópera

L’opéra

El recital

Le récital

La audición

L’audition

Tocar un instrumento

Jouer d’un instrument

Las notas musicales

Les notes de musique

La partitura

La partition

Cantar

Chanter

El canto

Le chant

Exemple
En la ópera
• Julia: Lola, ¿te apetece salir esta noche?
¡Se estrena El sombrero de tres picos en la
ópera de Madrid!
• Lola: Sí, y además, los intérpretes del ballet
mencionados son verdaderos artistas.
• Julia:

¡Qué bien!

magníﬁca! ¡Vamos!

Y ¡la danza ﬁnal es

NOTION Le-lexique-lié-à-la-musique
Vocabulaire - Les loisirs

Les activités extra-scolaires
Les activités sportives et artistiques
Practicar deporte

Faire du sport

El taller de lectura

L’atelier de lecture

Tocar música

Jouer de la musique

Dibujar

Dessiner

Hacer teatro

Faire du théâtre

Practicar danza (moderna)

Faire de la danse (moderne)

Jugar (ue) ajedrez

Jouer aux échecs

La lectura

La lecture

Exemple
Cada miércoles por la tarde practico deporte:
juego al baloncesto. El ﬁn de semana, voy a clases
de dibujo.

NOTION Les-activités-sportives-et-artistiques
Vocabulaire - Les loisirs

Les activités extra-scolaires
Les loisirs
Les loisirs

Los ocios

Ir al cine

Aller au cinéma

Navegar en internet

Surfer sur internet

Jugar (ue) con videojuegos

Jouer aux jeux vidéos

Salir con amigos

Sortir avec des amis

Ir a la piscina

Aller à la piscine

Ir de paseo

Se promener

Ir al parque

Aller au parc

NOTION Les-loisirs
Vocabulaire - Les loisirs

Les fêtes et leur organisation
Les différentes fêtes
El cumpleaños

L’anniversaire

La Navidad

Noël

El Año Nuevo

Le Nouvel An

Los Reyes Magos

Les Rois Mages

El día de San Valentín

La Saint-Valentin

El día de la Madre

La fête des mères

El día del Padre

La fête des pères

El día del Niño

La fête des enfants

La Nochebuena

La nuit de Noël

La Nochevieja

La nuit du 31 décembre

La Pascua

Pâques

NOTION Les-différentes-fêtes
Vocabulaire - Les loisirs

Les fêtes et leur organisation
L’organisation d’une fête
Hacer una lista de invitados

Faire la liste des invités

Hacer las compras

Faire des courses

Decorar la casa

Décorer la maison

Comprar un regalo

Acheter un cadeau

Enviar las invitaciones

Envoyer les invitations

El pastel, la torta

Le gâteau

El vestido

La robe

El traje

Le costume

Exemple
Organizar su quinceañera
• Esperanza:

¿Has pensado en enviar las

invitaciones para tu quinceañera?
• Rita: Sí, claro, también me he comprado el
vestido, he reservado la torta y he preparado
un discurso.
• Esperanza: ¡Guay! ¿Todo listo, entonces?
• Rita: Bueno...

La decoración de la casa

queda por hacer. ¿Me ayudas?

NOTION L’organisation-d’une-fête
Vocabulaire - Les loisirs

Le sport
Les différents sports
Le sport

El deporte

El fútbol

Le football

El baloncesto

Le basketball

La natación

La natation

El tenis

Le tennis

El atletismo

L’athlétisme

El balonmano

Le handball

El ciclismo

Le cyclisme

La gimnasia

La gymnastique

La halteroﬁlia

L’haltérophilie

El paracaidismo

Le parachutisme

El piragüismo

Le canoë

El rugby

Le rugby

El senderismo

La randonnée

El voleibol

Le volley-ball

El esquí

Le ski

La equitación

L’équitation

El triatlón

Le triathlon

El patinaje

Le patinage

El remo

L’aviron

Exemple
¿Qué deporte quieres practicar?
• Hija:

Papá, ya no quiero seguir con la

gimnasia el año próximo.

Preﬁero hacer

baloncesto.
• Padre:

¿Ah sí?

¡Pero cada año es algo

distinto!
• Hija:

Es que me gustaría practicar un

deporte colectivo, como el baloncesto, o el
fútbol.

NOTION Les-différents-sports
Vocabulaire - Les loisirs

Le sport
Le lexique lié au sport
El balón

Le ballon

Los esquís

Les skis

Los patines

Les patins

La raqueta

La raquette

La bicicleta

Le vélo

La pesa

L’haltère

La pelota

La balle, le ballon

El partido

Le match

La competencia, la competición

La compétition

La carrera

La course

El trofeo

Le trophée

El árbitro

L’arbitre

El estadio

Le stade

El equipo

L’équipe

El hincha

Le supporter

Exemple
Para jugar al fútbol o al rugby se necesita un
balón.

NOTION Le-lexique-lié-au-sport
Vocabulaire - Les loisirs

La musique
Les différents instruments
La guitarra

La guitare

El piano

Le piano

La batería

La batterie

La ﬂauta

La ﬂute

El arpa

La harpe

El violín

Le violon

La trompeta

La trompette

El saxofón

Le saxophone

El acordeón

L’accordéon

Las castañuelas

Les castagnettes

El violonchelo

Le violoncelle

El tambor

Le tambour

La armónica

L’harmonica

El teclado

Le clavier

El órgano

L’orgue

El clarinete

La clarinette

Exemple
Toco la guitarra y el piano.
Remarque
Pour dire « jouer d’un instrument » en espagnol,
on utilise le verbe tocar.

NOTION Les-différents-instruments
Vocabulaire - Les loisirs

La musique
Le lexique lié à la musique
El concierto

Le concert

La coral

La chorale

El ballet

Le ballet

La danza

La danse

La ópera

L’opéra

El recital

Le récital

La audición

L’audition

Tocar un instrumento

Jouer d’un instrument

Las notas musicales

Les notes de musique

La partitura

La partition

Cantar

Chanter

El canto

Le chant

Exemple
En la ópera
• Julia: Lola, ¿te apetece salir esta noche?
¡Se estrena El sombrero de tres picos en la
ópera de Madrid!
• Lola: Sí, y además, los intérpretes del ballet
mencionados son verdaderos artistas.
• Julia:

¡Qué bien!

magníﬁca! ¡Vamos!

Y ¡la danza ﬁnal es

NOTION Le-lexique-lié-à-la-musique
Vocabulaire - Les loisirs

Les activités extra-scolaires
Les activités sportives et artistiques
Practicar deporte

Faire du sport

El taller de lectura

L’atelier de lecture

Tocar música

Jouer de la musique

Dibujar

Dessiner

Hacer teatro

Faire du théâtre

Practicar danza (moderna)

Faire de la danse (moderne)

Jugar (ue) ajedrez

Jouer aux échecs

La lectura

La lecture

Exemple
Cada miércoles por la tarde practico deporte:
juego al baloncesto. El ﬁn de semana, voy a clases
de dibujo.

NOTION Les-activités-sportives-et-artistiques
Vocabulaire - Les loisirs

Les activités extra-scolaires
Les loisirs
Les loisirs

Los ocios

Ir al cine

Aller au cinéma

Navegar en internet

Surfer sur internet

Jugar (ue) con videojuegos

Jouer aux jeux vidéos

Salir con amigos

Sortir avec des amis

Ir a la piscina

Aller à la piscine

Ir de paseo

Se promener

Ir al parque

Aller au parc

NOTION Les-loisirs
Vocabulaire - Les loisirs

Les fêtes et leur organisation
Les différentes fêtes
El cumpleaños

L’anniversaire

La Navidad

Noël

El Año Nuevo

Le Nouvel An

Los Reyes Magos

Les Rois Mages

El día de San Valentín

La Saint-Valentin

El día de la Madre

La fête des mères

El día del Padre

La fête des pères

El día del Niño

La fête des enfants

La Nochebuena

La nuit de Noël

La Nochevieja

La nuit du 31 décembre

La Pascua

Pâques

NOTION Les-différentes-fêtes
Vocabulaire - Les loisirs

Les fêtes et leur organisation
L’organisation d’une fête
Hacer una lista de invitados

Faire la liste des invités

Hacer las compras

Faire des courses

Decorar la casa

Décorer la maison

Comprar un regalo

Acheter un cadeau

Enviar las invitaciones

Envoyer les invitations

El pastel, la torta

Le gâteau

El vestido

La robe

El traje

Le costume

Exemple
Organizar su quinceañera
• Esperanza:

¿Has pensado en enviar las

invitaciones para tu quinceañera?
• Rita: Sí, claro, también me he comprado el
vestido, he reservado la torta y he preparado
un discurso.
• Esperanza: ¡Guay! ¿Todo listo, entonces?
• Rita: Bueno...

La decoración de la casa

queda por hacer. ¿Me ayudas?

NOTION L’organisation-d’une-fête
