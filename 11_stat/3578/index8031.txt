Vocabulaire - L’école et le
monde professionnel

L’école et le matériel scolaire
Les lieux
La escuela

L’école

El colegio

Le collège

El aula

La salle de classe

El comedor

La cantine

El patio de recreo

La cour de récréation

La enfermería

L’inﬁrmerie

El gimnasio

Le gymnase

La cancha de deporte

Le terrain de sport

El vestuario

Le vestiaire

El baño

Les toilettes

La secretaría

Le secrétariat

La biblioteca

Le CDI

El laboratorio

Le laboratoire

La residencia, el internado

L’internat

El anﬁteatro

L’amphithéâtre

Exemple
En el colegio
• María: Pablito, te espero a las cinco en el
patio, ¿vale?
• Pablito: De acuerdo. De momento estoy en
la biblioteca, y antes tengo que pasar por la
secretaría.
• María:

Pues si quieres nos podemos

encontrar frente a la enfermería, que está al
lado.

NOTION Les-lieux
Vocabulaire - L’école et le
monde professionnel

L’école et le matériel scolaire
Le matériel scolaire
La mochila

Le sac à dos

El cuaderno

Le cahier

El libro

Le livre

El lápiz

Le crayon

El sacapuntas

Le taille-crayon

El bolígrafo

Le stylo

El estuche

La trousse

La goma

La gomme

El pegamento

La colle

Las tijeras

Les ciseaux

El rotulador

Le feutre

El marcador

Le marqueur

El pincel

Le pinceau

El clip

Le trombone

La carpeta

La pochette

La hoja

La feuille

La pizarra

Le tableau

La tiza

La craie

La calculadora

La calculatrice

La regla

La règle

El compás

Le compas

Exemple
En clase
• Profesor: Chicos, sacad los cuadernos y
escribid la fecha de hoy. No olvidéis utilizar
el rotulador de color rojo.
• Santi: Profe, no puedo, he olvidado mi
mochila en casa. Solo llevo conmigo un libro.
• Profesor: ¿Otra vez? Bueno, María te va a
prestar una hoja y bolígrafos.

NOTION Le-matériel-scolaire
Vocabulaire - L’école et le
monde professionnel

L’école et le matériel scolaire
Les matières
La clase

Le cours

La asignatura

La matière scolaire

Las matemáticas

Les mathématiques

La historia

L’histoire

La geografía

La géographie

Las humanidades

Les sciences humaines

Las ciencias naturales, la biología

Les sciences de la vie et de la terre

La física

La physique

La química

La chimie

La música

La musique

La educación física

L’éducation physique

La ﬁlosofía

La philosophie

La lengua extranjera

La langue étrangère

La educación plástica

Les arts plastiques

La tecnología

La technologie

Exemple
Las asignaturas que más me gustan son las
matemáticas y el deporte.

NOTION Les-matières
Vocabulaire - L’école et le
monde professionnel

Les études
Les études supérieures
El bachillerato

Le baccalauréat

La universidad

L’université

La escuela técnica

L’école technique

La formación profesional

La formation professionnelle

La matrícula

L’inscription

La beca

La bourse

Los exámenes

Les examens

Estudiar

Étudier

Aprobar un examen

Réussir un examen

Suspender un examen

Échouer à un examen

Exemple
Mi hermano tiene muchas horas de clase en la
universidad de Madrid.

NOTION Les-études-supérieures
Vocabulaire - L’école et le
monde professionnel

Les études
Les diplômes, les cursus
La licencia

La licence

El máster

Le master

La licenciatura

La maitrise

El diploma

Le diplôme

La carrera

Les études

El título

Le titre

El doctorado

Le doctorat

Exemple
Quiero hacer una carrera universitaria.

NOTION Les-diplômes-les-cursus
Vocabulaire - L’école et le
monde professionnel

Les professions

El profesor, la profesora

Le professeur, la professeure

El maestro, la maestra

L’instituteur, l’institutrice

El panadero, la panadera

Le boulanger, la boulangère

El camarero, la camarera

Le serveur, la serveuse

El carnicero, la carnicera

Le boucher, la bouchère

El cajero, la cajera

Le caissier, la caissière

El cocinero, la cocinera

Le cuisinier, la cuisinière

El pastelero, la pastelera

Le pâtissier, la pâtissière

El vendedor, la vendedora / El dependiente, la dependiente

Le vendeur, la vendeuse

El ingeniero, la ingeniera

L’ingénieur(e)

El arquitecto, la arquitecta

L’architecte

El albañil, la albañil

Le maçon

El fontanero, la fontanera

Le plombier, la plombière

El pintor, la pintora

Le peintre, la peintre

El informático, la informática

L’informaticien(ne)

El mecánico, la mecánica

Le mécanicien, la mécanicienne

El abogado, la abogada

L’avocat(e)

El juez, la jueza

Le juge, la juge

El músico, la música

Le musicien, la musicienne

El médico, la médica

Le docteur, la docteure

El enfermero, la enfermera

L’inﬁrmier, l’inﬁrmière

El partero, la partera

Le sage-femme homme, la sage-femme

El cirujano, la cirujana

Le chirurgien, la chirurgienne

El dentista, la dentista

Le dentiste, la dentiste

El pediatra, la pediatra

Le pédiatre, la pédiatre

Exemple
El futuro profesional
Chema: Pepa, ¿qué te gustaría ser, más tarde?
Algo artístico como ¿pintora?, o ¿música?
Pepa: ¿Yo? No… A mí me encanta la medicina.
Seré cirujana, o médica. O mejor aún, pediatra. ¿Y
tú?
Chema: Yo quiero ser fontanero, o albañil. Nada
más interesante como la construcción de las
casas.

NOTION Les-professions
Vocabulaire - L’école et le
monde professionnel

Les relations sociales
Les types de relations sociales
La amistad

L’amitié

El amigo, la amiga

L’ami(e)

El compañero, la compañera

Le camarade, la camarade

La familia

La famille

La pareja

Le couple

El enemigo, la enemiga

L’ennemi(e)

La pandilla

La bande de copains

El vecino, la vecina

Le voisin, la voisine

Exemple
Mis amigos y mi pareja son lo más importante de
mi vida.

NOTION Les-types-de-relations-sociales
Vocabulaire - L’école et le
monde professionnel

Les relations sociales
Les démonstrations
El abrazo

L’accolade

Abrazar

Embrasser, enlacer

El beso

Le baiser

Besar

Embrasser

El apretón de manos

La poignée de main

Saludar

Saluer

Exemple
Para saludar alguien que no conozco le doy un
apretón de manos.

NOTION Les-démonstrations
Vocabulaire - L’école et le
monde professionnel

Les relations sociales
Les sentiments
El ﬂechazo

Le coup de foudre

Enamorarse

Tomber amoureux

Llevarse bien con

S’entendre bien avec

Llevarse mal con

S’entendre mal avec

Preocuparse por

S’inquiéter pour

Echar de menos

Manquer

Tener celos

Être jaloux

Exemple
Le echo de menos, nos llevamos muy bien.

NOTION Les-sentiments
Vocabulaire - L’école et le
monde professionnel

Les nouvelles technologies
Les appareils
El ordenador

L’ordinateur

El (teléfono) móvil

Le (téléphone) portable

La tableta, la pantalla táctil

La tablette

La pantalla

L’écran

El ratón

La souris

El teclado

Le clavier

La impresora

L’imprimante

El cable

Le câble

Exemple
La pantalla de mi ordenador es pequeña.

NOTION Les-appareils
Vocabulaire - L’école et le
monde professionnel

Les nouvelles technologies
L’usage virtuel
El correo electrónico

Le courriel, l’e-mail

El perﬁl

Le proﬁl

El usuario

L’usager

La contraseña, la clave

Le mot de passe

La aplicación

L’application

La red

Le réseau, internet

La red social

Le réseau social

La página web

La site internet

El blog

Le blog

La memoria virtual

La mémoire virtuelle

Navegar en internet

Surfer sur internet

Descargar

Télécharger

Guardar

Enregistrer

Exemple
He cambiado la contraseña de mi correo
electrónico.

NOTION Lusage-virtuel
Vocabulaire - L’école et le
monde professionnel

L’école et le matériel scolaire
Les lieux
La escuela

L’école

El colegio

Le collège

El aula

La salle de classe

El comedor

La cantine

El patio de recreo

La cour de récréation

La enfermería

L’inﬁrmerie

El gimnasio

Le gymnase

La cancha de deporte

Le terrain de sport

El vestuario

Le vestiaire

El baño

Les toilettes

La secretaría

Le secrétariat

La biblioteca

Le CDI

El laboratorio

Le laboratoire

La residencia, el internado

L’internat

El anﬁteatro

L’amphithéâtre

Exemple
En el colegio
• María: Pablito, te espero a las cinco en el
patio, ¿vale?
• Pablito: De acuerdo. De momento estoy en
la biblioteca, y antes tengo que pasar por la
secretaría.
• María:

Pues si quieres nos podemos

encontrar frente a la enfermería, que está al
lado.

NOTION Les-lieux
Vocabulaire - L’école et le
monde professionnel

L’école et le matériel scolaire
Le matériel scolaire
La mochila

Le sac à dos

El cuaderno

Le cahier

El libro

Le livre

El lápiz

Le crayon

El sacapuntas

Le taille-crayon

El bolígrafo

Le stylo

El estuche

La trousse

La goma

La gomme

El pegamento

La colle

Las tijeras

Les ciseaux

El rotulador

Le feutre

El marcador

Le marqueur

El pincel

Le pinceau

El clip

Le trombone

La carpeta

La pochette

La hoja

La feuille

La pizarra

Le tableau

La tiza

La craie

La calculadora

La calculatrice

La regla

La règle

El compás

Le compas

Exemple
En clase
• Profesor: Chicos, sacad los cuadernos y
escribid la fecha de hoy. No olvidéis utilizar
el rotulador de color rojo.
• Santi: Profe, no puedo, he olvidado mi
mochila en casa. Solo llevo conmigo un libro.
• Profesor: ¿Otra vez? Bueno, María te va a
prestar una hoja y bolígrafos.

NOTION Le-matériel-scolaire
Vocabulaire - L’école et le
monde professionnel

L’école et le matériel scolaire
Les matières
La clase

Le cours

La asignatura

La matière scolaire

Las matemáticas

Les mathématiques

La historia

L’histoire

La geografía

La géographie

Las humanidades

Les sciences humaines

Las ciencias naturales, la biología

Les sciences de la vie et de la terre

La física

La physique

La química

La chimie

La música

La musique

La educación física

L’éducation physique

La ﬁlosofía

La philosophie

La lengua extranjera

La langue étrangère

La educación plástica

Les arts plastiques

La tecnología

La technologie

Exemple
Las asignaturas que más me gustan son las
matemáticas y el deporte.

NOTION Les-matières
Vocabulaire - L’école et le
monde professionnel

Les études
Les études supérieures
El bachillerato

Le baccalauréat

La universidad

L’université

La escuela técnica

L’école technique

La formación profesional

La formation professionnelle

La matrícula

L’inscription

La beca

La bourse

Los exámenes

Les examens

Estudiar

Étudier

Aprobar un examen

Réussir un examen

Suspender un examen

Échouer à un examen

Exemple
Mi hermano tiene muchas horas de clase en la
universidad de Madrid.

NOTION Les-études-supérieures
Vocabulaire - L’école et le
monde professionnel

Les études
Les diplômes, les cursus
La licencia

La licence

El máster

Le master

La licenciatura

La maitrise

El diploma

Le diplôme

La carrera

Les études

El título

Le titre

El doctorado

Le doctorat

Exemple
Quiero hacer una carrera universitaria.

NOTION Les-diplômes-les-cursus
Vocabulaire - L’école et le
monde professionnel

Les professions

El profesor, la profesora

Le professeur, la professeure

El maestro, la maestra

L’instituteur, l’institutrice

El panadero, la panadera

Le boulanger, la boulangère

El camarero, la camarera

Le serveur, la serveuse

El carnicero, la carnicera

Le boucher, la bouchère

El cajero, la cajera

Le caissier, la caissière

El cocinero, la cocinera

Le cuisinier, la cuisinière

El pastelero, la pastelera

Le pâtissier, la pâtissière

El vendedor, la vendedora / El dependiente, la dependiente

Le vendeur, la vendeuse

El ingeniero, la ingeniera

L’ingénieur(e)

El arquitecto, la arquitecta

L’architecte

El albañil, la albañil

Le maçon

El fontanero, la fontanera

Le plombier, la plombière

El pintor, la pintora

Le peintre, la peintre

El informático, la informática

L’informaticien(ne)

El mecánico, la mecánica

Le mécanicien, la mécanicienne

El abogado, la abogada

L’avocat(e)

El juez, la jueza

Le juge, la juge

El músico, la música

Le musicien, la musicienne

El médico, la médica

Le docteur, la docteure

El enfermero, la enfermera

L’inﬁrmier, l’inﬁrmière

El partero, la partera

Le sage-femme homme, la sage-femme

El cirujano, la cirujana

Le chirurgien, la chirurgienne

El dentista, la dentista

Le dentiste, la dentiste

El pediatra, la pediatra

Le pédiatre, la pédiatre

Exemple
El futuro profesional
Chema: Pepa, ¿qué te gustaría ser, más tarde?
Algo artístico como ¿pintora?, o ¿música?
Pepa: ¿Yo? No… A mí me encanta la medicina.
Seré cirujana, o médica. O mejor aún, pediatra. ¿Y
tú?
Chema: Yo quiero ser fontanero, o albañil. Nada
más interesante como la construcción de las
casas.

NOTION Les-professions
Vocabulaire - L’école et le
monde professionnel

Les relations sociales
Les types de relations sociales
La amistad

L’amitié

El amigo, la amiga

L’ami(e)

El compañero, la compañera

Le camarade, la camarade

La familia

La famille

La pareja

Le couple

El enemigo, la enemiga

L’ennemi(e)

La pandilla

La bande de copains

El vecino, la vecina

Le voisin, la voisine

Exemple
Mis amigos y mi pareja son lo más importante de
mi vida.

NOTION Les-types-de-relations-sociales
Vocabulaire - L’école et le
monde professionnel

Les relations sociales
Les démonstrations
El abrazo

L’accolade

Abrazar

Embrasser, enlacer

El beso

Le baiser

Besar

Embrasser

El apretón de manos

La poignée de main

Saludar

Saluer

Exemple
Para saludar alguien que no conozco le doy un
apretón de manos.

NOTION Les-démonstrations
Vocabulaire - L’école et le
monde professionnel

Les relations sociales
Les sentiments
El ﬂechazo

Le coup de foudre

Enamorarse

Tomber amoureux

Llevarse bien con

S’entendre bien avec

Llevarse mal con

S’entendre mal avec

Preocuparse por

S’inquiéter pour

Echar de menos

Manquer

Tener celos

Être jaloux

Exemple
Le echo de menos, nos llevamos muy bien.

NOTION Les-sentiments
Vocabulaire - L’école et le
monde professionnel

Les nouvelles technologies
Les appareils
El ordenador

L’ordinateur

El (teléfono) móvil

Le (téléphone) portable

La tableta, la pantalla táctil

La tablette

La pantalla

L’écran

El ratón

La souris

El teclado

Le clavier

La impresora

L’imprimante

El cable

Le câble

Exemple
La pantalla de mi ordenador es pequeña.

NOTION Les-appareils
Vocabulaire - L’école et le
monde professionnel

Les nouvelles technologies
L’usage virtuel
El correo electrónico

Le courriel, l’e-mail

El perﬁl

Le proﬁl

El usuario

L’usager

La contraseña, la clave

Le mot de passe

La aplicación

L’application

La red

Le réseau, internet

La red social

Le réseau social

La página web

La site internet

El blog

Le blog

La memoria virtual

La mémoire virtuelle

Navegar en internet

Surfer sur internet

Descargar

Télécharger

Guardar

Enregistrer

Exemple
He cambiado la contraseña de mi correo
electrónico.

NOTION Lusage-virtuel
Vocabulaire - L’école et le
monde professionnel

L’école et le matériel scolaire
Les lieux
La escuela

L’école

El colegio

Le collège

El aula

La salle de classe

El comedor

La cantine

El patio de recreo

La cour de récréation

La enfermería

L’inﬁrmerie

El gimnasio

Le gymnase

La cancha de deporte

Le terrain de sport

El vestuario

Le vestiaire

El baño

Les toilettes

La secretaría

Le secrétariat

La biblioteca

Le CDI

El laboratorio

Le laboratoire

La residencia, el internado

L’internat

El anﬁteatro

L’amphithéâtre

Exemple
En el colegio
• María: Pablito, te espero a las cinco en el
patio, ¿vale?
• Pablito: De acuerdo. De momento estoy en
la biblioteca, y antes tengo que pasar por la
secretaría.
• María:

Pues si quieres nos podemos

encontrar frente a la enfermería, que está al
lado.

NOTION Les-lieux
Vocabulaire - L’école et le
monde professionnel

L’école et le matériel scolaire
Le matériel scolaire
La mochila

Le sac à dos

El cuaderno

Le cahier

El libro

Le livre

El lápiz

Le crayon

El sacapuntas

Le taille-crayon

El bolígrafo

Le stylo

El estuche

La trousse

La goma

La gomme

El pegamento

La colle

Las tijeras

Les ciseaux

El rotulador

Le feutre

El marcador

Le marqueur

El pincel

Le pinceau

El clip

Le trombone

La carpeta

La pochette

La hoja

La feuille

La pizarra

Le tableau

La tiza

La craie

La calculadora

La calculatrice

La regla

La règle

El compás

Le compas

Exemple
En clase
• Profesor: Chicos, sacad los cuadernos y
escribid la fecha de hoy. No olvidéis utilizar
el rotulador de color rojo.
• Santi: Profe, no puedo, he olvidado mi
mochila en casa. Solo llevo conmigo un libro.
• Profesor: ¿Otra vez? Bueno, María te va a
prestar una hoja y bolígrafos.

NOTION Le-matériel-scolaire
Vocabulaire - L’école et le
monde professionnel

L’école et le matériel scolaire
Les matières
La clase

Le cours

La asignatura

La matière scolaire

Las matemáticas

Les mathématiques

La historia

L’histoire

La geografía

La géographie

Las humanidades

Les sciences humaines

Las ciencias naturales, la biología

Les sciences de la vie et de la terre

La física

La physique

La química

La chimie

La música

La musique

La educación física

L’éducation physique

La ﬁlosofía

La philosophie

La lengua extranjera

La langue étrangère

La educación plástica

Les arts plastiques

La tecnología

La technologie

Exemple
Las asignaturas que más me gustan son las
matemáticas y el deporte.

NOTION Les-matières
Vocabulaire - L’école et le
monde professionnel

Les études
Les études supérieures
El bachillerato

Le baccalauréat

La universidad

L’université

La escuela técnica

L’école technique

La formación profesional

La formation professionnelle

La matrícula

L’inscription

La beca

La bourse

Los exámenes

Les examens

Estudiar

Étudier

Aprobar un examen

Réussir un examen

Suspender un examen

Échouer à un examen

Exemple
Mi hermano tiene muchas horas de clase en la
universidad de Madrid.

NOTION Les-études-supérieures
Vocabulaire - L’école et le
monde professionnel

Les études
Les diplômes, les cursus
La licencia

La licence

El máster

Le master

La licenciatura

La maitrise

El diploma

Le diplôme

La carrera

Les études

El título

Le titre

El doctorado

Le doctorat

Exemple
Quiero hacer una carrera universitaria.

NOTION Les-diplômes-les-cursus
Vocabulaire - L’école et le
monde professionnel

Les professions

El profesor, la profesora

Le professeur, la professeure

El maestro, la maestra

L’instituteur, l’institutrice

El panadero, la panadera

Le boulanger, la boulangère

El camarero, la camarera

Le serveur, la serveuse

El carnicero, la carnicera

Le boucher, la bouchère

El cajero, la cajera

Le caissier, la caissière

El cocinero, la cocinera

Le cuisinier, la cuisinière

El pastelero, la pastelera

Le pâtissier, la pâtissière

El vendedor, la vendedora / El dependiente, la dependiente

Le vendeur, la vendeuse

El ingeniero, la ingeniera

L’ingénieur(e)

El arquitecto, la arquitecta

L’architecte

El albañil, la albañil

Le maçon

El fontanero, la fontanera

Le plombier, la plombière

El pintor, la pintora

Le peintre, la peintre

El informático, la informática

L’informaticien(ne)

El mecánico, la mecánica

Le mécanicien, la mécanicienne

El abogado, la abogada

L’avocat(e)

El juez, la jueza

Le juge, la juge

El músico, la música

Le musicien, la musicienne

El médico, la médica

Le docteur, la docteure

El enfermero, la enfermera

L’inﬁrmier, l’inﬁrmière

El partero, la partera

Le sage-femme homme, la sage-femme

El cirujano, la cirujana

Le chirurgien, la chirurgienne

El dentista, la dentista

Le dentiste, la dentiste

El pediatra, la pediatra

Le pédiatre, la pédiatre

Exemple
El futuro profesional
Chema: Pepa, ¿qué te gustaría ser, más tarde?
Algo artístico como ¿pintora?, o ¿música?
Pepa: ¿Yo? No… A mí me encanta la medicina.
Seré cirujana, o médica. O mejor aún, pediatra. ¿Y
tú?
Chema: Yo quiero ser fontanero, o albañil. Nada
más interesante como la construcción de las
casas.

NOTION Les-professions
Vocabulaire - L’école et le
monde professionnel

Les relations sociales
Les types de relations sociales
La amistad

L’amitié

El amigo, la amiga

L’ami(e)

El compañero, la compañera

Le camarade, la camarade

La familia

La famille

La pareja

Le couple

El enemigo, la enemiga

L’ennemi(e)

La pandilla

La bande de copains

El vecino, la vecina

Le voisin, la voisine

Exemple
Mis amigos y mi pareja son lo más importante de
mi vida.

NOTION Les-types-de-relations-sociales
Vocabulaire - L’école et le
monde professionnel

Les relations sociales
Les démonstrations
El abrazo

L’accolade

Abrazar

Embrasser, enlacer

El beso

Le baiser

Besar

Embrasser

El apretón de manos

La poignée de main

Saludar

Saluer

Exemple
Para saludar alguien que no conozco le doy un
apretón de manos.

NOTION Les-démonstrations
Vocabulaire - L’école et le
monde professionnel

Les relations sociales
Les sentiments
El ﬂechazo

Le coup de foudre

Enamorarse

Tomber amoureux

Llevarse bien con

S’entendre bien avec

Llevarse mal con

S’entendre mal avec

Preocuparse por

S’inquiéter pour

Echar de menos

Manquer

Tener celos

Être jaloux

Exemple
Le echo de menos, nos llevamos muy bien.

NOTION Les-sentiments
Vocabulaire - L’école et le
monde professionnel

Les nouvelles technologies
Les appareils
El ordenador

L’ordinateur

El (teléfono) móvil

Le (téléphone) portable

La tableta, la pantalla táctil

La tablette

La pantalla

L’écran

El ratón

La souris

El teclado

Le clavier

La impresora

L’imprimante

El cable

Le câble

Exemple
La pantalla de mi ordenador es pequeña.

NOTION Les-appareils
Vocabulaire - L’école et le
monde professionnel

Les nouvelles technologies
L’usage virtuel
El correo electrónico

Le courriel, l’e-mail

El perﬁl

Le proﬁl

El usuario

L’usager

La contraseña, la clave

Le mot de passe

La aplicación

L’application

La red

Le réseau, internet

La red social

Le réseau social

La página web

La site internet

El blog

Le blog

La memoria virtual

La mémoire virtuelle

Navegar en internet

Surfer sur internet

Descargar

Télécharger

Guardar

Enregistrer

Exemple
He cambiado la contraseña de mi correo
electrónico.

NOTION Lusage-virtuel
Vocabulaire - L’école et le
monde professionnel

L’école et le matériel scolaire
Les lieux
La escuela

L’école

El colegio

Le collège

El aula

La salle de classe

El comedor

La cantine

El patio de recreo

La cour de récréation

La enfermería

L’inﬁrmerie

El gimnasio

Le gymnase

La cancha de deporte

Le terrain de sport

El vestuario

Le vestiaire

El baño

Les toilettes

La secretaría

Le secrétariat

La biblioteca

Le CDI

El laboratorio

Le laboratoire

La residencia, el internado

L’internat

El anﬁteatro

L’amphithéâtre

Exemple
En el colegio
• María: Pablito, te espero a las cinco en el
patio, ¿vale?
• Pablito: De acuerdo. De momento estoy en
la biblioteca, y antes tengo que pasar por la
secretaría.
• María:

Pues si quieres nos podemos

encontrar frente a la enfermería, que está al
lado.

NOTION Les-lieux
Vocabulaire - L’école et le
monde professionnel

L’école et le matériel scolaire
Le matériel scolaire
La mochila

Le sac à dos

El cuaderno

Le cahier

El libro

Le livre

El lápiz

Le crayon

El sacapuntas

Le taille-crayon

El bolígrafo

Le stylo

El estuche

La trousse

La goma

La gomme

El pegamento

La colle

Las tijeras

Les ciseaux

El rotulador

Le feutre

El marcador

Le marqueur

El pincel

Le pinceau

El clip

Le trombone

La carpeta

La pochette

La hoja

La feuille

La pizarra

Le tableau

La tiza

La craie

La calculadora

La calculatrice

La regla

La règle

El compás

Le compas

Exemple
En clase
• Profesor: Chicos, sacad los cuadernos y
escribid la fecha de hoy. No olvidéis utilizar
el rotulador de color rojo.
• Santi: Profe, no puedo, he olvidado mi
mochila en casa. Solo llevo conmigo un libro.
• Profesor: ¿Otra vez? Bueno, María te va a
prestar una hoja y bolígrafos.

NOTION Le-matériel-scolaire
Vocabulaire - L’école et le
monde professionnel

L’école et le matériel scolaire
Les matières
La clase

Le cours

La asignatura

La matière scolaire

Las matemáticas

Les mathématiques

La historia

L’histoire

La geografía

La géographie

Las humanidades

Les sciences humaines

Las ciencias naturales, la biología

Les sciences de la vie et de la terre

La física

La physique

La química

La chimie

La música

La musique

La educación física

L’éducation physique

La ﬁlosofía

La philosophie

La lengua extranjera

La langue étrangère

La educación plástica

Les arts plastiques

La tecnología

La technologie

Exemple
Las asignaturas que más me gustan son las
matemáticas y el deporte.

NOTION Les-matières
Vocabulaire - L’école et le
monde professionnel

Les études
Les études supérieures
El bachillerato

Le baccalauréat

La universidad

L’université

La escuela técnica

L’école technique

La formación profesional

La formation professionnelle

La matrícula

L’inscription

La beca

La bourse

Los exámenes

Les examens

Estudiar

Étudier

Aprobar un examen

Réussir un examen

Suspender un examen

Échouer à un examen

Exemple
Mi hermano tiene muchas horas de clase en la
universidad de Madrid.

NOTION Les-études-supérieures
Vocabulaire - L’école et le
monde professionnel

Les études
Les diplômes, les cursus
La licencia

La licence

El máster

Le master

La licenciatura

La maitrise

El diploma

Le diplôme

La carrera

Les études

El título

Le titre

El doctorado

Le doctorat

Exemple
Quiero hacer una carrera universitaria.

NOTION Les-diplômes-les-cursus
Vocabulaire - L’école et le
monde professionnel

Les professions

El profesor, la profesora

Le professeur, la professeure

El maestro, la maestra

L’instituteur, l’institutrice

El panadero, la panadera

Le boulanger, la boulangère

El camarero, la camarera

Le serveur, la serveuse

El carnicero, la carnicera

Le boucher, la bouchère

El cajero, la cajera

Le caissier, la caissière

El cocinero, la cocinera

Le cuisinier, la cuisinière

El pastelero, la pastelera

Le pâtissier, la pâtissière

El vendedor, la vendedora / El dependiente, la dependiente

Le vendeur, la vendeuse

El ingeniero, la ingeniera

L’ingénieur(e)

El arquitecto, la arquitecta

L’architecte

El albañil, la albañil

Le maçon

El fontanero, la fontanera

Le plombier, la plombière

El pintor, la pintora

Le peintre, la peintre

El informático, la informática

L’informaticien(ne)

El mecánico, la mecánica

Le mécanicien, la mécanicienne

El abogado, la abogada

L’avocat(e)

El juez, la jueza

Le juge, la juge

El músico, la música

Le musicien, la musicienne

El médico, la médica

Le docteur, la docteure

El enfermero, la enfermera

L’inﬁrmier, l’inﬁrmière

El partero, la partera

Le sage-femme homme, la sage-femme

El cirujano, la cirujana

Le chirurgien, la chirurgienne

El dentista, la dentista

Le dentiste, la dentiste

El pediatra, la pediatra

Le pédiatre, la pédiatre

Exemple
El futuro profesional
Chema: Pepa, ¿qué te gustaría ser, más tarde?
Algo artístico como ¿pintora?, o ¿música?
Pepa: ¿Yo? No… A mí me encanta la medicina.
Seré cirujana, o médica. O mejor aún, pediatra. ¿Y
tú?
Chema: Yo quiero ser fontanero, o albañil. Nada
más interesante como la construcción de las
casas.

NOTION Les-professions
Vocabulaire - L’école et le
monde professionnel

Les relations sociales
Les types de relations sociales
La amistad

L’amitié

El amigo, la amiga

L’ami(e)

El compañero, la compañera

Le camarade, la camarade

La familia

La famille

La pareja

Le couple

El enemigo, la enemiga

L’ennemi(e)

La pandilla

La bande de copains

El vecino, la vecina

Le voisin, la voisine

Exemple
Mis amigos y mi pareja son lo más importante de
mi vida.

NOTION Les-types-de-relations-sociales
Vocabulaire - L’école et le
monde professionnel

Les relations sociales
Les démonstrations
El abrazo

L’accolade

Abrazar

Embrasser, enlacer

El beso

Le baiser

Besar

Embrasser

El apretón de manos

La poignée de main

Saludar

Saluer

Exemple
Para saludar alguien que no conozco le doy un
apretón de manos.

NOTION Les-démonstrations
Vocabulaire - L’école et le
monde professionnel

Les relations sociales
Les sentiments
El ﬂechazo

Le coup de foudre

Enamorarse

Tomber amoureux

Llevarse bien con

S’entendre bien avec

Llevarse mal con

S’entendre mal avec

Preocuparse por

S’inquiéter pour

Echar de menos

Manquer

Tener celos

Être jaloux

Exemple
Le echo de menos, nos llevamos muy bien.

NOTION Les-sentiments
Vocabulaire - L’école et le
monde professionnel

Les nouvelles technologies
Les appareils
El ordenador

L’ordinateur

El (teléfono) móvil

Le (téléphone) portable

La tableta, la pantalla táctil

La tablette

La pantalla

L’écran

El ratón

La souris

El teclado

Le clavier

La impresora

L’imprimante

El cable

Le câble

Exemple
La pantalla de mi ordenador es pequeña.

NOTION Les-appareils
Vocabulaire - L’école et le
monde professionnel

Les nouvelles technologies
L’usage virtuel
El correo electrónico

Le courriel, l’e-mail

El perﬁl

Le proﬁl

El usuario

L’usager

La contraseña, la clave

Le mot de passe

La aplicación

L’application

La red

Le réseau, internet

La red social

Le réseau social

La página web

La site internet

El blog

Le blog

La memoria virtual

La mémoire virtuelle

Navegar en internet

Surfer sur internet

Descargar

Télécharger

Guardar

Enregistrer

Exemple
He cambiado la contraseña de mi correo
electrónico.

NOTION Lusage-virtuel
