Aménager le territoire

L’organisation du territoire français

SCHEMA Lorganisation-du-territoire-français
Aménager le territoire

Schéma de révision

SCHEMA Schéma-de-révision
Aménager le territoire

Aménagement

Action qui consiste à transformer l’espace pour
répondre aux besoins des habitants.

DEFINITION Aménagement
Aménager le territoire

Collectivités territoriales

Régions, départements et communes. Elles ont
des compétences (pour exercer un pouvoir de
décision) et un budget.

DEFINITION Collectivités-territoriales
Aménager le territoire

Décentralisation

Transfert de certaines compétences de l’État
aux collectivités locales ou territoriales. Régions,
départements

et

communes

peuvent

prendre des décisions au niveau local.

alors

DEFINITION Décentralisation
Aménager le territoire

Métropole

Grande ville où sont concentrés les lieux de
pouvoir politique, économique et culturel.

DEFINITION Métropole
Aménager le territoire

Pôle de compétitivité

Regroupement d’entreprises,

de centres de

recherche et d’universités, encouragés par l’État.

DEFINITION Pôle-de-compétitivité
Aménager le territoire

Politique d’aménagement du
territoire

Ensemble

de

mesures

pour

organiser

la

répartition des hommes, des activités et des
équipements sur un espace.

DEFINITION Politique-d’aménagement-du-territoire
Aménager le territoire

Politique de la ville

Politique mise en place par les pouvoirs publics
dans les quartiers défavorisés pour réduire les
inégalités.

DEFINITION Politique-de-la-ville
Aménager le territoire

Un territoire marqué par des
inégalités croissantes
Les grandes aires urbaines
• les activités économiques se concentrent dans les
grandes aires urbaines
• ces

espaces

reçoivent

la

majorité

des

investissements qui se traduit dans les paysages :
– aménagement des quartiers d’affaires
– aménagement des voies de communication

Des espaces inégalement intégrés sur tout le
territoire
• de nombreux espaces sont moins intégrés au
reste du territoire
• les inégalités se voient à l’échelle nationale et locale
:
– les habitants des zones rurales n’ont pas accès
au mêmes services et infrastructures que ceux
des villes (ex. : hopitaux, universités)

Des mesures pour corriger ces inégalités
• mesure à l’échelle locale : politique de la ville (ex.
: rénovations urbaines)
• maintien des services dans les campagnes (ex. :
postes, services de proximité)

LEÇON Un-territoire-marqué-par-des-inégalités-croissantes
Aménager le territoire

Les nouveaux acteurs de
l’aménagement
L’État et les collectivités
• l’aménagement du territoire a longtemps été
déﬁni par l’État
• depuis les lois de décentralisation (1982-1983), le
rôle de l’État diminue :
– le

poids

des

acteurs

locaux

(régions,

départements, communes) se renforce
– ils déploient des stratégies pour attirer les
habitants et les entreprises et développer
des projets culturels (ex. : Le Louvre-Lens)

L’Union européenne
• mise

en

place

d’une politique

de

subventions pour les territoires en difﬁculté
• mise en place d’aides pour développer l’emploi

LEÇON Les-nouveaux-acteurs-de-l’aménagement
Aménager le territoire

Du rééquilibrage à la compétition
entre les territoires
Répondre à la concurrence entre territoires
• la mondialisation renforce les inégalités entre les
territoires
• les aménagements favorisent la concentration des
activités dans un nombre réduit de lieux (ex.

:

métropoles, pôles de compétitivité) aﬁn de faire
face à la concurrence internationale
• les

territoires

les

mieux intégrés dans la

mondialisation sont ceux qui sont les mieux
reliés aux réseaux de transport

Aménager pour réduire les inégalités
• pour ne pas délaisser le reste du territoire national,
les acteurs de l’aménagement essayent de fournir
le meilleur accès possible pour tous aux services
publics :
– ex. : accès au soins

LEÇON Du-rééquilibrage-à-la-compétition-entre-les-territoires
Aménager le territoire

Paris
Capitale politique et économique de la France. Depuis
la mise en place des lois de décentralisation en 1982, ce
n’est plus le seul centre d’impulsion de l’aménagement
du territoire. Beaucoup d’administrations et d’écoles ont
été installées dans les métropoles de province (ex. : à
Strasbourg, Lyon, etc.).

LIEU Paris
Aménager le territoire

Ile-de-France
La plus riche et la plus peuplée des régions françaises.
Elle a un important poids économique en France et
en Europe.

La région connait néanmoins de fortes

inégalités socio-spatiales.

LIEU Ile-de-France
Aménager le territoire

Nombre de régions françaises en
2016
13

NOMBRE Nombre-de-régions-françaises-en-2016
Aménager le territoire

Nombre de communes françaises en
2016
36 682

NOMBRE Nombre-de-communes-françaises-en-2016
Aménager le territoire

Nombre de départements français
en 2016
101

NOMBRE Nombre-de-départements-français-en-2016
Aménager le territoire

Nombre de quartiers défavorisés
aidés par la politique de la ville en
2015
1300

NOMBRE Nombre-de-quartiers-défavorisés-aidés-par-la-politique-de-la-ville-en-2015
Aménager le territoire

L’organisation du territoire français

SCHEMA Lorganisation-du-territoire-français
Aménager le territoire

Schéma de révision

SCHEMA Schéma-de-révision
Aménager le territoire

Aménagement

Action qui consiste à transformer l’espace pour
répondre aux besoins des habitants.

DEFINITION Aménagement
Aménager le territoire

Collectivités territoriales

Régions, départements et communes. Elles ont
des compétences (pour exercer un pouvoir de
décision) et un budget.

DEFINITION Collectivités-territoriales
Aménager le territoire

Décentralisation

Transfert de certaines compétences de l’État
aux collectivités locales ou territoriales. Régions,
départements

et

communes

peuvent

prendre des décisions au niveau local.

alors

DEFINITION Décentralisation
Aménager le territoire

Métropole

Grande ville où sont concentrés les lieux de
pouvoir politique, économique et culturel.

DEFINITION Métropole
Aménager le territoire

Pôle de compétitivité

Regroupement d’entreprises,

de centres de

recherche et d’universités, encouragés par l’État.

DEFINITION Pôle-de-compétitivité
Aménager le territoire

Politique d’aménagement du
territoire

Ensemble

de

mesures

pour

organiser

la

répartition des hommes, des activités et des
équipements sur un espace.

DEFINITION Politique-d’aménagement-du-territoire
Aménager le territoire

Politique de la ville

Politique mise en place par les pouvoirs publics
dans les quartiers défavorisés pour réduire les
inégalités.

DEFINITION Politique-de-la-ville
Aménager le territoire

Un territoire marqué par des
inégalités croissantes
Les grandes aires urbaines
• les activités économiques se concentrent dans les
grandes aires urbaines
• ces

espaces

reçoivent

la

majorité

des

investissements qui se traduit dans les paysages :
– aménagement des quartiers d’affaires
– aménagement des voies de communication

Des espaces inégalement intégrés sur tout le
territoire
• de nombreux espaces sont moins intégrés au
reste du territoire
• les inégalités se voient à l’échelle nationale et locale
:
– les habitants des zones rurales n’ont pas accès
au mêmes services et infrastructures que ceux
des villes (ex. : hopitaux, universités)

Des mesures pour corriger ces inégalités
• mesure à l’échelle locale : politique de la ville (ex.
: rénovations urbaines)
• maintien des services dans les campagnes (ex. :
postes, services de proximité)

LEÇON Un-territoire-marqué-par-des-inégalités-croissantes
Aménager le territoire

Les nouveaux acteurs de
l’aménagement
L’État et les collectivités
• l’aménagement du territoire a longtemps été
déﬁni par l’État
• depuis les lois de décentralisation (1982-1983), le
rôle de l’État diminue :
– le

poids

des

acteurs

locaux

(régions,

départements, communes) se renforce
– ils déploient des stratégies pour attirer les
habitants et les entreprises et développer
des projets culturels (ex. : Le Louvre-Lens)

L’Union européenne
• mise

en

place

d’une politique

de

subventions pour les territoires en difﬁculté
• mise en place d’aides pour développer l’emploi

LEÇON Les-nouveaux-acteurs-de-l’aménagement
Aménager le territoire

Du rééquilibrage à la compétition
entre les territoires
Répondre à la concurrence entre territoires
• la mondialisation renforce les inégalités entre les
territoires
• les aménagements favorisent la concentration des
activités dans un nombre réduit de lieux (ex.

:

métropoles, pôles de compétitivité) aﬁn de faire
face à la concurrence internationale
• les

territoires

les

mieux intégrés dans la

mondialisation sont ceux qui sont les mieux
reliés aux réseaux de transport

Aménager pour réduire les inégalités
• pour ne pas délaisser le reste du territoire national,
les acteurs de l’aménagement essayent de fournir
le meilleur accès possible pour tous aux services
publics :
– ex. : accès au soins

LEÇON Du-rééquilibrage-à-la-compétition-entre-les-territoires
Aménager le territoire

Paris
Capitale politique et économique de la France. Depuis
la mise en place des lois de décentralisation en 1982, ce
n’est plus le seul centre d’impulsion de l’aménagement
du territoire. Beaucoup d’administrations et d’écoles ont
été installées dans les métropoles de province (ex. : à
Strasbourg, Lyon, etc.).

LIEU Paris
Aménager le territoire

Ile-de-France
La plus riche et la plus peuplée des régions françaises.
Elle a un important poids économique en France et
en Europe.

La région connait néanmoins de fortes

inégalités socio-spatiales.

LIEU Ile-de-France
Aménager le territoire

Nombre de régions françaises en
2016
13

NOMBRE Nombre-de-régions-françaises-en-2016
Aménager le territoire

Nombre de communes françaises en
2016
36 682

NOMBRE Nombre-de-communes-françaises-en-2016
Aménager le territoire

Nombre de départements français
en 2016
101

NOMBRE Nombre-de-départements-français-en-2016
Aménager le territoire

Nombre de quartiers défavorisés
aidés par la politique de la ville en
2015
1300

NOMBRE Nombre-de-quartiers-défavorisés-aidés-par-la-politique-de-la-ville-en-2015
Aménager le territoire

L’organisation du territoire français

SCHEMA Lorganisation-du-territoire-français
Aménager le territoire

Schéma de révision

SCHEMA Schéma-de-révision
Aménager le territoire

Aménagement

Action qui consiste à transformer l’espace pour
répondre aux besoins des habitants.

DEFINITION Aménagement
Aménager le territoire

Collectivités territoriales

Régions, départements et communes. Elles ont
des compétences (pour exercer un pouvoir de
décision) et un budget.

DEFINITION Collectivités-territoriales
Aménager le territoire

Décentralisation

Transfert de certaines compétences de l’État
aux collectivités locales ou territoriales. Régions,
départements

et

communes

peuvent

prendre des décisions au niveau local.

alors

DEFINITION Décentralisation
Aménager le territoire

Métropole

Grande ville où sont concentrés les lieux de
pouvoir politique, économique et culturel.

DEFINITION Métropole
Aménager le territoire

Pôle de compétitivité

Regroupement d’entreprises,

de centres de

recherche et d’universités, encouragés par l’État.

DEFINITION Pôle-de-compétitivité
Aménager le territoire

Politique d’aménagement du
territoire

Ensemble

de

mesures

pour

organiser

la

répartition des hommes, des activités et des
équipements sur un espace.

DEFINITION Politique-d’aménagement-du-territoire
Aménager le territoire

Politique de la ville

Politique mise en place par les pouvoirs publics
dans les quartiers défavorisés pour réduire les
inégalités.

DEFINITION Politique-de-la-ville
Aménager le territoire

Un territoire marqué par des
inégalités croissantes
Les grandes aires urbaines
• les activités économiques se concentrent dans les
grandes aires urbaines
• ces

espaces

reçoivent

la

majorité

des

investissements qui se traduit dans les paysages :
– aménagement des quartiers d’affaires
– aménagement des voies de communication

Des espaces inégalement intégrés sur tout le
territoire
• de nombreux espaces sont moins intégrés au
reste du territoire
• les inégalités se voient à l’échelle nationale et locale
:
– les habitants des zones rurales n’ont pas accès
au mêmes services et infrastructures que ceux
des villes (ex. : hopitaux, universités)

Des mesures pour corriger ces inégalités
• mesure à l’échelle locale : politique de la ville (ex.
: rénovations urbaines)
• maintien des services dans les campagnes (ex. :
postes, services de proximité)

LEÇON Un-territoire-marqué-par-des-inégalités-croissantes
Aménager le territoire

Les nouveaux acteurs de
l’aménagement
L’État et les collectivités
• l’aménagement du territoire a longtemps été
déﬁni par l’État
• depuis les lois de décentralisation (1982-1983), le
rôle de l’État diminue :
– le

poids

des

acteurs

locaux

(régions,

départements, communes) se renforce
– ils déploient des stratégies pour attirer les
habitants et les entreprises et développer
des projets culturels (ex. : Le Louvre-Lens)

L’Union européenne
• mise

en

place

d’une politique

de

subventions pour les territoires en difﬁculté
• mise en place d’aides pour développer l’emploi

LEÇON Les-nouveaux-acteurs-de-l’aménagement
Aménager le territoire

Du rééquilibrage à la compétition
entre les territoires
Répondre à la concurrence entre territoires
• la mondialisation renforce les inégalités entre les
territoires
• les aménagements favorisent la concentration des
activités dans un nombre réduit de lieux (ex.

:

métropoles, pôles de compétitivité) aﬁn de faire
face à la concurrence internationale
• les

territoires

les

mieux intégrés dans la

mondialisation sont ceux qui sont les mieux
reliés aux réseaux de transport

Aménager pour réduire les inégalités
• pour ne pas délaisser le reste du territoire national,
les acteurs de l’aménagement essayent de fournir
le meilleur accès possible pour tous aux services
publics :
– ex. : accès au soins

LEÇON Du-rééquilibrage-à-la-compétition-entre-les-territoires
Aménager le territoire

Paris
Capitale politique et économique de la France. Depuis
la mise en place des lois de décentralisation en 1982, ce
n’est plus le seul centre d’impulsion de l’aménagement
du territoire. Beaucoup d’administrations et d’écoles ont
été installées dans les métropoles de province (ex. : à
Strasbourg, Lyon, etc.).

LIEU Paris
Aménager le territoire

Ile-de-France
La plus riche et la plus peuplée des régions françaises.
Elle a un important poids économique en France et
en Europe.

La région connait néanmoins de fortes

inégalités socio-spatiales.

LIEU Ile-de-France
Aménager le territoire

Nombre de régions françaises en
2016
13

NOMBRE Nombre-de-régions-françaises-en-2016
Aménager le territoire

Nombre de communes françaises en
2016
36 682

NOMBRE Nombre-de-communes-françaises-en-2016
Aménager le territoire

Nombre de départements français
en 2016
101

NOMBRE Nombre-de-départements-français-en-2016
Aménager le territoire

Nombre de quartiers défavorisés
aidés par la politique de la ville en
2015
1300

NOMBRE Nombre-de-quartiers-défavorisés-aidés-par-la-politique-de-la-ville-en-2015
Aménager le territoire

L’organisation du territoire français

SCHEMA Lorganisation-du-territoire-français
Aménager le territoire

Schéma de révision

SCHEMA Schéma-de-révision
Aménager le territoire

Aménagement

Action qui consiste à transformer l’espace pour
répondre aux besoins des habitants.

DEFINITION Aménagement
Aménager le territoire

Collectivités territoriales

Régions, départements et communes. Elles ont
des compétences (pour exercer un pouvoir de
décision) et un budget.

DEFINITION Collectivités-territoriales
Aménager le territoire

Décentralisation

Transfert de certaines compétences de l’État
aux collectivités locales ou territoriales. Régions,
départements

et

communes

peuvent

prendre des décisions au niveau local.

alors

DEFINITION Décentralisation
Aménager le territoire

Métropole

Grande ville où sont concentrés les lieux de
pouvoir politique, économique et culturel.

DEFINITION Métropole
Aménager le territoire

Pôle de compétitivité

Regroupement d’entreprises,

de centres de

recherche et d’universités, encouragés par l’État.

DEFINITION Pôle-de-compétitivité
Aménager le territoire

Politique d’aménagement du
territoire

Ensemble

de

mesures

pour

organiser

la

répartition des hommes, des activités et des
équipements sur un espace.

DEFINITION Politique-d’aménagement-du-territoire
Aménager le territoire

Politique de la ville

Politique mise en place par les pouvoirs publics
dans les quartiers défavorisés pour réduire les
inégalités.

DEFINITION Politique-de-la-ville
Aménager le territoire

Un territoire marqué par des
inégalités croissantes
Les grandes aires urbaines
• les activités économiques se concentrent dans les
grandes aires urbaines
• ces

espaces

reçoivent

la

majorité

des

investissements qui se traduit dans les paysages :
– aménagement des quartiers d’affaires
– aménagement des voies de communication

Des espaces inégalement intégrés sur tout le
territoire
• de nombreux espaces sont moins intégrés au
reste du territoire
• les inégalités se voient à l’échelle nationale et locale
:
– les habitants des zones rurales n’ont pas accès
au mêmes services et infrastructures que ceux
des villes (ex. : hopitaux, universités)

Des mesures pour corriger ces inégalités
• mesure à l’échelle locale : politique de la ville (ex.
: rénovations urbaines)
• maintien des services dans les campagnes (ex. :
postes, services de proximité)

LEÇON Un-territoire-marqué-par-des-inégalités-croissantes
Aménager le territoire

Les nouveaux acteurs de
l’aménagement
L’État et les collectivités
• l’aménagement du territoire a longtemps été
déﬁni par l’État
• depuis les lois de décentralisation (1982-1983), le
rôle de l’État diminue :
– le

poids

des

acteurs

locaux

(régions,

départements, communes) se renforce
– ils déploient des stratégies pour attirer les
habitants et les entreprises et développer
des projets culturels (ex. : Le Louvre-Lens)

L’Union européenne
• mise

en

place

d’une politique

de

subventions pour les territoires en difﬁculté
• mise en place d’aides pour développer l’emploi

LEÇON Les-nouveaux-acteurs-de-l’aménagement
Aménager le territoire

Du rééquilibrage à la compétition
entre les territoires
Répondre à la concurrence entre territoires
• la mondialisation renforce les inégalités entre les
territoires
• les aménagements favorisent la concentration des
activités dans un nombre réduit de lieux (ex.

:

métropoles, pôles de compétitivité) aﬁn de faire
face à la concurrence internationale
• les

territoires

les

mieux intégrés dans la

mondialisation sont ceux qui sont les mieux
reliés aux réseaux de transport

Aménager pour réduire les inégalités
• pour ne pas délaisser le reste du territoire national,
les acteurs de l’aménagement essayent de fournir
le meilleur accès possible pour tous aux services
publics :
– ex. : accès au soins

LEÇON Du-rééquilibrage-à-la-compétition-entre-les-territoires
Aménager le territoire

Paris
Capitale politique et économique de la France. Depuis
la mise en place des lois de décentralisation en 1982, ce
n’est plus le seul centre d’impulsion de l’aménagement
du territoire. Beaucoup d’administrations et d’écoles ont
été installées dans les métropoles de province (ex. : à
Strasbourg, Lyon, etc.).

LIEU Paris
Aménager le territoire

Ile-de-France
La plus riche et la plus peuplée des régions françaises.
Elle a un important poids économique en France et
en Europe.

La région connait néanmoins de fortes

inégalités socio-spatiales.

LIEU Ile-de-France
Aménager le territoire

Nombre de régions françaises en
2016
13

NOMBRE Nombre-de-régions-françaises-en-2016
Aménager le territoire

Nombre de communes françaises en
2016
36 682

NOMBRE Nombre-de-communes-françaises-en-2016
Aménager le territoire

Nombre de départements français
en 2016
101

NOMBRE Nombre-de-départements-français-en-2016
Aménager le territoire

Nombre de quartiers défavorisés
aidés par la politique de la ville en
2015
1300

NOMBRE Nombre-de-quartiers-défavorisés-aidés-par-la-politique-de-la-ville-en-2015
