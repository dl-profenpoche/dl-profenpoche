Les
activités

impacts
humaines

des
sur

l’environnement

Une simulation des conséquences
du réchauffement climatique sur la
végétation en montagne

Quand le climat se réchauffe, le milieu est modiﬁé. Les
espèces les plus sensibles ne peuvent survivre que dans
un milieu et une zone climatique particuliers. Si ces
espèces ne peuvent pas se déplacer rapidement quand
leur milieu se modiﬁe, elles disparaissent.

SCHEMA Une-simulation-des-conséquences-du-réchauffement-climatique-sur-la-végétation-en-montagne
Les
activités

impacts
humaines

des
sur

l’environnement

Les zones de pleine mer où la pêche
est pratiquée et les conséquences
sur les espèces marines.

Les zones de pêche sont toujours plus étendues en
surface et en profondeur. Seulement 10 % des poissons
pêchés sont consommés. Le reste correspond à des
prises accidentelles ou non exploitables. Par exemple,
pour 1 kg de crevettes pêchées, 8 kg d’organismes
marins sont rejetés en mer.

SCHEMA Les-zones-de-pleine-mer-où-la-pêche-est-pratiquée-et-les-conséquences-sur-les-espèces-marines
Les
activités

impacts
humaines

l’environnement

L’essentiel en schéma

des
sur

SCHEMA L’essentiel-en-schéma
Les
activités

impacts
humaines

l’environnement

Les idées importantes

des
sur

SCHEMA Les-idées-importantes
Les
activités

impacts
humaines

des
sur

l’environnement

Biodiversité

Ensemble des êtres vivants et des écosystèmes
présents sur Terre.

DEFINITION Biodiversité
Les
activités

impacts
humaines

des
sur

l’environnement

Écosystème

Ensemble formé par des êtres vivants, le milieu
dans lequel ils vivent et leurs interactions.

DEFINITION Écosystème
Les
activités

impacts
humaines

des
sur

l’environnement

Espèce invasive

Espèce introduite dans un milieu et qui nuit à la
biodiversité locale.

DEFINITION Espèce-invasive
Les

impacts

activités

des

humaines

sur

l’environnement

Surpêche

Exploitation

trop

océaniques (ex.

intense

des

ressources

: poissons) par rapport à leur

vitesse de renouvellement.

DEFINITION Surpêche
Les
activités

impacts
humaines

des
sur

l’environnement

L’impact du réchauffement
climatique sur la biodiversité
Des perturbations de la biodiversité
• les modiﬁcations rapides du climat liées aux
activités humaines entrainent la migration des
espèces et la disparition de beaucoup d’espèces
qui ne parviennent pas à s’acclimater
• une des conséquences est la modiﬁcation de la
biodiversité

Des

mesures

pour

diminuer

cette

perturbation
• quelques exemples de mesures pour atténuer
cette perturbation des milieux :
– limitation de notre consommation d’énergie
– développement des énergies renouvelables
– aménagement du territoire
– agriculture raisonnée
– parcs naturels
– sauvegarder les zones humides

LEÇON L’impact-du-réchauffement-climatique-sur-la-biodiversité
Les

impacts

activités

des

humaines

sur

l’environnement

Les espèces envahissantes et la
biodiversité locale
Les espèces invasives (ou envahissantes)
• une espèce invasive est une espèce introduite dans
un milieu, qui nuit à la biodiversité locale
– ex. : le moustique tigre, présent dans le sud de
la France
– ex. : la renouée du Japon, une plante d’origine
asiatique introduite en Europe au XIXe siècle
et qui s’est répandue partout en Europe
• elles

sont

introduites

accidentellement

ou

volontairement par les activités humaines hors de
leur zone de répartition naturelle

Les

espèces

envahissantes

modiﬁent

l’écosystème
• ces plantes invasives entrent en compétition avec
les espèces locales par différents moyens :
– en les empêchant de se nourrir ou de se
reproduire
– en agissant en prédateur
• des mesures de lutte active et de surveillance des
zones à risque permettent de limiter l’arrivée de
nouvelles espèces invasives
– réglementation

de

l’import

des

exotiques
– surveillance des populations invasives

espèces

LEÇON Les-espèces-envahissantes-et-la-biodiversité-locale
Les
activités

impacts

des

humaines

sur

l’environnement

Le milieu marin et la surpêche
La consommation de produits de la mer
• les ressources océaniques sont exploitées plus vite
qu’elles ne se renouvellent : c’est la surpêche
• la surpêche a pour conséquence de diminuer
la

biodiversité,

de perturber

les

chaines

alimentaires et pourrait poser des problèmes
pour nourrir l’humanité

Des solutions sont envisagées
• des mesures visant à compenser ces effets négatifs
existent :
– développement de l’aquaculture
– réglementation mondiale de la pêche (quotas
de pêche)
– création de réserves maritimes protégées
– interdiction de la pêche en eau profonde

LEÇON Le-milieu-marin-et-la-surpêche
Les
activités

impacts
humaines

des
sur

l’environnement

Une simulation des conséquences
du réchauffement climatique sur la
végétation en montagne

Quand le climat se réchauffe, le milieu est modiﬁé. Les
espèces les plus sensibles ne peuvent survivre que dans
un milieu et une zone climatique particuliers. Si ces
espèces ne peuvent pas se déplacer rapidement quand
leur milieu se modiﬁe, elles disparaissent.

SCHEMA Une-simulation-des-conséquences-du-réchauffement-climatique-sur-la-végétation-en-montagne
Les
activités

impacts
humaines

des
sur

l’environnement

Les zones de pleine mer où la pêche
est pratiquée et les conséquences
sur les espèces marines.

Les zones de pêche sont toujours plus étendues en
surface et en profondeur. Seulement 10 % des poissons
pêchés sont consommés. Le reste correspond à des
prises accidentelles ou non exploitables. Par exemple,
pour 1 kg de crevettes pêchées, 8 kg d’organismes
marins sont rejetés en mer.

SCHEMA Les-zones-de-pleine-mer-où-la-pêche-est-pratiquée-et-les-conséquences-sur-les-espèces-marines
Les
activités

impacts
humaines

l’environnement

L’essentiel en schéma

des
sur

SCHEMA L’essentiel-en-schéma
Les
activités

impacts
humaines

l’environnement

Les idées importantes

des
sur

SCHEMA Les-idées-importantes
Les
activités

impacts
humaines

des
sur

l’environnement

Biodiversité

Ensemble des êtres vivants et des écosystèmes
présents sur Terre.

DEFINITION Biodiversité
Les
activités

impacts
humaines

des
sur

l’environnement

Écosystème

Ensemble formé par des êtres vivants, le milieu
dans lequel ils vivent et leurs interactions.

DEFINITION Écosystème
Les
activités

impacts
humaines

des
sur

l’environnement

Espèce invasive

Espèce introduite dans un milieu et qui nuit à la
biodiversité locale.

DEFINITION Espèce-invasive
Les

impacts

activités

des

humaines

sur

l’environnement

Surpêche

Exploitation

trop

océaniques (ex.

intense

des

ressources

: poissons) par rapport à leur

vitesse de renouvellement.

DEFINITION Surpêche
Les
activités

impacts
humaines

des
sur

l’environnement

L’impact du réchauffement
climatique sur la biodiversité
Des perturbations de la biodiversité
• les modiﬁcations rapides du climat liées aux
activités humaines entrainent la migration des
espèces et la disparition de beaucoup d’espèces
qui ne parviennent pas à s’acclimater
• une des conséquences est la modiﬁcation de la
biodiversité

Des

mesures

pour

diminuer

cette

perturbation
• quelques exemples de mesures pour atténuer
cette perturbation des milieux :
– limitation de notre consommation d’énergie
– développement des énergies renouvelables
– aménagement du territoire
– agriculture raisonnée
– parcs naturels
– sauvegarder les zones humides

LEÇON L’impact-du-réchauffement-climatique-sur-la-biodiversité
Les

impacts

activités

des

humaines

sur

l’environnement

Les espèces envahissantes et la
biodiversité locale
Les espèces invasives (ou envahissantes)
• une espèce invasive est une espèce introduite dans
un milieu, qui nuit à la biodiversité locale
– ex. : le moustique tigre, présent dans le sud de
la France
– ex. : la renouée du Japon, une plante d’origine
asiatique introduite en Europe au XIXe siècle
et qui s’est répandue partout en Europe
• elles

sont

introduites

accidentellement

ou

volontairement par les activités humaines hors de
leur zone de répartition naturelle

Les

espèces

envahissantes

modiﬁent

l’écosystème
• ces plantes invasives entrent en compétition avec
les espèces locales par différents moyens :
– en les empêchant de se nourrir ou de se
reproduire
– en agissant en prédateur
• des mesures de lutte active et de surveillance des
zones à risque permettent de limiter l’arrivée de
nouvelles espèces invasives
– réglementation

de

l’import

des

exotiques
– surveillance des populations invasives

espèces

LEÇON Les-espèces-envahissantes-et-la-biodiversité-locale
Les
activités

impacts

des

humaines

sur

l’environnement

Le milieu marin et la surpêche
La consommation de produits de la mer
• les ressources océaniques sont exploitées plus vite
qu’elles ne se renouvellent : c’est la surpêche
• la surpêche a pour conséquence de diminuer
la

biodiversité,

de perturber

les

chaines

alimentaires et pourrait poser des problèmes
pour nourrir l’humanité

Des solutions sont envisagées
• des mesures visant à compenser ces effets négatifs
existent :
– développement de l’aquaculture
– réglementation mondiale de la pêche (quotas
de pêche)
– création de réserves maritimes protégées
– interdiction de la pêche en eau profonde

LEÇON Le-milieu-marin-et-la-surpêche
Les
activités

impacts
humaines

des
sur

l’environnement

Une simulation des conséquences
du réchauffement climatique sur la
végétation en montagne

Quand le climat se réchauffe, le milieu est modiﬁé. Les
espèces les plus sensibles ne peuvent survivre que dans
un milieu et une zone climatique particuliers. Si ces
espèces ne peuvent pas se déplacer rapidement quand
leur milieu se modiﬁe, elles disparaissent.

SCHEMA Une-simulation-des-conséquences-du-réchauffement-climatique-sur-la-végétation-en-montagne
Les
activités

impacts
humaines

des
sur

l’environnement

Les zones de pleine mer où la pêche
est pratiquée et les conséquences
sur les espèces marines.

Les zones de pêche sont toujours plus étendues en
surface et en profondeur. Seulement 10 % des poissons
pêchés sont consommés. Le reste correspond à des
prises accidentelles ou non exploitables. Par exemple,
pour 1 kg de crevettes pêchées, 8 kg d’organismes
marins sont rejetés en mer.

SCHEMA Les-zones-de-pleine-mer-où-la-pêche-est-pratiquée-et-les-conséquences-sur-les-espèces-marines
Les
activités

impacts
humaines

l’environnement

L’essentiel en schéma

des
sur

SCHEMA L’essentiel-en-schéma
Les
activités

impacts
humaines

l’environnement

Les idées importantes

des
sur

SCHEMA Les-idées-importantes
Les
activités

impacts
humaines

des
sur

l’environnement

Biodiversité

Ensemble des êtres vivants et des écosystèmes
présents sur Terre.

DEFINITION Biodiversité
Les
activités

impacts
humaines

des
sur

l’environnement

Écosystème

Ensemble formé par des êtres vivants, le milieu
dans lequel ils vivent et leurs interactions.

DEFINITION Écosystème
Les
activités

impacts
humaines

des
sur

l’environnement

Espèce invasive

Espèce introduite dans un milieu et qui nuit à la
biodiversité locale.

DEFINITION Espèce-invasive
Les

impacts

activités

des

humaines

sur

l’environnement

Surpêche

Exploitation

trop

océaniques (ex.

intense

des

ressources

: poissons) par rapport à leur

vitesse de renouvellement.

DEFINITION Surpêche
Les
activités

impacts
humaines

des
sur

l’environnement

L’impact du réchauffement
climatique sur la biodiversité
Des perturbations de la biodiversité
• les modiﬁcations rapides du climat liées aux
activités humaines entrainent la migration des
espèces et la disparition de beaucoup d’espèces
qui ne parviennent pas à s’acclimater
• une des conséquences est la modiﬁcation de la
biodiversité

Des

mesures

pour

diminuer

cette

perturbation
• quelques exemples de mesures pour atténuer
cette perturbation des milieux :
– limitation de notre consommation d’énergie
– développement des énergies renouvelables
– aménagement du territoire
– agriculture raisonnée
– parcs naturels
– sauvegarder les zones humides

LEÇON L’impact-du-réchauffement-climatique-sur-la-biodiversité
Les

impacts

activités

des

humaines

sur

l’environnement

Les espèces envahissantes et la
biodiversité locale
Les espèces invasives (ou envahissantes)
• une espèce invasive est une espèce introduite dans
un milieu, qui nuit à la biodiversité locale
– ex. : le moustique tigre, présent dans le sud de
la France
– ex. : la renouée du Japon, une plante d’origine
asiatique introduite en Europe au XIXe siècle
et qui s’est répandue partout en Europe
• elles

sont

introduites

accidentellement

ou

volontairement par les activités humaines hors de
leur zone de répartition naturelle

Les

espèces

envahissantes

modiﬁent

l’écosystème
• ces plantes invasives entrent en compétition avec
les espèces locales par différents moyens :
– en les empêchant de se nourrir ou de se
reproduire
– en agissant en prédateur
• des mesures de lutte active et de surveillance des
zones à risque permettent de limiter l’arrivée de
nouvelles espèces invasives
– réglementation

de

l’import

des

exotiques
– surveillance des populations invasives

espèces

LEÇON Les-espèces-envahissantes-et-la-biodiversité-locale
Les
activités

impacts

des

humaines

sur

l’environnement

Le milieu marin et la surpêche
La consommation de produits de la mer
• les ressources océaniques sont exploitées plus vite
qu’elles ne se renouvellent : c’est la surpêche
• la surpêche a pour conséquence de diminuer
la

biodiversité,

de perturber

les

chaines

alimentaires et pourrait poser des problèmes
pour nourrir l’humanité

Des solutions sont envisagées
• des mesures visant à compenser ces effets négatifs
existent :
– développement de l’aquaculture
– réglementation mondiale de la pêche (quotas
de pêche)
– création de réserves maritimes protégées
– interdiction de la pêche en eau profonde

LEÇON Le-milieu-marin-et-la-surpêche
Les
activités

impacts
humaines

des
sur

l’environnement

Une simulation des conséquences
du réchauffement climatique sur la
végétation en montagne

Quand le climat se réchauffe, le milieu est modiﬁé. Les
espèces les plus sensibles ne peuvent survivre que dans
un milieu et une zone climatique particuliers. Si ces
espèces ne peuvent pas se déplacer rapidement quand
leur milieu se modiﬁe, elles disparaissent.

SCHEMA Une-simulation-des-conséquences-du-réchauffement-climatique-sur-la-végétation-en-montagne
Les
activités

impacts
humaines

des
sur

l’environnement

Les zones de pleine mer où la pêche
est pratiquée et les conséquences
sur les espèces marines.

Les zones de pêche sont toujours plus étendues en
surface et en profondeur. Seulement 10 % des poissons
pêchés sont consommés. Le reste correspond à des
prises accidentelles ou non exploitables. Par exemple,
pour 1 kg de crevettes pêchées, 8 kg d’organismes
marins sont rejetés en mer.

SCHEMA Les-zones-de-pleine-mer-où-la-pêche-est-pratiquée-et-les-conséquences-sur-les-espèces-marines
Les
activités

impacts
humaines

l’environnement

L’essentiel en schéma

des
sur

SCHEMA L’essentiel-en-schéma
Les
activités

impacts
humaines

l’environnement

Les idées importantes

des
sur

SCHEMA Les-idées-importantes
Les
activités

impacts
humaines

des
sur

l’environnement

Biodiversité

Ensemble des êtres vivants et des écosystèmes
présents sur Terre.

DEFINITION Biodiversité
Les
activités

impacts
humaines

des
sur

l’environnement

Écosystème

Ensemble formé par des êtres vivants, le milieu
dans lequel ils vivent et leurs interactions.

DEFINITION Écosystème
Les
activités

impacts
humaines

des
sur

l’environnement

Espèce invasive

Espèce introduite dans un milieu et qui nuit à la
biodiversité locale.

DEFINITION Espèce-invasive
Les

impacts

activités

des

humaines

sur

l’environnement

Surpêche

Exploitation

trop

océaniques (ex.

intense

des

ressources

: poissons) par rapport à leur

vitesse de renouvellement.

DEFINITION Surpêche
Les
activités

impacts
humaines

des
sur

l’environnement

L’impact du réchauffement
climatique sur la biodiversité
Des perturbations de la biodiversité
• les modiﬁcations rapides du climat liées aux
activités humaines entrainent la migration des
espèces et la disparition de beaucoup d’espèces
qui ne parviennent pas à s’acclimater
• une des conséquences est la modiﬁcation de la
biodiversité

Des

mesures

pour

diminuer

cette

perturbation
• quelques exemples de mesures pour atténuer
cette perturbation des milieux :
– limitation de notre consommation d’énergie
– développement des énergies renouvelables
– aménagement du territoire
– agriculture raisonnée
– parcs naturels
– sauvegarder les zones humides

LEÇON L’impact-du-réchauffement-climatique-sur-la-biodiversité
Les

impacts

activités

des

humaines

sur

l’environnement

Les espèces envahissantes et la
biodiversité locale
Les espèces invasives (ou envahissantes)
• une espèce invasive est une espèce introduite dans
un milieu, qui nuit à la biodiversité locale
– ex. : le moustique tigre, présent dans le sud de
la France
– ex. : la renouée du Japon, une plante d’origine
asiatique introduite en Europe au XIXe siècle
et qui s’est répandue partout en Europe
• elles

sont

introduites

accidentellement

ou

volontairement par les activités humaines hors de
leur zone de répartition naturelle

Les

espèces

envahissantes

modiﬁent

l’écosystème
• ces plantes invasives entrent en compétition avec
les espèces locales par différents moyens :
– en les empêchant de se nourrir ou de se
reproduire
– en agissant en prédateur
• des mesures de lutte active et de surveillance des
zones à risque permettent de limiter l’arrivée de
nouvelles espèces invasives
– réglementation

de

l’import

des

exotiques
– surveillance des populations invasives

espèces

LEÇON Les-espèces-envahissantes-et-la-biodiversité-locale
Les
activités

impacts

des

humaines

sur

l’environnement

Le milieu marin et la surpêche
La consommation de produits de la mer
• les ressources océaniques sont exploitées plus vite
qu’elles ne se renouvellent : c’est la surpêche
• la surpêche a pour conséquence de diminuer
la

biodiversité,

de perturber

les

chaines

alimentaires et pourrait poser des problèmes
pour nourrir l’humanité

Des solutions sont envisagées
• des mesures visant à compenser ces effets négatifs
existent :
– développement de l’aquaculture
– réglementation mondiale de la pêche (quotas
de pêche)
– création de réserves maritimes protégées
– interdiction de la pêche en eau profonde

LEÇON Le-milieu-marin-et-la-surpêche
