La

réponse

immunitaire

adaptative face aux infections

L’essentiel en schéma

SCHEMA L’essentiel-en-schéma
La

réponse

immunitaire

adaptative face aux infections

Les idées importantes

SCHEMA Les-idées-importantes
La

réponse

immunitaire

adaptative face aux infections

La coopération entre les acteurs du
système immunitaire

Les cellules de la réponse immunitaire innée et
adaptative communiquent entre elles et ont des tâches
complémentaires.

SCHEMA La-coopération-entre-les-acteurs-du-système-immunitaire
La

réponse

immunitaire

adaptative face aux infections

Les étapes dans la recherche d’un
vaccin

Lorsqu’un virus ou une bactérie responsable d’une
maladie est identiﬁé, il est possible d’élaborer un vaccin.
De nombreuses étapes de contrôle sont nécessaires
pour que ce vaccin soit sans danger pour l’organisme.

SCHEMA Les-étapes-dans-la-recherche-d’un-vaccin
La

réponse

immunitaire

adaptative face aux infections

Anticorps

Molécule produite par un lymphocyte B dans le
sang, capable de reconnaitre spéciﬁquement un
antigène.

DEFINITION Anticorps
La

réponse

immunitaire

adaptative face aux infections

Antigène

Molécule

étrangère

spéciﬁquement
immunitaire.

à

l’organisme

reconnue

par

le

qui

est

système

DEFINITION Antigène
La

réponse

immunitaire

adaptative face aux infections

Lymphocyte

Catégorie

de

leucocytes

spéciﬁquement à un antigène.

s’attaquant

DEFINITION Lymphocyte
La

réponse

immunitaire

adaptative face aux infections

Mémoire immunitaire

Réserve

de

lymphocytes

prêts

à

répondre

rapidement lors d’une nouvelle rencontre de
l’antigène.

DEFINITION Mémoire-immunitaire
La

réponse

immunitaire

adaptative face aux infections

Séropositive

Se dit d’une personne produisant des anticorps
contre un antigène donné.

DEFINITION Séropositive
La

réponse

immunitaire

adaptative face aux infections

Sérum

Partie liquide du sang contenant des molécules
produites par les cellules du sang, dont les
anticorps.

DEFINITION Sérum
La

réponse

immunitaire

adaptative face aux infections

Sida

Syndrome d’immuno déﬁcience acquise.

DEFINITION Sida
La

réponse

immunitaire

adaptative face aux infections

Vaccin

Produit contenant un antigène capable de
provoquer une réaction immunitaire spéciﬁque
de manière à protéger un individu contre une
rencontre ultérieure avec cet antigène.

DEFINITION Vaccin
La

réponse

immunitaire

adaptative face aux infections

Chronologie

1796

•

Découverte du principe de la
vaccination par Jenner

1885

•

Invention du vaccin contre la
rage par Pasteur

1983

•

Découverte du VIH par
Montagnier

CHRONO La-réponse-immunitaire-adaptative-face-aux-infections
La

réponse

immunitaire

adaptative face aux infections

Les anticorps neutralisent les
microorganismes
Les

anticorps

sont

produits

par

les

lymphocytes B
• le

sang

contient

des

cellules

(hématies

et

leucocytes) et du liquide (le sérum)
• les anticorps sont des molécules présentes dans
le sérum et sont produits par une catégorie de
leucocytes : les lymphocytes B
• chaque type de lymphocyte B ne produit qu’une
seule forme d’anticorps après avoir rencontré le
pathogène qui lui correspond

Les

anticorps

neutralisent

les

microorganismes pathogènes
• les anticorps contenus dans le sérum neutralisent
les microorganismes et favorisent leur phagocytose
• les

anticorps

se

ﬁxent

à

des

molécules

caractéristiques d’un microorganisme appelées
antigènes
• chaque

type

d’anticorps

reconnait

spéciﬁquement un élément étranger caractérisé
par son antigène

Certaines maladies peuvent être soignées en
utilisant des anticorps
• les anticorps sont très spéciﬁques de leur antigène
(ils ne reconnaissent que leur antigène cible) et
sont peu toxiques (ils n’agissent que sur leur
microorganisme cible)
– ces caractéristiques en font des pistes de
traitement intéressantes
– ex. : on tente de développer des traitements à
base d’anticorps pour lutter contre les cancers

LEÇON Les-anticorps-neutralisent-les-microorganismes
La

réponse

immunitaire

adaptative face aux infections

Les lymphocytes et les phagocytes
coopèrent
L’élimination des cellules infectées par des
lymphocytes
• les lymphocytes T, autres acteurs de la réponse
adaptative, reconnaissent et éliminent les cellules
de l’organisme qui sont infectées par un virus
• chaque type de lymphocyte T reconnait les cellules
infectées par un seul type de microorganisme
– ex.

:

les lymphocytes T anti-grippe

reconnaissent les cellules infectées par le
virus de la grippe
• le Virus de l’Immunodéﬁcience Humaine (VIH)
infecte et détruit des lymphocytes T
– il est responsable du sida (Syndrome de
l’ImmunoDéﬁcience
infectieuse

Acquise),

affaiblissant

le

maladie
système

immunitaire et qui peut causer la mort

Les différentes réponses immunitaires
• la phagocytose est la première réponse du système
immunitaire face à une infection, il s’agit d’une
réponse rapide et non spéciﬁque
– si elle ne sufﬁt pas, une deuxième réponse se
déclenche : la réponse adaptative
• la réponse adaptative est plus lente à se mettre en
place

La mémoire immunitaire
• une partie de ces lymphocytes reste en réserve
dans l’organisme et peut rapidement agir contre
ce pathogène plus tard :
immunitaire

c’est la mémoire

LEÇON Les-lymphocytes-et-les-phagocytes-coopèrent
La

réponse

immunitaire

adaptative face aux infections

La vaccination : un enjeu individuel
et collectif
Le principe de la vaccination
• au XVIIIe siècle, on a découvert qu’être exposé à un
virus peu dangereux mais qui ressemble à un virus
dangereux a un effet protecteur contre ce dernier :
c’est le principe de la vaccination
• un vaccin contient un antigène : il déclenche une
réponse immunitaire mais n’est pas dangereux
• lors de la vaccination, on produit des anticorps
et des lymphocytes T contre cet antigène :

si

l’on rencontre cet antigène plus tard, la réponse
immunitaire adaptative est plus forte et plus rapide
(mémoire immunitaire)

L’efﬁcacité de la vaccination
• les campagnes de vaccination permettent de
réduire considérablement le nombre de personnes
atteintes par des maladies infectieuses
• si le nombre de personnes vaccinées pour une
maladie est très élevé, il est possible d’éradiquer
cette maladie :

on ne se vaccine donc pas

seulement pour soi mais aussi pour toute la société

La conception d’un vaccin
• pour être utilisé, un vaccin doit protéger de la
maladie sans la provoquer

LEÇON La-vaccination-un-enjeu-individuel-et-collectif
La

réponse

immunitaire

adaptative face aux infections

L’essentiel en schéma

SCHEMA L’essentiel-en-schéma
La

réponse

immunitaire

adaptative face aux infections

Les idées importantes

SCHEMA Les-idées-importantes
La

réponse

immunitaire

adaptative face aux infections

La coopération entre les acteurs du
système immunitaire

Les cellules de la réponse immunitaire innée et
adaptative communiquent entre elles et ont des tâches
complémentaires.

SCHEMA La-coopération-entre-les-acteurs-du-système-immunitaire
La

réponse

immunitaire

adaptative face aux infections

Les étapes dans la recherche d’un
vaccin

Lorsqu’un virus ou une bactérie responsable d’une
maladie est identiﬁé, il est possible d’élaborer un vaccin.
De nombreuses étapes de contrôle sont nécessaires
pour que ce vaccin soit sans danger pour l’organisme.

SCHEMA Les-étapes-dans-la-recherche-d’un-vaccin
La

réponse

immunitaire

adaptative face aux infections

Anticorps

Molécule produite par un lymphocyte B dans le
sang, capable de reconnaitre spéciﬁquement un
antigène.

DEFINITION Anticorps
La

réponse

immunitaire

adaptative face aux infections

Antigène

Molécule

étrangère

spéciﬁquement
immunitaire.

à

l’organisme

reconnue

par

le

qui

est

système

DEFINITION Antigène
La

réponse

immunitaire

adaptative face aux infections

Lymphocyte

Catégorie

de

leucocytes

spéciﬁquement à un antigène.

s’attaquant

DEFINITION Lymphocyte
La

réponse

immunitaire

adaptative face aux infections

Mémoire immunitaire

Réserve

de

lymphocytes

prêts

à

répondre

rapidement lors d’une nouvelle rencontre de
l’antigène.

DEFINITION Mémoire-immunitaire
La

réponse

immunitaire

adaptative face aux infections

Séropositive

Se dit d’une personne produisant des anticorps
contre un antigène donné.

DEFINITION Séropositive
La

réponse

immunitaire

adaptative face aux infections

Sérum

Partie liquide du sang contenant des molécules
produites par les cellules du sang, dont les
anticorps.

DEFINITION Sérum
La

réponse

immunitaire

adaptative face aux infections

Sida

Syndrome d’immuno déﬁcience acquise.

DEFINITION Sida
La

réponse

immunitaire

adaptative face aux infections

Vaccin

Produit contenant un antigène capable de
provoquer une réaction immunitaire spéciﬁque
de manière à protéger un individu contre une
rencontre ultérieure avec cet antigène.

DEFINITION Vaccin
La

réponse

immunitaire

adaptative face aux infections

Chronologie

1796

•

Découverte du principe de la
vaccination par Jenner

1885

•

Invention du vaccin contre la
rage par Pasteur

1983

•

Découverte du VIH par
Montagnier

CHRONO La-réponse-immunitaire-adaptative-face-aux-infections
La

réponse

immunitaire

adaptative face aux infections

Les anticorps neutralisent les
microorganismes
Les

anticorps

sont

produits

par

les

lymphocytes B
• le

sang

contient

des

cellules

(hématies

et

leucocytes) et du liquide (le sérum)
• les anticorps sont des molécules présentes dans
le sérum et sont produits par une catégorie de
leucocytes : les lymphocytes B
• chaque type de lymphocyte B ne produit qu’une
seule forme d’anticorps après avoir rencontré le
pathogène qui lui correspond

Les

anticorps

neutralisent

les

microorganismes pathogènes
• les anticorps contenus dans le sérum neutralisent
les microorganismes et favorisent leur phagocytose
• les

anticorps

se

ﬁxent

à

des

molécules

caractéristiques d’un microorganisme appelées
antigènes
• chaque

type

d’anticorps

reconnait

spéciﬁquement un élément étranger caractérisé
par son antigène

Certaines maladies peuvent être soignées en
utilisant des anticorps
• les anticorps sont très spéciﬁques de leur antigène
(ils ne reconnaissent que leur antigène cible) et
sont peu toxiques (ils n’agissent que sur leur
microorganisme cible)
– ces caractéristiques en font des pistes de
traitement intéressantes
– ex. : on tente de développer des traitements à
base d’anticorps pour lutter contre les cancers

LEÇON Les-anticorps-neutralisent-les-microorganismes
La

réponse

immunitaire

adaptative face aux infections

Les lymphocytes et les phagocytes
coopèrent
L’élimination des cellules infectées par des
lymphocytes
• les lymphocytes T, autres acteurs de la réponse
adaptative, reconnaissent et éliminent les cellules
de l’organisme qui sont infectées par un virus
• chaque type de lymphocyte T reconnait les cellules
infectées par un seul type de microorganisme
– ex.

:

les lymphocytes T anti-grippe

reconnaissent les cellules infectées par le
virus de la grippe
• le Virus de l’Immunodéﬁcience Humaine (VIH)
infecte et détruit des lymphocytes T
– il est responsable du sida (Syndrome de
l’ImmunoDéﬁcience
infectieuse

Acquise),

affaiblissant

le

maladie
système

immunitaire et qui peut causer la mort

Les différentes réponses immunitaires
• la phagocytose est la première réponse du système
immunitaire face à une infection, il s’agit d’une
réponse rapide et non spéciﬁque
– si elle ne sufﬁt pas, une deuxième réponse se
déclenche : la réponse adaptative
• la réponse adaptative est plus lente à se mettre en
place

La mémoire immunitaire
• une partie de ces lymphocytes reste en réserve
dans l’organisme et peut rapidement agir contre
ce pathogène plus tard :
immunitaire

c’est la mémoire

LEÇON Les-lymphocytes-et-les-phagocytes-coopèrent
La

réponse

immunitaire

adaptative face aux infections

La vaccination : un enjeu individuel
et collectif
Le principe de la vaccination
• au XVIIIe siècle, on a découvert qu’être exposé à un
virus peu dangereux mais qui ressemble à un virus
dangereux a un effet protecteur contre ce dernier :
c’est le principe de la vaccination
• un vaccin contient un antigène : il déclenche une
réponse immunitaire mais n’est pas dangereux
• lors de la vaccination, on produit des anticorps
et des lymphocytes T contre cet antigène :

si

l’on rencontre cet antigène plus tard, la réponse
immunitaire adaptative est plus forte et plus rapide
(mémoire immunitaire)

L’efﬁcacité de la vaccination
• les campagnes de vaccination permettent de
réduire considérablement le nombre de personnes
atteintes par des maladies infectieuses
• si le nombre de personnes vaccinées pour une
maladie est très élevé, il est possible d’éradiquer
cette maladie :

on ne se vaccine donc pas

seulement pour soi mais aussi pour toute la société

La conception d’un vaccin
• pour être utilisé, un vaccin doit protéger de la
maladie sans la provoquer

LEÇON La-vaccination-un-enjeu-individuel-et-collectif
La

réponse

immunitaire

adaptative face aux infections

L’essentiel en schéma

SCHEMA L’essentiel-en-schéma
La

réponse

immunitaire

adaptative face aux infections

Les idées importantes

SCHEMA Les-idées-importantes
La

réponse

immunitaire

adaptative face aux infections

La coopération entre les acteurs du
système immunitaire

Les cellules de la réponse immunitaire innée et
adaptative communiquent entre elles et ont des tâches
complémentaires.

SCHEMA La-coopération-entre-les-acteurs-du-système-immunitaire
La

réponse

immunitaire

adaptative face aux infections

Les étapes dans la recherche d’un
vaccin

Lorsqu’un virus ou une bactérie responsable d’une
maladie est identiﬁé, il est possible d’élaborer un vaccin.
De nombreuses étapes de contrôle sont nécessaires
pour que ce vaccin soit sans danger pour l’organisme.

SCHEMA Les-étapes-dans-la-recherche-d’un-vaccin
La

réponse

immunitaire

adaptative face aux infections

Anticorps

Molécule produite par un lymphocyte B dans le
sang, capable de reconnaitre spéciﬁquement un
antigène.

DEFINITION Anticorps
La

réponse

immunitaire

adaptative face aux infections

Antigène

Molécule

étrangère

spéciﬁquement
immunitaire.

à

l’organisme

reconnue

par

le

qui

est

système

DEFINITION Antigène
La

réponse

immunitaire

adaptative face aux infections

Lymphocyte

Catégorie

de

leucocytes

spéciﬁquement à un antigène.

s’attaquant

DEFINITION Lymphocyte
La

réponse

immunitaire

adaptative face aux infections

Mémoire immunitaire

Réserve

de

lymphocytes

prêts

à

répondre

rapidement lors d’une nouvelle rencontre de
l’antigène.

DEFINITION Mémoire-immunitaire
La

réponse

immunitaire

adaptative face aux infections

Séropositive

Se dit d’une personne produisant des anticorps
contre un antigène donné.

DEFINITION Séropositive
La

réponse

immunitaire

adaptative face aux infections

Sérum

Partie liquide du sang contenant des molécules
produites par les cellules du sang, dont les
anticorps.

DEFINITION Sérum
La

réponse

immunitaire

adaptative face aux infections

Sida

Syndrome d’immuno déﬁcience acquise.

DEFINITION Sida
La

réponse

immunitaire

adaptative face aux infections

Vaccin

Produit contenant un antigène capable de
provoquer une réaction immunitaire spéciﬁque
de manière à protéger un individu contre une
rencontre ultérieure avec cet antigène.

DEFINITION Vaccin
La

réponse

immunitaire

adaptative face aux infections

Chronologie

1796

•

Découverte du principe de la
vaccination par Jenner

1885

•

Invention du vaccin contre la
rage par Pasteur

1983

•

Découverte du VIH par
Montagnier

CHRONO La-réponse-immunitaire-adaptative-face-aux-infections
La

réponse

immunitaire

adaptative face aux infections

Les anticorps neutralisent les
microorganismes
Les

anticorps

sont

produits

par

les

lymphocytes B
• le

sang

contient

des

cellules

(hématies

et

leucocytes) et du liquide (le sérum)
• les anticorps sont des molécules présentes dans
le sérum et sont produits par une catégorie de
leucocytes : les lymphocytes B
• chaque type de lymphocyte B ne produit qu’une
seule forme d’anticorps après avoir rencontré le
pathogène qui lui correspond

Les

anticorps

neutralisent

les

microorganismes pathogènes
• les anticorps contenus dans le sérum neutralisent
les microorganismes et favorisent leur phagocytose
• les

anticorps

se

ﬁxent

à

des

molécules

caractéristiques d’un microorganisme appelées
antigènes
• chaque

type

d’anticorps

reconnait

spéciﬁquement un élément étranger caractérisé
par son antigène

Certaines maladies peuvent être soignées en
utilisant des anticorps
• les anticorps sont très spéciﬁques de leur antigène
(ils ne reconnaissent que leur antigène cible) et
sont peu toxiques (ils n’agissent que sur leur
microorganisme cible)
– ces caractéristiques en font des pistes de
traitement intéressantes
– ex. : on tente de développer des traitements à
base d’anticorps pour lutter contre les cancers

LEÇON Les-anticorps-neutralisent-les-microorganismes
La

réponse

immunitaire

adaptative face aux infections

Les lymphocytes et les phagocytes
coopèrent
L’élimination des cellules infectées par des
lymphocytes
• les lymphocytes T, autres acteurs de la réponse
adaptative, reconnaissent et éliminent les cellules
de l’organisme qui sont infectées par un virus
• chaque type de lymphocyte T reconnait les cellules
infectées par un seul type de microorganisme
– ex.

:

les lymphocytes T anti-grippe

reconnaissent les cellules infectées par le
virus de la grippe
• le Virus de l’Immunodéﬁcience Humaine (VIH)
infecte et détruit des lymphocytes T
– il est responsable du sida (Syndrome de
l’ImmunoDéﬁcience
infectieuse

Acquise),

affaiblissant

le

maladie
système

immunitaire et qui peut causer la mort

Les différentes réponses immunitaires
• la phagocytose est la première réponse du système
immunitaire face à une infection, il s’agit d’une
réponse rapide et non spéciﬁque
– si elle ne sufﬁt pas, une deuxième réponse se
déclenche : la réponse adaptative
• la réponse adaptative est plus lente à se mettre en
place

La mémoire immunitaire
• une partie de ces lymphocytes reste en réserve
dans l’organisme et peut rapidement agir contre
ce pathogène plus tard :
immunitaire

c’est la mémoire

LEÇON Les-lymphocytes-et-les-phagocytes-coopèrent
La

réponse

immunitaire

adaptative face aux infections

La vaccination : un enjeu individuel
et collectif
Le principe de la vaccination
• au XVIIIe siècle, on a découvert qu’être exposé à un
virus peu dangereux mais qui ressemble à un virus
dangereux a un effet protecteur contre ce dernier :
c’est le principe de la vaccination
• un vaccin contient un antigène : il déclenche une
réponse immunitaire mais n’est pas dangereux
• lors de la vaccination, on produit des anticorps
et des lymphocytes T contre cet antigène :

si

l’on rencontre cet antigène plus tard, la réponse
immunitaire adaptative est plus forte et plus rapide
(mémoire immunitaire)

L’efﬁcacité de la vaccination
• les campagnes de vaccination permettent de
réduire considérablement le nombre de personnes
atteintes par des maladies infectieuses
• si le nombre de personnes vaccinées pour une
maladie est très élevé, il est possible d’éradiquer
cette maladie :

on ne se vaccine donc pas

seulement pour soi mais aussi pour toute la société

La conception d’un vaccin
• pour être utilisé, un vaccin doit protéger de la
maladie sans la provoquer

LEÇON La-vaccination-un-enjeu-individuel-et-collectif
La

réponse

immunitaire

adaptative face aux infections

L’essentiel en schéma

SCHEMA L’essentiel-en-schéma
La

réponse

immunitaire

adaptative face aux infections

Les idées importantes

SCHEMA Les-idées-importantes
La

réponse

immunitaire

adaptative face aux infections

La coopération entre les acteurs du
système immunitaire

Les cellules de la réponse immunitaire innée et
adaptative communiquent entre elles et ont des tâches
complémentaires.

SCHEMA La-coopération-entre-les-acteurs-du-système-immunitaire
La

réponse

immunitaire

adaptative face aux infections

Les étapes dans la recherche d’un
vaccin

Lorsqu’un virus ou une bactérie responsable d’une
maladie est identiﬁé, il est possible d’élaborer un vaccin.
De nombreuses étapes de contrôle sont nécessaires
pour que ce vaccin soit sans danger pour l’organisme.

SCHEMA Les-étapes-dans-la-recherche-d’un-vaccin
La

réponse

immunitaire

adaptative face aux infections

Anticorps

Molécule produite par un lymphocyte B dans le
sang, capable de reconnaitre spéciﬁquement un
antigène.

DEFINITION Anticorps
La

réponse

immunitaire

adaptative face aux infections

Antigène

Molécule

étrangère

spéciﬁquement
immunitaire.

à

l’organisme

reconnue

par

le

qui

est

système

DEFINITION Antigène
La

réponse

immunitaire

adaptative face aux infections

Lymphocyte

Catégorie

de

leucocytes

spéciﬁquement à un antigène.

s’attaquant

DEFINITION Lymphocyte
La

réponse

immunitaire

adaptative face aux infections

Mémoire immunitaire

Réserve

de

lymphocytes

prêts

à

répondre

rapidement lors d’une nouvelle rencontre de
l’antigène.

DEFINITION Mémoire-immunitaire
La

réponse

immunitaire

adaptative face aux infections

Séropositive

Se dit d’une personne produisant des anticorps
contre un antigène donné.

DEFINITION Séropositive
La

réponse

immunitaire

adaptative face aux infections

Sérum

Partie liquide du sang contenant des molécules
produites par les cellules du sang, dont les
anticorps.

DEFINITION Sérum
La

réponse

immunitaire

adaptative face aux infections

Sida

Syndrome d’immuno déﬁcience acquise.

DEFINITION Sida
La

réponse

immunitaire

adaptative face aux infections

Vaccin

Produit contenant un antigène capable de
provoquer une réaction immunitaire spéciﬁque
de manière à protéger un individu contre une
rencontre ultérieure avec cet antigène.

DEFINITION Vaccin
La

réponse

immunitaire

adaptative face aux infections

Chronologie

1796

•

Découverte du principe de la
vaccination par Jenner

1885

•

Invention du vaccin contre la
rage par Pasteur

1983

•

Découverte du VIH par
Montagnier

CHRONO La-réponse-immunitaire-adaptative-face-aux-infections
La

réponse

immunitaire

adaptative face aux infections

Les anticorps neutralisent les
microorganismes
Les

anticorps

sont

produits

par

les

lymphocytes B
• le

sang

contient

des

cellules

(hématies

et

leucocytes) et du liquide (le sérum)
• les anticorps sont des molécules présentes dans
le sérum et sont produits par une catégorie de
leucocytes : les lymphocytes B
• chaque type de lymphocyte B ne produit qu’une
seule forme d’anticorps après avoir rencontré le
pathogène qui lui correspond

Les

anticorps

neutralisent

les

microorganismes pathogènes
• les anticorps contenus dans le sérum neutralisent
les microorganismes et favorisent leur phagocytose
• les

anticorps

se

ﬁxent

à

des

molécules

caractéristiques d’un microorganisme appelées
antigènes
• chaque

type

d’anticorps

reconnait

spéciﬁquement un élément étranger caractérisé
par son antigène

Certaines maladies peuvent être soignées en
utilisant des anticorps
• les anticorps sont très spéciﬁques de leur antigène
(ils ne reconnaissent que leur antigène cible) et
sont peu toxiques (ils n’agissent que sur leur
microorganisme cible)
– ces caractéristiques en font des pistes de
traitement intéressantes
– ex. : on tente de développer des traitements à
base d’anticorps pour lutter contre les cancers

LEÇON Les-anticorps-neutralisent-les-microorganismes
La

réponse

immunitaire

adaptative face aux infections

Les lymphocytes et les phagocytes
coopèrent
L’élimination des cellules infectées par des
lymphocytes
• les lymphocytes T, autres acteurs de la réponse
adaptative, reconnaissent et éliminent les cellules
de l’organisme qui sont infectées par un virus
• chaque type de lymphocyte T reconnait les cellules
infectées par un seul type de microorganisme
– ex.

:

les lymphocytes T anti-grippe

reconnaissent les cellules infectées par le
virus de la grippe
• le Virus de l’Immunodéﬁcience Humaine (VIH)
infecte et détruit des lymphocytes T
– il est responsable du sida (Syndrome de
l’ImmunoDéﬁcience
infectieuse

Acquise),

affaiblissant

le

maladie
système

immunitaire et qui peut causer la mort

Les différentes réponses immunitaires
• la phagocytose est la première réponse du système
immunitaire face à une infection, il s’agit d’une
réponse rapide et non spéciﬁque
– si elle ne sufﬁt pas, une deuxième réponse se
déclenche : la réponse adaptative
• la réponse adaptative est plus lente à se mettre en
place

La mémoire immunitaire
• une partie de ces lymphocytes reste en réserve
dans l’organisme et peut rapidement agir contre
ce pathogène plus tard :
immunitaire

c’est la mémoire

LEÇON Les-lymphocytes-et-les-phagocytes-coopèrent
La

réponse

immunitaire

adaptative face aux infections

La vaccination : un enjeu individuel
et collectif
Le principe de la vaccination
• au XVIIIe siècle, on a découvert qu’être exposé à un
virus peu dangereux mais qui ressemble à un virus
dangereux a un effet protecteur contre ce dernier :
c’est le principe de la vaccination
• un vaccin contient un antigène : il déclenche une
réponse immunitaire mais n’est pas dangereux
• lors de la vaccination, on produit des anticorps
et des lymphocytes T contre cet antigène :

si

l’on rencontre cet antigène plus tard, la réponse
immunitaire adaptative est plus forte et plus rapide
(mémoire immunitaire)

L’efﬁcacité de la vaccination
• les campagnes de vaccination permettent de
réduire considérablement le nombre de personnes
atteintes par des maladies infectieuses
• si le nombre de personnes vaccinées pour une
maladie est très élevé, il est possible d’éradiquer
cette maladie :

on ne se vaccine donc pas

seulement pour soi mais aussi pour toute la société

La conception d’un vaccin
• pour être utilisé, un vaccin doit protéger de la
maladie sans la provoquer

LEÇON La-vaccination-un-enjeu-individuel-et-collectif
