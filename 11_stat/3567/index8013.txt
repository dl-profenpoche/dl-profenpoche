De la diversité génétique à la
biodiversité

La formation des gamètes

Les cellules reproductrices, ou gamètes, sont formées
lors de la méiose.

Comme toute division cellulaire,

la méiose est précédée d’une copie de chaque
chromosome qui passe d’une à deux chromatides.

SCHEMA La-formation-des-gamètes
De la diversité génétique à la
biodiversité

Le devenir des mutations

SCHEMA Le-devenir-des-mutations
De la diversité génétique à la
biodiversité

L’essentiel en schéma

SCHEMA L’essentiel-en-schéma
De la diversité génétique à la
biodiversité

Les idées importantes

SCHEMA Les-idées-importantes
De la diversité génétique à la
biodiversité

Allèle

Une version d’un gène.

DEFINITION Allèle
De la diversité génétique à la
biodiversité

Biodiversité

Ensemble des êtres vivants et des écosystèmes
présents sur Terre.

DEFINITION Biodiversité
De la diversité génétique à la
biodiversité

Brassage génétique

Création

au

hasard

de

combinaisons

de

chromosomes et donc d’allèles, au cours de
la reproduction sexuée.

DEFINITION Brassage-génétique
De la diversité génétique à la
biodiversité

Écosystème

Ensemble formé par un milieu de vie et ses êtres
vivants en interaction.

DEFINITION Écosystème
De la diversité génétique à la
biodiversité

Mutation

Modiﬁcation d’un gène pouvant entrainer la
formation d’un nouvel allèle.

DEFINITION Mutation
De la diversité génétique à la
biodiversité

Une diversité permise par la
reproduction sexuée
La diversité des gamètes formés
• la méiose a lieu dans les cellules sexuelles au niveau
des gonades (testicules ou ovaires)
• lors de la méiose, le matériel génétique est
doublé puis réparti au hasard dans quatre gamètes
(spermatozoïdes ou ovules)
• chaque gamète formé possède une combinaison
d’allèles différente car ils n’ont pas reçu les mêmes
combinaisons de chromosomes.
– on parle de brassage génétique : dans le
cas d’un être humain,
chromosomes,

celui-ci

qui possède 46
peut

fabriquer

des millions de gamètes génétiquement
différents

La diversité des possibilités de rencontre des
gamètes
• lors de la fécondation, un des gamètes mâles
rencontre un des gamètes femelles
• la combinaison d’allèles que portera le nouvel
individu dépend des allèles portés par le gamète
mâle et par le gamète femelle qui se rencontrent
au hasard
• de nombreuses combinaisons sont possibles : il
y a un second brassage génétique lors de la
fécondation

Le nombre d’individus différents possibles
est immense
• pour

une

cellule

avec

deux

paires

de

chromosomes, il y a quatre gamètes possibles
différents ; ce nombre double à chaque nouvelle
paire de chromosomes
– ex.

:

un humain possède 23 paires de

chromosomes donc cela représente jusqu’à
223 possibilités, soit plus de 8 millions de
gamètes génétiquement différents
• la

fécondation

multiplie

le

nombre

de

combinaisons possibles puisqu’elle unit au hasard
un gamète mâle et un gamète femelle
• pour un même couple de parents, il y a plus de cent
mille milliards de combinaisons possibles
• remarque : les vrais jumeaux sont identiques car ils
proviennent d’une seule et même cellule œuf

LEÇON Une-diversité-permise-par-la-reproduction-sexuée
De la diversité génétique à la
biodiversité

Les mutations peuvent créer de
nouveaux allèles
Les mutations et leur transmission
• une mutation est un phénomène rare qui consiste
en une modiﬁcation de l’ADN d’un gène (une
chance sur un milliard)
• si

la

mutation

touche

une

cellule

non

reproductrice, la mutation ne concernera que
les cellules formées par la division de cette cellule
• si la mutation a lieu dans un gamète et que ce
gamète est fécondé, alors le nouvel individu issu de
la fécondation possède un nouvel allèle
– un nouvel allèle apparait dans la population

Les conséquences des mutations
• les

mutations

peuvent

modiﬁer

des

caractéristiques biologiques des organismes
– ex. : pattes à la place des antennes chez la
mouche drosophile
• les mutations peuvent être bénéﬁques, neutres
ou néfastes pour la survie ou la reproduction des
individus concernés
– certaines mutations peuvent être à l’origine de
maladies (ex. : la mucoviscidose)
– certaines mutations apportent un avantage
par rapport aux autres (ex. : forme sombre des
papillons, moins visible par les prédateurs sur
un tronc sombre)
– certaines mutations sont neutres (ex. : lobe
des oreilles décollé ou plaqué)

LEÇON Les-mutations-peuvent-créer-de-nouveaux-allèles
De la diversité génétique à la
biodiversité

La biodiversité se retrouve à trois
niveaux
Une diversité d’écosystèmes
• un écosystème est un ensemble formé par un
milieu de vie (ex. : jardin) et ses êtres vivants (ex.
: abeille, ﬂeur) ainsi que leurs interactions 
(ex. : l’abeille butine la ﬂeur)
• il existe de nombreux écosystèmes différents sur
Terre, ex. : un jardin, la peau humaine, la forêt, etc.

La diversité des espèces
• il existe sur Terre de nombreuses espèces pouvant
appartenir à des groupes différents (ex.

:

mammifères, bactéries) et possédant chacune
des caractéristiques particulières (ex.

: régime

alimentaire, conditions optimales de survie)
• les espèces interagissent au sein d’un écosystème
– ex. : l’araignée est un prédateur de l’abeille,
l’abeille butine les ﬂeurs, etc.
• du fait de ces interactions, toutes les espèces
peuvent être affectées lorsqu’une seule espèce est
touchée
– ex.

:

un insecticide ciblant les abeilles

peut affecter les populations d’araignées et de
plantes

La diversité des allèles
• au sein d’une même espèce, il existe une grande
diversité d’individus
• cette diversité s’explique par la diversité des
combinaisons d’allèles possibles

LEÇON La-biodiversité-se-retrouve-à-trois-niveaux
De la diversité génétique à la
biodiversité

La formation des gamètes

Les cellules reproductrices, ou gamètes, sont formées
lors de la méiose.

Comme toute division cellulaire,

la méiose est précédée d’une copie de chaque
chromosome qui passe d’une à deux chromatides.

SCHEMA La-formation-des-gamètes
De la diversité génétique à la
biodiversité

Le devenir des mutations

SCHEMA Le-devenir-des-mutations
De la diversité génétique à la
biodiversité

L’essentiel en schéma

SCHEMA L’essentiel-en-schéma
De la diversité génétique à la
biodiversité

Les idées importantes

SCHEMA Les-idées-importantes
De la diversité génétique à la
biodiversité

Allèle

Une version d’un gène.

DEFINITION Allèle
De la diversité génétique à la
biodiversité

Biodiversité

Ensemble des êtres vivants et des écosystèmes
présents sur Terre.

DEFINITION Biodiversité
De la diversité génétique à la
biodiversité

Brassage génétique

Création

au

hasard

de

combinaisons

de

chromosomes et donc d’allèles, au cours de
la reproduction sexuée.

DEFINITION Brassage-génétique
De la diversité génétique à la
biodiversité

Écosystème

Ensemble formé par un milieu de vie et ses êtres
vivants en interaction.

DEFINITION Écosystème
De la diversité génétique à la
biodiversité

Mutation

Modiﬁcation d’un gène pouvant entrainer la
formation d’un nouvel allèle.

DEFINITION Mutation
De la diversité génétique à la
biodiversité

Une diversité permise par la
reproduction sexuée
La diversité des gamètes formés
• la méiose a lieu dans les cellules sexuelles au niveau
des gonades (testicules ou ovaires)
• lors de la méiose, le matériel génétique est
doublé puis réparti au hasard dans quatre gamètes
(spermatozoïdes ou ovules)
• chaque gamète formé possède une combinaison
d’allèles différente car ils n’ont pas reçu les mêmes
combinaisons de chromosomes.
– on parle de brassage génétique : dans le
cas d’un être humain,
chromosomes,

celui-ci

qui possède 46
peut

fabriquer

des millions de gamètes génétiquement
différents

La diversité des possibilités de rencontre des
gamètes
• lors de la fécondation, un des gamètes mâles
rencontre un des gamètes femelles
• la combinaison d’allèles que portera le nouvel
individu dépend des allèles portés par le gamète
mâle et par le gamète femelle qui se rencontrent
au hasard
• de nombreuses combinaisons sont possibles : il
y a un second brassage génétique lors de la
fécondation

Le nombre d’individus différents possibles
est immense
• pour

une

cellule

avec

deux

paires

de

chromosomes, il y a quatre gamètes possibles
différents ; ce nombre double à chaque nouvelle
paire de chromosomes
– ex.

:

un humain possède 23 paires de

chromosomes donc cela représente jusqu’à
223 possibilités, soit plus de 8 millions de
gamètes génétiquement différents
• la

fécondation

multiplie

le

nombre

de

combinaisons possibles puisqu’elle unit au hasard
un gamète mâle et un gamète femelle
• pour un même couple de parents, il y a plus de cent
mille milliards de combinaisons possibles
• remarque : les vrais jumeaux sont identiques car ils
proviennent d’une seule et même cellule œuf

LEÇON Une-diversité-permise-par-la-reproduction-sexuée
De la diversité génétique à la
biodiversité

Les mutations peuvent créer de
nouveaux allèles
Les mutations et leur transmission
• une mutation est un phénomène rare qui consiste
en une modiﬁcation de l’ADN d’un gène (une
chance sur un milliard)
• si

la

mutation

touche

une

cellule

non

reproductrice, la mutation ne concernera que
les cellules formées par la division de cette cellule
• si la mutation a lieu dans un gamète et que ce
gamète est fécondé, alors le nouvel individu issu de
la fécondation possède un nouvel allèle
– un nouvel allèle apparait dans la population

Les conséquences des mutations
• les

mutations

peuvent

modiﬁer

des

caractéristiques biologiques des organismes
– ex. : pattes à la place des antennes chez la
mouche drosophile
• les mutations peuvent être bénéﬁques, neutres
ou néfastes pour la survie ou la reproduction des
individus concernés
– certaines mutations peuvent être à l’origine de
maladies (ex. : la mucoviscidose)
– certaines mutations apportent un avantage
par rapport aux autres (ex. : forme sombre des
papillons, moins visible par les prédateurs sur
un tronc sombre)
– certaines mutations sont neutres (ex. : lobe
des oreilles décollé ou plaqué)

LEÇON Les-mutations-peuvent-créer-de-nouveaux-allèles
De la diversité génétique à la
biodiversité

La biodiversité se retrouve à trois
niveaux
Une diversité d’écosystèmes
• un écosystème est un ensemble formé par un
milieu de vie (ex. : jardin) et ses êtres vivants (ex.
: abeille, ﬂeur) ainsi que leurs interactions 
(ex. : l’abeille butine la ﬂeur)
• il existe de nombreux écosystèmes différents sur
Terre, ex. : un jardin, la peau humaine, la forêt, etc.

La diversité des espèces
• il existe sur Terre de nombreuses espèces pouvant
appartenir à des groupes différents (ex.

:

mammifères, bactéries) et possédant chacune
des caractéristiques particulières (ex.

: régime

alimentaire, conditions optimales de survie)
• les espèces interagissent au sein d’un écosystème
– ex. : l’araignée est un prédateur de l’abeille,
l’abeille butine les ﬂeurs, etc.
• du fait de ces interactions, toutes les espèces
peuvent être affectées lorsqu’une seule espèce est
touchée
– ex.

:

un insecticide ciblant les abeilles

peut affecter les populations d’araignées et de
plantes

La diversité des allèles
• au sein d’une même espèce, il existe une grande
diversité d’individus
• cette diversité s’explique par la diversité des
combinaisons d’allèles possibles

LEÇON La-biodiversité-se-retrouve-à-trois-niveaux
De la diversité génétique à la
biodiversité

La formation des gamètes

Les cellules reproductrices, ou gamètes, sont formées
lors de la méiose.

Comme toute division cellulaire,

la méiose est précédée d’une copie de chaque
chromosome qui passe d’une à deux chromatides.

SCHEMA La-formation-des-gamètes
De la diversité génétique à la
biodiversité

Le devenir des mutations

SCHEMA Le-devenir-des-mutations
De la diversité génétique à la
biodiversité

L’essentiel en schéma

SCHEMA L’essentiel-en-schéma
De la diversité génétique à la
biodiversité

Les idées importantes

SCHEMA Les-idées-importantes
De la diversité génétique à la
biodiversité

Allèle

Une version d’un gène.

DEFINITION Allèle
De la diversité génétique à la
biodiversité

Biodiversité

Ensemble des êtres vivants et des écosystèmes
présents sur Terre.

DEFINITION Biodiversité
De la diversité génétique à la
biodiversité

Brassage génétique

Création

au

hasard

de

combinaisons

de

chromosomes et donc d’allèles, au cours de
la reproduction sexuée.

DEFINITION Brassage-génétique
De la diversité génétique à la
biodiversité

Écosystème

Ensemble formé par un milieu de vie et ses êtres
vivants en interaction.

DEFINITION Écosystème
De la diversité génétique à la
biodiversité

Mutation

Modiﬁcation d’un gène pouvant entrainer la
formation d’un nouvel allèle.

DEFINITION Mutation
De la diversité génétique à la
biodiversité

Une diversité permise par la
reproduction sexuée
La diversité des gamètes formés
• la méiose a lieu dans les cellules sexuelles au niveau
des gonades (testicules ou ovaires)
• lors de la méiose, le matériel génétique est
doublé puis réparti au hasard dans quatre gamètes
(spermatozoïdes ou ovules)
• chaque gamète formé possède une combinaison
d’allèles différente car ils n’ont pas reçu les mêmes
combinaisons de chromosomes.
– on parle de brassage génétique : dans le
cas d’un être humain,
chromosomes,

celui-ci

qui possède 46
peut

fabriquer

des millions de gamètes génétiquement
différents

La diversité des possibilités de rencontre des
gamètes
• lors de la fécondation, un des gamètes mâles
rencontre un des gamètes femelles
• la combinaison d’allèles que portera le nouvel
individu dépend des allèles portés par le gamète
mâle et par le gamète femelle qui se rencontrent
au hasard
• de nombreuses combinaisons sont possibles : il
y a un second brassage génétique lors de la
fécondation

Le nombre d’individus différents possibles
est immense
• pour

une

cellule

avec

deux

paires

de

chromosomes, il y a quatre gamètes possibles
différents ; ce nombre double à chaque nouvelle
paire de chromosomes
– ex.

:

un humain possède 23 paires de

chromosomes donc cela représente jusqu’à
223 possibilités, soit plus de 8 millions de
gamètes génétiquement différents
• la

fécondation

multiplie

le

nombre

de

combinaisons possibles puisqu’elle unit au hasard
un gamète mâle et un gamète femelle
• pour un même couple de parents, il y a plus de cent
mille milliards de combinaisons possibles
• remarque : les vrais jumeaux sont identiques car ils
proviennent d’une seule et même cellule œuf

LEÇON Une-diversité-permise-par-la-reproduction-sexuée
De la diversité génétique à la
biodiversité

Les mutations peuvent créer de
nouveaux allèles
Les mutations et leur transmission
• une mutation est un phénomène rare qui consiste
en une modiﬁcation de l’ADN d’un gène (une
chance sur un milliard)
• si

la

mutation

touche

une

cellule

non

reproductrice, la mutation ne concernera que
les cellules formées par la division de cette cellule
• si la mutation a lieu dans un gamète et que ce
gamète est fécondé, alors le nouvel individu issu de
la fécondation possède un nouvel allèle
– un nouvel allèle apparait dans la population

Les conséquences des mutations
• les

mutations

peuvent

modiﬁer

des

caractéristiques biologiques des organismes
– ex. : pattes à la place des antennes chez la
mouche drosophile
• les mutations peuvent être bénéﬁques, neutres
ou néfastes pour la survie ou la reproduction des
individus concernés
– certaines mutations peuvent être à l’origine de
maladies (ex. : la mucoviscidose)
– certaines mutations apportent un avantage
par rapport aux autres (ex. : forme sombre des
papillons, moins visible par les prédateurs sur
un tronc sombre)
– certaines mutations sont neutres (ex. : lobe
des oreilles décollé ou plaqué)

LEÇON Les-mutations-peuvent-créer-de-nouveaux-allèles
De la diversité génétique à la
biodiversité

La biodiversité se retrouve à trois
niveaux
Une diversité d’écosystèmes
• un écosystème est un ensemble formé par un
milieu de vie (ex. : jardin) et ses êtres vivants (ex.
: abeille, ﬂeur) ainsi que leurs interactions 
(ex. : l’abeille butine la ﬂeur)
• il existe de nombreux écosystèmes différents sur
Terre, ex. : un jardin, la peau humaine, la forêt, etc.

La diversité des espèces
• il existe sur Terre de nombreuses espèces pouvant
appartenir à des groupes différents (ex.

:

mammifères, bactéries) et possédant chacune
des caractéristiques particulières (ex.

: régime

alimentaire, conditions optimales de survie)
• les espèces interagissent au sein d’un écosystème
– ex. : l’araignée est un prédateur de l’abeille,
l’abeille butine les ﬂeurs, etc.
• du fait de ces interactions, toutes les espèces
peuvent être affectées lorsqu’une seule espèce est
touchée
– ex.

:

un insecticide ciblant les abeilles

peut affecter les populations d’araignées et de
plantes

La diversité des allèles
• au sein d’une même espèce, il existe une grande
diversité d’individus
• cette diversité s’explique par la diversité des
combinaisons d’allèles possibles

LEÇON La-biodiversité-se-retrouve-à-trois-niveaux
De la diversité génétique à la
biodiversité

La formation des gamètes

Les cellules reproductrices, ou gamètes, sont formées
lors de la méiose.

Comme toute division cellulaire,

la méiose est précédée d’une copie de chaque
chromosome qui passe d’une à deux chromatides.

SCHEMA La-formation-des-gamètes
De la diversité génétique à la
biodiversité

Le devenir des mutations

SCHEMA Le-devenir-des-mutations
De la diversité génétique à la
biodiversité

L’essentiel en schéma

SCHEMA L’essentiel-en-schéma
De la diversité génétique à la
biodiversité

Les idées importantes

SCHEMA Les-idées-importantes
De la diversité génétique à la
biodiversité

Allèle

Une version d’un gène.

DEFINITION Allèle
De la diversité génétique à la
biodiversité

Biodiversité

Ensemble des êtres vivants et des écosystèmes
présents sur Terre.

DEFINITION Biodiversité
De la diversité génétique à la
biodiversité

Brassage génétique

Création

au

hasard

de

combinaisons

de

chromosomes et donc d’allèles, au cours de
la reproduction sexuée.

DEFINITION Brassage-génétique
De la diversité génétique à la
biodiversité

Écosystème

Ensemble formé par un milieu de vie et ses êtres
vivants en interaction.

DEFINITION Écosystème
De la diversité génétique à la
biodiversité

Mutation

Modiﬁcation d’un gène pouvant entrainer la
formation d’un nouvel allèle.

DEFINITION Mutation
De la diversité génétique à la
biodiversité

Une diversité permise par la
reproduction sexuée
La diversité des gamètes formés
• la méiose a lieu dans les cellules sexuelles au niveau
des gonades (testicules ou ovaires)
• lors de la méiose, le matériel génétique est
doublé puis réparti au hasard dans quatre gamètes
(spermatozoïdes ou ovules)
• chaque gamète formé possède une combinaison
d’allèles différente car ils n’ont pas reçu les mêmes
combinaisons de chromosomes.
– on parle de brassage génétique : dans le
cas d’un être humain,
chromosomes,

celui-ci

qui possède 46
peut

fabriquer

des millions de gamètes génétiquement
différents

La diversité des possibilités de rencontre des
gamètes
• lors de la fécondation, un des gamètes mâles
rencontre un des gamètes femelles
• la combinaison d’allèles que portera le nouvel
individu dépend des allèles portés par le gamète
mâle et par le gamète femelle qui se rencontrent
au hasard
• de nombreuses combinaisons sont possibles : il
y a un second brassage génétique lors de la
fécondation

Le nombre d’individus différents possibles
est immense
• pour

une

cellule

avec

deux

paires

de

chromosomes, il y a quatre gamètes possibles
différents ; ce nombre double à chaque nouvelle
paire de chromosomes
– ex.

:

un humain possède 23 paires de

chromosomes donc cela représente jusqu’à
223 possibilités, soit plus de 8 millions de
gamètes génétiquement différents
• la

fécondation

multiplie

le

nombre

de

combinaisons possibles puisqu’elle unit au hasard
un gamète mâle et un gamète femelle
• pour un même couple de parents, il y a plus de cent
mille milliards de combinaisons possibles
• remarque : les vrais jumeaux sont identiques car ils
proviennent d’une seule et même cellule œuf

LEÇON Une-diversité-permise-par-la-reproduction-sexuée
De la diversité génétique à la
biodiversité

Les mutations peuvent créer de
nouveaux allèles
Les mutations et leur transmission
• une mutation est un phénomène rare qui consiste
en une modiﬁcation de l’ADN d’un gène (une
chance sur un milliard)
• si

la

mutation

touche

une

cellule

non

reproductrice, la mutation ne concernera que
les cellules formées par la division de cette cellule
• si la mutation a lieu dans un gamète et que ce
gamète est fécondé, alors le nouvel individu issu de
la fécondation possède un nouvel allèle
– un nouvel allèle apparait dans la population

Les conséquences des mutations
• les

mutations

peuvent

modiﬁer

des

caractéristiques biologiques des organismes
– ex. : pattes à la place des antennes chez la
mouche drosophile
• les mutations peuvent être bénéﬁques, neutres
ou néfastes pour la survie ou la reproduction des
individus concernés
– certaines mutations peuvent être à l’origine de
maladies (ex. : la mucoviscidose)
– certaines mutations apportent un avantage
par rapport aux autres (ex. : forme sombre des
papillons, moins visible par les prédateurs sur
un tronc sombre)
– certaines mutations sont neutres (ex. : lobe
des oreilles décollé ou plaqué)

LEÇON Les-mutations-peuvent-créer-de-nouveaux-allèles
De la diversité génétique à la
biodiversité

La biodiversité se retrouve à trois
niveaux
Une diversité d’écosystèmes
• un écosystème est un ensemble formé par un
milieu de vie (ex. : jardin) et ses êtres vivants (ex.
: abeille, ﬂeur) ainsi que leurs interactions 
(ex. : l’abeille butine la ﬂeur)
• il existe de nombreux écosystèmes différents sur
Terre, ex. : un jardin, la peau humaine, la forêt, etc.

La diversité des espèces
• il existe sur Terre de nombreuses espèces pouvant
appartenir à des groupes différents (ex.

:

mammifères, bactéries) et possédant chacune
des caractéristiques particulières (ex.

: régime

alimentaire, conditions optimales de survie)
• les espèces interagissent au sein d’un écosystème
– ex. : l’araignée est un prédateur de l’abeille,
l’abeille butine les ﬂeurs, etc.
• du fait de ces interactions, toutes les espèces
peuvent être affectées lorsqu’une seule espèce est
touchée
– ex.

:

un insecticide ciblant les abeilles

peut affecter les populations d’araignées et de
plantes

La diversité des allèles
• au sein d’une même espèce, il existe une grande
diversité d’individus
• cette diversité s’explique par la diversité des
combinaisons d’allèles possibles

LEÇON La-biodiversité-se-retrouve-à-trois-niveaux
