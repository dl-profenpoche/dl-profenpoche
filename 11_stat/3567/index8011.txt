La

nutrition

à

l’échelle

cellulaire

L’essentiel en schéma

SCHEMA L’essentiel-en-schéma
La

nutrition

à

l’échelle

cellulaire

Les idées importantes

SCHEMA Les-idées-importantes
La

nutrition

à

l’échelle

cellulaire

Cellule chlorophyllienne

Cellule végétale contenant des chloroplastes.

DEFINITION Cellule-chlorophyllienne
La

nutrition

à

l’échelle

cellulaire

Chloroplaste

Structure au sein des cellules végétales qui
permet la photosynthèse.

DEFINITION Chloroplaste
La

nutrition

à

l’échelle

cellulaire

Microorganisme

Organisme vivant, non visible à l’œil nu, souvent
composé d’une seule cellule.

DEFINITION Microorganisme
La

nutrition

à

l’échelle

cellulaire

Photosynthèse

Réaction chimique qui a lieu dans les cellules
chlorophylliennes et qui produit de la matière
organique à partir d’énergie lumineuse, d’eau et
de dioxyde de carbone.
CO_2 + H_2O + énergie lumineuse → matière
organique + O_2

DEFINITION Photosynthèse
La

nutrition

à

l’échelle

cellulaire

Respiration cellulaire

Réaction chimique qui a lieu dans toutes les
cellules et qui fournit l’énergie nécessaire à leur
fonctionnement à partir de nutriments et de
dioxygène.
nutriments + O_2 → énergie + CO_2 + H_2O

DEFINITION Respiration-cellulaire
La

nutrition

à

l’échelle

cellulaire

Symbiose

Relation

durable

entre

deux

organismes,

bénéﬁque pour les deux partenaires et nécessaire
à leur survie.

DEFINITION Symbiose
La

nutrition

à

l’échelle

cellulaire

La nutrition des cellules animales
Les

cellules

sont

approvisionnées

en

dioxygène et en nutriments par le sang
• le sang transporte les nutriments issus de
l’alimentation ainsi que le dioxygène provenant
des poumons (ex. : mammifères) ou des branchies
(ex. : poissons)
• certains animaux n’ont pas de sang mais de
l’hémolymphe qui transporte les nutriments (ex. :
les insectes) ainsi que le dioxygène parfois (ex. :
crustacés)
• le sang arrive dans les tissus par les capillaires
sanguins
• les capillaires sanguins sont très nombreux (grande
surface) et leur paroi est très ﬁne les éléments
la traversent donc facilement et atteignent les
cellules

Les

cellules

animales

effectuent

la

respiration cellulaire
• les cellules animales utilisent du dioxygène et des
nutriments pour libérer de l’énergie :

c’est la

respiration cellulaire
– nutriments + dioxygène (O_2) → énergie + eau
+ dioxyde de carbone (CO_2)
• cette réaction chimique produit un déchet qu’il
faut évacuer : le dioxyde de carbone

Les

cellules

animales

produisent

des

déchets
• la respiration cellulaire produit du dioxyde de
carbone qui est un déchet pour les cellules
• le dioxyde de carbone et l’urée sont des déchets :
ils sont rejetés des cellules vers le sang, puis vers les
poumons (dioxyde de carbone) ou les reins (urée)

LEÇON La-nutrition-des-cellules-animales
La

nutrition

à

l’échelle

cellulaire

La nutrition des cellules végétales
Les

cellules

végétales

effectuent

la

respiration cellulaire
• comme les cellules animales, les cellules végétales
respirent : elles produisent de l’énergie à partir
du dioxygène et des nutriments, et rejettent du
dioxyde de carbone

Certaines cellules végétales effectuent la
photosynthèse
• certaines cellules végétales exposées à la lumière
absorbent du dioxyde de carbone et rejettent du
dioxygène
• elles produisent de la matière organique sous
forme de glucides
• une

réaction

chimique

au

sein

des cellules

végétales exposées à la lumière a lieu permettant
la production de glucides à partir de dioxyde de
carbone et d’eau, grâce à l’énergie lumineuse :
c’est la photosynthèse

La

photosynthèse

a

lieu

dans

les

chloroplastes
• certaines cellules végétales possèdent des petites
structures vertes : les chloroplastes
• on

appelle

ces

cellules

des

cellules

chlorophylliennes
– ex. : cellules de feuilles
• on trouve des glucides dans des chloroplastes de
cellules chlorophylliennes éclairées : c’est le lieu de
la photosynthèse
• les cellules ne possédant pas de chloroplastes
n’effectuent

pas

la

photosynthèse

mais

uniquement la respiration (comme une cellule
animale)
– ex. : cellules d’épiderme d’oignon

LEÇON La-nutrition-des-cellules-végétales
La

nutrition

à

l’échelle

cellulaire

L’apport des microorganismes dans
la nutrition cellulaire
Les microorganismes aident les animaux à
digérer
• les animaux peuvent digérer certains aliments
uniquement grâce aux enzymes produites par
des microorganismes de leur tube digestif
– ex. : les algues rouges sont bien digérées dans
la population japonaise
• les microorganismes du tube digestif des animaux
proﬁtent de la matière organique digérée
• la

relation

entre

microorganismes

de

les

animaux

leur

tube

et

digestif

les
est

bénéﬁque pour chacun d’eux : c’est une symbiose

Les microorganismes aident les végétaux à
prélever de la matière de leur milieu
• la

plupart

des

végétaux

entretiennent

des

relations de symbiose avec d’autres organismes ou
microorganismes :ces relations sont bénéﬁques à
la croissance de chacun d’eux
– ex.

:

l’association entre les racines des

végétaux et des champignons s’appelle une
mycorhize et permet aux végétaux d’absorber
les minéraux du sol et aux champignons
d’obtenir de la matière organique
– ex.

:

l’association entre une algue et un

champignon s’appelle un lichen ; on en trouve
souvent sur des rochers, des murs ou des
troncs d’arbres

LEÇON L’apport-des-microorganismes-dans-la-nutrition-cellulaire
La

nutrition

à

l’échelle

cellulaire

L’essentiel en schéma

SCHEMA L’essentiel-en-schéma
La

nutrition

à

l’échelle

cellulaire

Les idées importantes

SCHEMA Les-idées-importantes
La

nutrition

à

l’échelle

cellulaire

Cellule chlorophyllienne

Cellule végétale contenant des chloroplastes.

DEFINITION Cellule-chlorophyllienne
La

nutrition

à

l’échelle

cellulaire

Chloroplaste

Structure au sein des cellules végétales qui
permet la photosynthèse.

DEFINITION Chloroplaste
La

nutrition

à

l’échelle

cellulaire

Microorganisme

Organisme vivant, non visible à l’œil nu, souvent
composé d’une seule cellule.

DEFINITION Microorganisme
La

nutrition

à

l’échelle

cellulaire

Photosynthèse

Réaction chimique qui a lieu dans les cellules
chlorophylliennes et qui produit de la matière
organique à partir d’énergie lumineuse, d’eau et
de dioxyde de carbone.
CO_2 + H_2O + énergie lumineuse → matière
organique + O_2

DEFINITION Photosynthèse
La

nutrition

à

l’échelle

cellulaire

Respiration cellulaire

Réaction chimique qui a lieu dans toutes les
cellules et qui fournit l’énergie nécessaire à leur
fonctionnement à partir de nutriments et de
dioxygène.
nutriments + O_2 → énergie + CO_2 + H_2O

DEFINITION Respiration-cellulaire
La

nutrition

à

l’échelle

cellulaire

Symbiose

Relation

durable

entre

deux

organismes,

bénéﬁque pour les deux partenaires et nécessaire
à leur survie.

DEFINITION Symbiose
La

nutrition

à

l’échelle

cellulaire

La nutrition des cellules animales
Les

cellules

sont

approvisionnées

en

dioxygène et en nutriments par le sang
• le sang transporte les nutriments issus de
l’alimentation ainsi que le dioxygène provenant
des poumons (ex. : mammifères) ou des branchies
(ex. : poissons)
• certains animaux n’ont pas de sang mais de
l’hémolymphe qui transporte les nutriments (ex. :
les insectes) ainsi que le dioxygène parfois (ex. :
crustacés)
• le sang arrive dans les tissus par les capillaires
sanguins
• les capillaires sanguins sont très nombreux (grande
surface) et leur paroi est très ﬁne les éléments
la traversent donc facilement et atteignent les
cellules

Les

cellules

animales

effectuent

la

respiration cellulaire
• les cellules animales utilisent du dioxygène et des
nutriments pour libérer de l’énergie :

c’est la

respiration cellulaire
– nutriments + dioxygène (O_2) → énergie + eau
+ dioxyde de carbone (CO_2)
• cette réaction chimique produit un déchet qu’il
faut évacuer : le dioxyde de carbone

Les

cellules

animales

produisent

des

déchets
• la respiration cellulaire produit du dioxyde de
carbone qui est un déchet pour les cellules
• le dioxyde de carbone et l’urée sont des déchets :
ils sont rejetés des cellules vers le sang, puis vers les
poumons (dioxyde de carbone) ou les reins (urée)

LEÇON La-nutrition-des-cellules-animales
La

nutrition

à

l’échelle

cellulaire

La nutrition des cellules végétales
Les

cellules

végétales

effectuent

la

respiration cellulaire
• comme les cellules animales, les cellules végétales
respirent : elles produisent de l’énergie à partir
du dioxygène et des nutriments, et rejettent du
dioxyde de carbone

Certaines cellules végétales effectuent la
photosynthèse
• certaines cellules végétales exposées à la lumière
absorbent du dioxyde de carbone et rejettent du
dioxygène
• elles produisent de la matière organique sous
forme de glucides
• une

réaction

chimique

au

sein

des cellules

végétales exposées à la lumière a lieu permettant
la production de glucides à partir de dioxyde de
carbone et d’eau, grâce à l’énergie lumineuse :
c’est la photosynthèse

La

photosynthèse

a

lieu

dans

les

chloroplastes
• certaines cellules végétales possèdent des petites
structures vertes : les chloroplastes
• on

appelle

ces

cellules

des

cellules

chlorophylliennes
– ex. : cellules de feuilles
• on trouve des glucides dans des chloroplastes de
cellules chlorophylliennes éclairées : c’est le lieu de
la photosynthèse
• les cellules ne possédant pas de chloroplastes
n’effectuent

pas

la

photosynthèse

mais

uniquement la respiration (comme une cellule
animale)
– ex. : cellules d’épiderme d’oignon

LEÇON La-nutrition-des-cellules-végétales
La

nutrition

à

l’échelle

cellulaire

L’apport des microorganismes dans
la nutrition cellulaire
Les microorganismes aident les animaux à
digérer
• les animaux peuvent digérer certains aliments
uniquement grâce aux enzymes produites par
des microorganismes de leur tube digestif
– ex. : les algues rouges sont bien digérées dans
la population japonaise
• les microorganismes du tube digestif des animaux
proﬁtent de la matière organique digérée
• la

relation

entre

microorganismes

de

les

animaux

leur

tube

et

digestif

les
est

bénéﬁque pour chacun d’eux : c’est une symbiose

Les microorganismes aident les végétaux à
prélever de la matière de leur milieu
• la

plupart

des

végétaux

entretiennent

des

relations de symbiose avec d’autres organismes ou
microorganismes :ces relations sont bénéﬁques à
la croissance de chacun d’eux
– ex.

:

l’association entre les racines des

végétaux et des champignons s’appelle une
mycorhize et permet aux végétaux d’absorber
les minéraux du sol et aux champignons
d’obtenir de la matière organique
– ex.

:

l’association entre une algue et un

champignon s’appelle un lichen ; on en trouve
souvent sur des rochers, des murs ou des
troncs d’arbres

LEÇON L’apport-des-microorganismes-dans-la-nutrition-cellulaire
La

nutrition

à

l’échelle

cellulaire

L’essentiel en schéma

SCHEMA L’essentiel-en-schéma
La

nutrition

à

l’échelle

cellulaire

Les idées importantes

SCHEMA Les-idées-importantes
La

nutrition

à

l’échelle

cellulaire

Cellule chlorophyllienne

Cellule végétale contenant des chloroplastes.

DEFINITION Cellule-chlorophyllienne
La

nutrition

à

l’échelle

cellulaire

Chloroplaste

Structure au sein des cellules végétales qui
permet la photosynthèse.

DEFINITION Chloroplaste
La

nutrition

à

l’échelle

cellulaire

Microorganisme

Organisme vivant, non visible à l’œil nu, souvent
composé d’une seule cellule.

DEFINITION Microorganisme
La

nutrition

à

l’échelle

cellulaire

Photosynthèse

Réaction chimique qui a lieu dans les cellules
chlorophylliennes et qui produit de la matière
organique à partir d’énergie lumineuse, d’eau et
de dioxyde de carbone.
CO_2 + H_2O + énergie lumineuse → matière
organique + O_2

DEFINITION Photosynthèse
La

nutrition

à

l’échelle

cellulaire

Respiration cellulaire

Réaction chimique qui a lieu dans toutes les
cellules et qui fournit l’énergie nécessaire à leur
fonctionnement à partir de nutriments et de
dioxygène.
nutriments + O_2 → énergie + CO_2 + H_2O

DEFINITION Respiration-cellulaire
La

nutrition

à

l’échelle

cellulaire

Symbiose

Relation

durable

entre

deux

organismes,

bénéﬁque pour les deux partenaires et nécessaire
à leur survie.

DEFINITION Symbiose
La

nutrition

à

l’échelle

cellulaire

La nutrition des cellules animales
Les

cellules

sont

approvisionnées

en

dioxygène et en nutriments par le sang
• le sang transporte les nutriments issus de
l’alimentation ainsi que le dioxygène provenant
des poumons (ex. : mammifères) ou des branchies
(ex. : poissons)
• certains animaux n’ont pas de sang mais de
l’hémolymphe qui transporte les nutriments (ex. :
les insectes) ainsi que le dioxygène parfois (ex. :
crustacés)
• le sang arrive dans les tissus par les capillaires
sanguins
• les capillaires sanguins sont très nombreux (grande
surface) et leur paroi est très ﬁne les éléments
la traversent donc facilement et atteignent les
cellules

Les

cellules

animales

effectuent

la

respiration cellulaire
• les cellules animales utilisent du dioxygène et des
nutriments pour libérer de l’énergie :

c’est la

respiration cellulaire
– nutriments + dioxygène (O_2) → énergie + eau
+ dioxyde de carbone (CO_2)
• cette réaction chimique produit un déchet qu’il
faut évacuer : le dioxyde de carbone

Les

cellules

animales

produisent

des

déchets
• la respiration cellulaire produit du dioxyde de
carbone qui est un déchet pour les cellules
• le dioxyde de carbone et l’urée sont des déchets :
ils sont rejetés des cellules vers le sang, puis vers les
poumons (dioxyde de carbone) ou les reins (urée)

LEÇON La-nutrition-des-cellules-animales
La

nutrition

à

l’échelle

cellulaire

La nutrition des cellules végétales
Les

cellules

végétales

effectuent

la

respiration cellulaire
• comme les cellules animales, les cellules végétales
respirent : elles produisent de l’énergie à partir
du dioxygène et des nutriments, et rejettent du
dioxyde de carbone

Certaines cellules végétales effectuent la
photosynthèse
• certaines cellules végétales exposées à la lumière
absorbent du dioxyde de carbone et rejettent du
dioxygène
• elles produisent de la matière organique sous
forme de glucides
• une

réaction

chimique

au

sein

des cellules

végétales exposées à la lumière a lieu permettant
la production de glucides à partir de dioxyde de
carbone et d’eau, grâce à l’énergie lumineuse :
c’est la photosynthèse

La

photosynthèse

a

lieu

dans

les

chloroplastes
• certaines cellules végétales possèdent des petites
structures vertes : les chloroplastes
• on

appelle

ces

cellules

des

cellules

chlorophylliennes
– ex. : cellules de feuilles
• on trouve des glucides dans des chloroplastes de
cellules chlorophylliennes éclairées : c’est le lieu de
la photosynthèse
• les cellules ne possédant pas de chloroplastes
n’effectuent

pas

la

photosynthèse

mais

uniquement la respiration (comme une cellule
animale)
– ex. : cellules d’épiderme d’oignon

LEÇON La-nutrition-des-cellules-végétales
La

nutrition

à

l’échelle

cellulaire

L’apport des microorganismes dans
la nutrition cellulaire
Les microorganismes aident les animaux à
digérer
• les animaux peuvent digérer certains aliments
uniquement grâce aux enzymes produites par
des microorganismes de leur tube digestif
– ex. : les algues rouges sont bien digérées dans
la population japonaise
• les microorganismes du tube digestif des animaux
proﬁtent de la matière organique digérée
• la

relation

entre

microorganismes

de

les

animaux

leur

tube

et

digestif

les
est

bénéﬁque pour chacun d’eux : c’est une symbiose

Les microorganismes aident les végétaux à
prélever de la matière de leur milieu
• la

plupart

des

végétaux

entretiennent

des

relations de symbiose avec d’autres organismes ou
microorganismes :ces relations sont bénéﬁques à
la croissance de chacun d’eux
– ex.

:

l’association entre les racines des

végétaux et des champignons s’appelle une
mycorhize et permet aux végétaux d’absorber
les minéraux du sol et aux champignons
d’obtenir de la matière organique
– ex.

:

l’association entre une algue et un

champignon s’appelle un lichen ; on en trouve
souvent sur des rochers, des murs ou des
troncs d’arbres

LEÇON L’apport-des-microorganismes-dans-la-nutrition-cellulaire
La

nutrition

à

l’échelle

cellulaire

L’essentiel en schéma

SCHEMA L’essentiel-en-schéma
La

nutrition

à

l’échelle

cellulaire

Les idées importantes

SCHEMA Les-idées-importantes
La

nutrition

à

l’échelle

cellulaire

Cellule chlorophyllienne

Cellule végétale contenant des chloroplastes.

DEFINITION Cellule-chlorophyllienne
La

nutrition

à

l’échelle

cellulaire

Chloroplaste

Structure au sein des cellules végétales qui
permet la photosynthèse.

DEFINITION Chloroplaste
La

nutrition

à

l’échelle

cellulaire

Microorganisme

Organisme vivant, non visible à l’œil nu, souvent
composé d’une seule cellule.

DEFINITION Microorganisme
La

nutrition

à

l’échelle

cellulaire

Photosynthèse

Réaction chimique qui a lieu dans les cellules
chlorophylliennes et qui produit de la matière
organique à partir d’énergie lumineuse, d’eau et
de dioxyde de carbone.
CO_2 + H_2O + énergie lumineuse → matière
organique + O_2

DEFINITION Photosynthèse
La

nutrition

à

l’échelle

cellulaire

Respiration cellulaire

Réaction chimique qui a lieu dans toutes les
cellules et qui fournit l’énergie nécessaire à leur
fonctionnement à partir de nutriments et de
dioxygène.
nutriments + O_2 → énergie + CO_2 + H_2O

DEFINITION Respiration-cellulaire
La

nutrition

à

l’échelle

cellulaire

Symbiose

Relation

durable

entre

deux

organismes,

bénéﬁque pour les deux partenaires et nécessaire
à leur survie.

DEFINITION Symbiose
La

nutrition

à

l’échelle

cellulaire

La nutrition des cellules animales
Les

cellules

sont

approvisionnées

en

dioxygène et en nutriments par le sang
• le sang transporte les nutriments issus de
l’alimentation ainsi que le dioxygène provenant
des poumons (ex. : mammifères) ou des branchies
(ex. : poissons)
• certains animaux n’ont pas de sang mais de
l’hémolymphe qui transporte les nutriments (ex. :
les insectes) ainsi que le dioxygène parfois (ex. :
crustacés)
• le sang arrive dans les tissus par les capillaires
sanguins
• les capillaires sanguins sont très nombreux (grande
surface) et leur paroi est très ﬁne les éléments
la traversent donc facilement et atteignent les
cellules

Les

cellules

animales

effectuent

la

respiration cellulaire
• les cellules animales utilisent du dioxygène et des
nutriments pour libérer de l’énergie :

c’est la

respiration cellulaire
– nutriments + dioxygène (O_2) → énergie + eau
+ dioxyde de carbone (CO_2)
• cette réaction chimique produit un déchet qu’il
faut évacuer : le dioxyde de carbone

Les

cellules

animales

produisent

des

déchets
• la respiration cellulaire produit du dioxyde de
carbone qui est un déchet pour les cellules
• le dioxyde de carbone et l’urée sont des déchets :
ils sont rejetés des cellules vers le sang, puis vers les
poumons (dioxyde de carbone) ou les reins (urée)

LEÇON La-nutrition-des-cellules-animales
La

nutrition

à

l’échelle

cellulaire

La nutrition des cellules végétales
Les

cellules

végétales

effectuent

la

respiration cellulaire
• comme les cellules animales, les cellules végétales
respirent : elles produisent de l’énergie à partir
du dioxygène et des nutriments, et rejettent du
dioxyde de carbone

Certaines cellules végétales effectuent la
photosynthèse
• certaines cellules végétales exposées à la lumière
absorbent du dioxyde de carbone et rejettent du
dioxygène
• elles produisent de la matière organique sous
forme de glucides
• une

réaction

chimique

au

sein

des cellules

végétales exposées à la lumière a lieu permettant
la production de glucides à partir de dioxyde de
carbone et d’eau, grâce à l’énergie lumineuse :
c’est la photosynthèse

La

photosynthèse

a

lieu

dans

les

chloroplastes
• certaines cellules végétales possèdent des petites
structures vertes : les chloroplastes
• on

appelle

ces

cellules

des

cellules

chlorophylliennes
– ex. : cellules de feuilles
• on trouve des glucides dans des chloroplastes de
cellules chlorophylliennes éclairées : c’est le lieu de
la photosynthèse
• les cellules ne possédant pas de chloroplastes
n’effectuent

pas

la

photosynthèse

mais

uniquement la respiration (comme une cellule
animale)
– ex. : cellules d’épiderme d’oignon

LEÇON La-nutrition-des-cellules-végétales
La

nutrition

à

l’échelle

cellulaire

L’apport des microorganismes dans
la nutrition cellulaire
Les microorganismes aident les animaux à
digérer
• les animaux peuvent digérer certains aliments
uniquement grâce aux enzymes produites par
des microorganismes de leur tube digestif
– ex. : les algues rouges sont bien digérées dans
la population japonaise
• les microorganismes du tube digestif des animaux
proﬁtent de la matière organique digérée
• la

relation

entre

microorganismes

de

les

animaux

leur

tube

et

digestif

les
est

bénéﬁque pour chacun d’eux : c’est une symbiose

Les microorganismes aident les végétaux à
prélever de la matière de leur milieu
• la

plupart

des

végétaux

entretiennent

des

relations de symbiose avec d’autres organismes ou
microorganismes :ces relations sont bénéﬁques à
la croissance de chacun d’eux
– ex.

:

l’association entre les racines des

végétaux et des champignons s’appelle une
mycorhize et permet aux végétaux d’absorber
les minéraux du sol et aux champignons
d’obtenir de la matière organique
– ex.

:

l’association entre une algue et un

champignon s’appelle un lichen ; on en trouve
souvent sur des rochers, des murs ou des
troncs d’arbres

LEÇON L’apport-des-microorganismes-dans-la-nutrition-cellulaire
