Des aliments aux nutriments

L’essentiel en schéma

SCHEMA L’essentiel-en-schéma
Des aliments aux nutriments

Les idées importantes

SCHEMA Les-idées-importantes
Des aliments aux nutriments

Le contenu des sucs digestifs

Les sucs digestifs contiennent des molécules nommées
enzymes. Chaque enzyme a un rôle différent et porte
un nom différent.

SCHEMA Le-contenu-des-sucs-digestifs
Des aliments aux nutriments

Les rôles respectifs des enzymes
humaines et du microbiote dans la
digestion des glucides

SCHEMA Les-rôles-respectifs-des-enzymes-humaines-et-du-microbiote-dans-la-digestion-des-glucides
Des aliments aux nutriments

Absorption

Processus par lequel des molécules (comme les
nutriments) sont introduites dans l’organisme.

DEFINITION Absorption
Des aliments aux nutriments

Digestion chimique

Simpliﬁcation des aliments en nutriments sous
l’action d’enzymes.

DEFINITION Digestion-chimique
Des aliments aux nutriments

Enzyme

Molécule contenue dans les sucs digestifs qui
transforme les aliments en nutriments.

DEFINITION Enzyme
Des aliments aux nutriments

Microbiote intestinal

Ensemble des microorganismes présents dans
l’intestin.

DEFINITION Microbiote-intestinal
Des aliments aux nutriments

La digestion des aliments
Les aliments et les nutriments
• les aliments sont composés de grosses molécules
complexes

formées

par

l’assemblage

de

nutriments (« briques » élémentaires)
– ex. : sucres simples, acides gras, acides aminés
sont des nutriments
– ex.

: glucides, lipides, protéines sont des

molécules complexes contenues dans les
aliments
• la digestion consiste à décomposer les aliments
: les nutriments sont alors sous forme simple et
absorbables au niveau de l’intestin
• ces nutriments, une fois absorbés, peuvent servir
à construire de nouvelles molécules complexes
impliquées dans le bon fonctionnement de
l’organisme ou à produire de l’énergie

Le rôle des sucs digestifs dans la digestion
• certains organes du tube digestif libèrent des
substances appelées sucs digestifs :
– la bouche produit de la salive
– le foie produit de la bile
– l’estomac produit du suc gastrique
– le pancréas produit du suc pancréatique
– le gros intestin produit du suc intestinal
• ces sucs digestifs contiennent des molécules
appelées enzymes qui décomposent les aliments
en nutriments
• chaque enzyme peut décomposer un seul type
d’aliment
– ex. : l’amylase décompose l’amidon qui est un
glucide

LEÇON La-digestion-des-aliments
Des aliments aux nutriments

L’absorption des nutriments
Seuls les nutriments sont absorbés
• la majorité de l’absorption a lieu au niveau de
l’intestin grêle
• la paroi de l’intestin grêle n’est pas capable
d’absorber les grosses molécules des aliments, elle
n’absorbe que les nutriments : la digestion par les
enzymes est donc indispensable

La surface d’absorption de l’intestin grêle
• l’intestin grêle est un tube de 6 à 7 m de long et
d’environ 4 cm de diamètre, replié sur lui-même
• la paroi intestinale est une surface d’échanges
au travers de laquelle les nutriments passent
facilement car :
– son épaisseur est faible (de l’ordre du millième
de millimètre)
– sa surface est grande grâce à ses nombreux
replis (supérieure à celle d’un terrain de tennis)
– elle contient de nombreux capillaires sanguins

LEÇON L’absorption-des-nutriments
Des aliments aux nutriments

Le rôle du microbiote dans la
digestion
Les enzymes du microbiote intestinal
• les microorganismes vivant dans notre tube
digestif (microbiote) libèrent elles également des
enzymes
• les enzymes libérées par le microbiote sont
beaucoup plus nombreuses et variées que celles
libérées par les organes de notre tube digestif
• le microbiote participe donc beaucoup à la
digestion chimique des aliments en nutriments

Le rôle du microbiote dans l’absorption
intestinale
• le microbiote facilite l’absorption intestinale des
nutriments

L’importance du microbiote pour le bon
fonctionnement
• on observe des différences entre le microbiote d’un
individu sain et celui d’un individu obèse
• des chercheurs essayent donc de lutter contre
l’obésité en apportant des bactéries qui vont aller
dans le tube digestif des malades
• il est possible d’ajouter des bactéries au microbiote
d’un individu en prenant des probiotiques

LEÇON Le-rôle-du-microbiote-dans-la-digestion
Des aliments aux nutriments

L’essentiel en schéma

SCHEMA L’essentiel-en-schéma
Des aliments aux nutriments

Les idées importantes

SCHEMA Les-idées-importantes
Des aliments aux nutriments

Le contenu des sucs digestifs

Les sucs digestifs contiennent des molécules nommées
enzymes. Chaque enzyme a un rôle différent et porte
un nom différent.

SCHEMA Le-contenu-des-sucs-digestifs
Des aliments aux nutriments

Les rôles respectifs des enzymes
humaines et du microbiote dans la
digestion des glucides

SCHEMA Les-rôles-respectifs-des-enzymes-humaines-et-du-microbiote-dans-la-digestion-des-glucides
Des aliments aux nutriments

Absorption

Processus par lequel des molécules (comme les
nutriments) sont introduites dans l’organisme.

DEFINITION Absorption
Des aliments aux nutriments

Digestion chimique

Simpliﬁcation des aliments en nutriments sous
l’action d’enzymes.

DEFINITION Digestion-chimique
Des aliments aux nutriments

Enzyme

Molécule contenue dans les sucs digestifs qui
transforme les aliments en nutriments.

DEFINITION Enzyme
Des aliments aux nutriments

Microbiote intestinal

Ensemble des microorganismes présents dans
l’intestin.

DEFINITION Microbiote-intestinal
Des aliments aux nutriments

La digestion des aliments
Les aliments et les nutriments
• les aliments sont composés de grosses molécules
complexes

formées

par

l’assemblage

de

nutriments (« briques » élémentaires)
– ex. : sucres simples, acides gras, acides aminés
sont des nutriments
– ex.

: glucides, lipides, protéines sont des

molécules complexes contenues dans les
aliments
• la digestion consiste à décomposer les aliments
: les nutriments sont alors sous forme simple et
absorbables au niveau de l’intestin
• ces nutriments, une fois absorbés, peuvent servir
à construire de nouvelles molécules complexes
impliquées dans le bon fonctionnement de
l’organisme ou à produire de l’énergie

Le rôle des sucs digestifs dans la digestion
• certains organes du tube digestif libèrent des
substances appelées sucs digestifs :
– la bouche produit de la salive
– le foie produit de la bile
– l’estomac produit du suc gastrique
– le pancréas produit du suc pancréatique
– le gros intestin produit du suc intestinal
• ces sucs digestifs contiennent des molécules
appelées enzymes qui décomposent les aliments
en nutriments
• chaque enzyme peut décomposer un seul type
d’aliment
– ex. : l’amylase décompose l’amidon qui est un
glucide

LEÇON La-digestion-des-aliments
Des aliments aux nutriments

L’absorption des nutriments
Seuls les nutriments sont absorbés
• la majorité de l’absorption a lieu au niveau de
l’intestin grêle
• la paroi de l’intestin grêle n’est pas capable
d’absorber les grosses molécules des aliments, elle
n’absorbe que les nutriments : la digestion par les
enzymes est donc indispensable

La surface d’absorption de l’intestin grêle
• l’intestin grêle est un tube de 6 à 7 m de long et
d’environ 4 cm de diamètre, replié sur lui-même
• la paroi intestinale est une surface d’échanges
au travers de laquelle les nutriments passent
facilement car :
– son épaisseur est faible (de l’ordre du millième
de millimètre)
– sa surface est grande grâce à ses nombreux
replis (supérieure à celle d’un terrain de tennis)
– elle contient de nombreux capillaires sanguins

LEÇON L’absorption-des-nutriments
Des aliments aux nutriments

Le rôle du microbiote dans la
digestion
Les enzymes du microbiote intestinal
• les microorganismes vivant dans notre tube
digestif (microbiote) libèrent elles également des
enzymes
• les enzymes libérées par le microbiote sont
beaucoup plus nombreuses et variées que celles
libérées par les organes de notre tube digestif
• le microbiote participe donc beaucoup à la
digestion chimique des aliments en nutriments

Le rôle du microbiote dans l’absorption
intestinale
• le microbiote facilite l’absorption intestinale des
nutriments

L’importance du microbiote pour le bon
fonctionnement
• on observe des différences entre le microbiote d’un
individu sain et celui d’un individu obèse
• des chercheurs essayent donc de lutter contre
l’obésité en apportant des bactéries qui vont aller
dans le tube digestif des malades
• il est possible d’ajouter des bactéries au microbiote
d’un individu en prenant des probiotiques

LEÇON Le-rôle-du-microbiote-dans-la-digestion
Des aliments aux nutriments

L’essentiel en schéma

SCHEMA L’essentiel-en-schéma
Des aliments aux nutriments

Les idées importantes

SCHEMA Les-idées-importantes
Des aliments aux nutriments

Le contenu des sucs digestifs

Les sucs digestifs contiennent des molécules nommées
enzymes. Chaque enzyme a un rôle différent et porte
un nom différent.

SCHEMA Le-contenu-des-sucs-digestifs
Des aliments aux nutriments

Les rôles respectifs des enzymes
humaines et du microbiote dans la
digestion des glucides

SCHEMA Les-rôles-respectifs-des-enzymes-humaines-et-du-microbiote-dans-la-digestion-des-glucides
Des aliments aux nutriments

Absorption

Processus par lequel des molécules (comme les
nutriments) sont introduites dans l’organisme.

DEFINITION Absorption
Des aliments aux nutriments

Digestion chimique

Simpliﬁcation des aliments en nutriments sous
l’action d’enzymes.

DEFINITION Digestion-chimique
Des aliments aux nutriments

Enzyme

Molécule contenue dans les sucs digestifs qui
transforme les aliments en nutriments.

DEFINITION Enzyme
Des aliments aux nutriments

Microbiote intestinal

Ensemble des microorganismes présents dans
l’intestin.

DEFINITION Microbiote-intestinal
Des aliments aux nutriments

La digestion des aliments
Les aliments et les nutriments
• les aliments sont composés de grosses molécules
complexes

formées

par

l’assemblage

de

nutriments (« briques » élémentaires)
– ex. : sucres simples, acides gras, acides aminés
sont des nutriments
– ex.

: glucides, lipides, protéines sont des

molécules complexes contenues dans les
aliments
• la digestion consiste à décomposer les aliments
: les nutriments sont alors sous forme simple et
absorbables au niveau de l’intestin
• ces nutriments, une fois absorbés, peuvent servir
à construire de nouvelles molécules complexes
impliquées dans le bon fonctionnement de
l’organisme ou à produire de l’énergie

Le rôle des sucs digestifs dans la digestion
• certains organes du tube digestif libèrent des
substances appelées sucs digestifs :
– la bouche produit de la salive
– le foie produit de la bile
– l’estomac produit du suc gastrique
– le pancréas produit du suc pancréatique
– le gros intestin produit du suc intestinal
• ces sucs digestifs contiennent des molécules
appelées enzymes qui décomposent les aliments
en nutriments
• chaque enzyme peut décomposer un seul type
d’aliment
– ex. : l’amylase décompose l’amidon qui est un
glucide

LEÇON La-digestion-des-aliments
Des aliments aux nutriments

L’absorption des nutriments
Seuls les nutriments sont absorbés
• la majorité de l’absorption a lieu au niveau de
l’intestin grêle
• la paroi de l’intestin grêle n’est pas capable
d’absorber les grosses molécules des aliments, elle
n’absorbe que les nutriments : la digestion par les
enzymes est donc indispensable

La surface d’absorption de l’intestin grêle
• l’intestin grêle est un tube de 6 à 7 m de long et
d’environ 4 cm de diamètre, replié sur lui-même
• la paroi intestinale est une surface d’échanges
au travers de laquelle les nutriments passent
facilement car :
– son épaisseur est faible (de l’ordre du millième
de millimètre)
– sa surface est grande grâce à ses nombreux
replis (supérieure à celle d’un terrain de tennis)
– elle contient de nombreux capillaires sanguins

LEÇON L’absorption-des-nutriments
Des aliments aux nutriments

Le rôle du microbiote dans la
digestion
Les enzymes du microbiote intestinal
• les microorganismes vivant dans notre tube
digestif (microbiote) libèrent elles également des
enzymes
• les enzymes libérées par le microbiote sont
beaucoup plus nombreuses et variées que celles
libérées par les organes de notre tube digestif
• le microbiote participe donc beaucoup à la
digestion chimique des aliments en nutriments

Le rôle du microbiote dans l’absorption
intestinale
• le microbiote facilite l’absorption intestinale des
nutriments

L’importance du microbiote pour le bon
fonctionnement
• on observe des différences entre le microbiote d’un
individu sain et celui d’un individu obèse
• des chercheurs essayent donc de lutter contre
l’obésité en apportant des bactéries qui vont aller
dans le tube digestif des malades
• il est possible d’ajouter des bactéries au microbiote
d’un individu en prenant des probiotiques

LEÇON Le-rôle-du-microbiote-dans-la-digestion
Des aliments aux nutriments

L’essentiel en schéma

SCHEMA L’essentiel-en-schéma
Des aliments aux nutriments

Les idées importantes

SCHEMA Les-idées-importantes
Des aliments aux nutriments

Le contenu des sucs digestifs

Les sucs digestifs contiennent des molécules nommées
enzymes. Chaque enzyme a un rôle différent et porte
un nom différent.

SCHEMA Le-contenu-des-sucs-digestifs
Des aliments aux nutriments

Les rôles respectifs des enzymes
humaines et du microbiote dans la
digestion des glucides

SCHEMA Les-rôles-respectifs-des-enzymes-humaines-et-du-microbiote-dans-la-digestion-des-glucides
Des aliments aux nutriments

Absorption

Processus par lequel des molécules (comme les
nutriments) sont introduites dans l’organisme.

DEFINITION Absorption
Des aliments aux nutriments

Digestion chimique

Simpliﬁcation des aliments en nutriments sous
l’action d’enzymes.

DEFINITION Digestion-chimique
Des aliments aux nutriments

Enzyme

Molécule contenue dans les sucs digestifs qui
transforme les aliments en nutriments.

DEFINITION Enzyme
Des aliments aux nutriments

Microbiote intestinal

Ensemble des microorganismes présents dans
l’intestin.

DEFINITION Microbiote-intestinal
Des aliments aux nutriments

La digestion des aliments
Les aliments et les nutriments
• les aliments sont composés de grosses molécules
complexes

formées

par

l’assemblage

de

nutriments (« briques » élémentaires)
– ex. : sucres simples, acides gras, acides aminés
sont des nutriments
– ex.

: glucides, lipides, protéines sont des

molécules complexes contenues dans les
aliments
• la digestion consiste à décomposer les aliments
: les nutriments sont alors sous forme simple et
absorbables au niveau de l’intestin
• ces nutriments, une fois absorbés, peuvent servir
à construire de nouvelles molécules complexes
impliquées dans le bon fonctionnement de
l’organisme ou à produire de l’énergie

Le rôle des sucs digestifs dans la digestion
• certains organes du tube digestif libèrent des
substances appelées sucs digestifs :
– la bouche produit de la salive
– le foie produit de la bile
– l’estomac produit du suc gastrique
– le pancréas produit du suc pancréatique
– le gros intestin produit du suc intestinal
• ces sucs digestifs contiennent des molécules
appelées enzymes qui décomposent les aliments
en nutriments
• chaque enzyme peut décomposer un seul type
d’aliment
– ex. : l’amylase décompose l’amidon qui est un
glucide

LEÇON La-digestion-des-aliments
Des aliments aux nutriments

L’absorption des nutriments
Seuls les nutriments sont absorbés
• la majorité de l’absorption a lieu au niveau de
l’intestin grêle
• la paroi de l’intestin grêle n’est pas capable
d’absorber les grosses molécules des aliments, elle
n’absorbe que les nutriments : la digestion par les
enzymes est donc indispensable

La surface d’absorption de l’intestin grêle
• l’intestin grêle est un tube de 6 à 7 m de long et
d’environ 4 cm de diamètre, replié sur lui-même
• la paroi intestinale est une surface d’échanges
au travers de laquelle les nutriments passent
facilement car :
– son épaisseur est faible (de l’ordre du millième
de millimètre)
– sa surface est grande grâce à ses nombreux
replis (supérieure à celle d’un terrain de tennis)
– elle contient de nombreux capillaires sanguins

LEÇON L’absorption-des-nutriments
Des aliments aux nutriments

Le rôle du microbiote dans la
digestion
Les enzymes du microbiote intestinal
• les microorganismes vivant dans notre tube
digestif (microbiote) libèrent elles également des
enzymes
• les enzymes libérées par le microbiote sont
beaucoup plus nombreuses et variées que celles
libérées par les organes de notre tube digestif
• le microbiote participe donc beaucoup à la
digestion chimique des aliments en nutriments

Le rôle du microbiote dans l’absorption
intestinale
• le microbiote facilite l’absorption intestinale des
nutriments

L’importance du microbiote pour le bon
fonctionnement
• on observe des différences entre le microbiote d’un
individu sain et celui d’un individu obèse
• des chercheurs essayent donc de lutter contre
l’obésité en apportant des bactéries qui vont aller
dans le tube digestif des malades
• il est possible d’ajouter des bactéries au microbiote
d’un individu en prenant des probiotiques

LEÇON Le-rôle-du-microbiote-dans-la-digestion
