La France dans la Seconde
Guerre mondiale

La France dans la Seconde Guerre
mondiale

SCHEMA La-France-dans-la-Seconde-Guerre-mondiale
La France dans la Seconde
Guerre mondiale

Schéma de révision

SCHEMA Schéma-de-révision
La France dans la Seconde
Guerre mondiale

Armistice

Arrêt des combats.

DEFINITION Armistice
La France dans la Seconde
Guerre mondiale

Collaboration

Politique de coopération avec l’Allemagne nazie
pratiquée en France par le régime de Vichy mais
aussi par certains Français de 1940 à 1944.

DEFINITION Collaboration
La France dans la Seconde
Guerre mondiale

Forces françaises de l’intérieur (FFI)

Regroupement de l’ensemble des groupes armés
de la Résistance intérieure à partir de 1944.

DEFINITION Forces-françaises-de-l’intérieur-FFI
La France dans la Seconde
Guerre mondiale

Maquis

Région isolée, souvent en montagne ou dans
des zones à la végétation dense, servant de base
arrière d’opération pour un groupe de résistants.

DEFINITION Maquis
La France dans la Seconde
Guerre mondiale

Milice

Organisation policière créée en 1943 par le régime
de Vichy pour traquer les Juifs, les réfractaires au
STO et les résistants.

DEFINITION Milice
La France dans la Seconde
Guerre mondiale

Régime autoritaire

Régime politique dans lequel une seule personne
concentre beaucoup de pouvoir.

DEFINITION Régime-autoritaire
La France dans la Seconde
Guerre mondiale

Réseau

Organisatoin

clandestine

de

la

Résistance

intérieure menant des activités de sabotage,
de renseignement ou d’évasion.

DEFINITION Réseau
La France dans la Seconde
Guerre mondiale

Résistance

Ensemble des organisations clandestines luttant
contre

l’occupation

allemande

Seconde Guerre mondiale.

pendant

la

DEFINITION Résistance
La France dans la Seconde
Guerre mondiale

Service du travail obligatoire (STO)

Réquisition et envoi de jeunes travailleurs français
vers l’Allemagne, à partir de 1943.

DEFINITION Service-du-travail-obligatoire-STO
La France dans la Seconde
Guerre mondiale

Chronologie

18/06/1940

•

Appel du général de Gaulle

22/06/1940

•

Armistice qui marque la
défaite de la France

24/10/1940

•

Début de la collaboration

05/1943

•

Création du CNR

06/06/1944

•

Débarquement en
Normandie

08/1944

•

Libération de Paris par la
Résistance et la 2e DB

CHRONO La-France-dans-la-Seconde-Guerre-mondiale
La France dans la Seconde
Guerre mondiale

Charles de Gaulle

1890 - 1970

Biographie
Exilé à Londres, il lance un appel à la résistance. À la
Libération, il devient chef du Gouvernement provisoire.

PERSO Charles-de-Gaulle
La France dans la Seconde
Guerre mondiale

Philippe Pétain

1856 - 1951

Biographie
Vainqueur prestigieux à la bataille de Verdun, il est
chef de l’État entre 1940 et 1944. Il s’engage dans la
collaboration avec l’Allemagne.

PERSO Philippe-Pétain
La France dans la Seconde
Guerre mondiale

Jean Moulin

1899 - 1943

Biographie
Ancien préfet, il est nommé par de Gaulle à la tête du
Conseil national de la Résistance. Il est arrêté par la
police allemande et meurt en détention en juillet 1943.

PERSO Jean-Moulin
La France dans la Seconde
Guerre mondiale

Le traumatisme de la défaite
La défaite militaire
• juin 1940 : l’armée française est battue par l’armée
allemande
• 22 juin 1940 : signature de l’armistice qui entraine
l’occupation des deux tiers du pays par l’armée
allemande
• début de l’exode pour des milliers de Français qui
partent en zone libre pour fuir l’occupation

Un nouveau régime politique
• 10 juillet 1940 : les pleins pouvoirs sont donnés au
maréchal Pétain :
– ﬁn de la IIIe République
– début du régime de Vichy

LEÇON Le-traumatisme-de-la-défaite
La France dans la Seconde
Guerre mondiale

Vichy et la collaboration
Un régime fondé sur de nouvelles valeurs
• régime qui rejette les valeurs républicaines et
démocratiques
• régime autoritaire qui repose sur le culte du
maréchal Pétain
• mise en place de la « Révolution nationale » qui
applique la devise « Travail, famille, patrie »

Une collaboration active avec le régime nazi
• une collaboration d’État se met en place
• la Milice est chargée par le régime de Vichy de
traquer les Juifs, résistants et réfractaires au STO
• l’État

français

applique

antisémite nazie :
– lois d’exclusion
– déportation de 75 000 Juifs

une

politique

LEÇON Vichy-et-la-collaboration
La France dans la Seconde
Guerre mondiale

La Résistance
Naissance de la Résistance
• la Résistance nait suite à l’appel du général de
Gaulle le 18 juin 1940 en réaction au régime de
Vichy
• depuis Londres, il s’impose comme le chef de la
France libre, aidé par les gouverneurs de certaines
colonies françaises

La Résistance intérieure
• les maquis s’organisent sur le territoire français
pour combattre l’occupant
• les résistants agissent dans la clandestinité au sein
de réseaux et sont fortement réprimés
• en 1943, Jean Moulin organise les différentes
organisations résistantes au sein du Conseil
national de la résistance (CNR)
• les débarquements alliés en Normandie,
Provence

et

l’action

des

forces françaises

de l’intérieur (FFI) permettent
territoire

en

la libération

du

LEÇON La-Résistance
La France dans la Seconde
Guerre mondiale

Vichy
Ville dans laquelle se réfugie le gouvernement français
en 1940. Après l’armistice, le maréchal Pétain en fait la
capitale de la zone libre.

LIEU Vichy
La France dans la Seconde
Guerre mondiale

Le maquis du Vercors
Importante base de la Résistance française, en zone libre
jusqu’en 1942.

LIEU Le-maquis-du-Vercors
La France dans la Seconde
Guerre mondiale

Paris
Entre le 14 juin 1940 et le 24 aout 1944, Paris est
occupée et déclarée ville ouverte, ce qui empêche sa
destruction. Elle devient le siège du commandement
militaire allemand en France.

LIEU Paris
La France dans la Seconde
Guerre mondiale

Nombre de Juifs déportés depuis la
France entre 1942 et 1944
75 000

NOMBRE Nombre-de-Juifs-déportés-depuis-la-France-entre-1942-et-1944
La France dans la Seconde
Guerre mondiale

Nombre de résistants déportés
durant la Seconde Guerre mondiale
70 000

NOMBRE Nombre-de-résistants-déportés-durant-la-Seconde-Guerre-mondiale
La France dans la Seconde
Guerre mondiale

La France dans la Seconde Guerre
mondiale

SCHEMA La-France-dans-la-Seconde-Guerre-mondiale
La France dans la Seconde
Guerre mondiale

Schéma de révision

SCHEMA Schéma-de-révision
La France dans la Seconde
Guerre mondiale

Armistice

Arrêt des combats.

DEFINITION Armistice
La France dans la Seconde
Guerre mondiale

Collaboration

Politique de coopération avec l’Allemagne nazie
pratiquée en France par le régime de Vichy mais
aussi par certains Français de 1940 à 1944.

DEFINITION Collaboration
La France dans la Seconde
Guerre mondiale

Forces françaises de l’intérieur (FFI)

Regroupement de l’ensemble des groupes armés
de la Résistance intérieure à partir de 1944.

DEFINITION Forces-françaises-de-l’intérieur-FFI
La France dans la Seconde
Guerre mondiale

Maquis

Région isolée, souvent en montagne ou dans
des zones à la végétation dense, servant de base
arrière d’opération pour un groupe de résistants.

DEFINITION Maquis
La France dans la Seconde
Guerre mondiale

Milice

Organisation policière créée en 1943 par le régime
de Vichy pour traquer les Juifs, les réfractaires au
STO et les résistants.

DEFINITION Milice
La France dans la Seconde
Guerre mondiale

Régime autoritaire

Régime politique dans lequel une seule personne
concentre beaucoup de pouvoir.

DEFINITION Régime-autoritaire
La France dans la Seconde
Guerre mondiale

Réseau

Organisatoin

clandestine

de

la

Résistance

intérieure menant des activités de sabotage,
de renseignement ou d’évasion.

DEFINITION Réseau
La France dans la Seconde
Guerre mondiale

Résistance

Ensemble des organisations clandestines luttant
contre

l’occupation

allemande

Seconde Guerre mondiale.

pendant

la

DEFINITION Résistance
La France dans la Seconde
Guerre mondiale

Service du travail obligatoire (STO)

Réquisition et envoi de jeunes travailleurs français
vers l’Allemagne, à partir de 1943.

DEFINITION Service-du-travail-obligatoire-STO
La France dans la Seconde
Guerre mondiale

Chronologie

18/06/1940

•

Appel du général de Gaulle

22/06/1940

•

Armistice qui marque la
défaite de la France

24/10/1940

•

Début de la collaboration

05/1943

•

Création du CNR

06/06/1944

•

Débarquement en
Normandie

08/1944

•

Libération de Paris par la
Résistance et la 2e DB

CHRONO La-France-dans-la-Seconde-Guerre-mondiale
La France dans la Seconde
Guerre mondiale

Charles de Gaulle

1890 - 1970

Biographie
Exilé à Londres, il lance un appel à la résistance. À la
Libération, il devient chef du Gouvernement provisoire.

PERSO Charles-de-Gaulle
La France dans la Seconde
Guerre mondiale

Philippe Pétain

1856 - 1951

Biographie
Vainqueur prestigieux à la bataille de Verdun, il est
chef de l’État entre 1940 et 1944. Il s’engage dans la
collaboration avec l’Allemagne.

PERSO Philippe-Pétain
La France dans la Seconde
Guerre mondiale

Jean Moulin

1899 - 1943

Biographie
Ancien préfet, il est nommé par de Gaulle à la tête du
Conseil national de la Résistance. Il est arrêté par la
police allemande et meurt en détention en juillet 1943.

PERSO Jean-Moulin
La France dans la Seconde
Guerre mondiale

Le traumatisme de la défaite
La défaite militaire
• juin 1940 : l’armée française est battue par l’armée
allemande
• 22 juin 1940 : signature de l’armistice qui entraine
l’occupation des deux tiers du pays par l’armée
allemande
• début de l’exode pour des milliers de Français qui
partent en zone libre pour fuir l’occupation

Un nouveau régime politique
• 10 juillet 1940 : les pleins pouvoirs sont donnés au
maréchal Pétain :
– ﬁn de la IIIe République
– début du régime de Vichy

LEÇON Le-traumatisme-de-la-défaite
La France dans la Seconde
Guerre mondiale

Vichy et la collaboration
Un régime fondé sur de nouvelles valeurs
• régime qui rejette les valeurs républicaines et
démocratiques
• régime autoritaire qui repose sur le culte du
maréchal Pétain
• mise en place de la « Révolution nationale » qui
applique la devise « Travail, famille, patrie »

Une collaboration active avec le régime nazi
• une collaboration d’État se met en place
• la Milice est chargée par le régime de Vichy de
traquer les Juifs, résistants et réfractaires au STO
• l’État

français

applique

antisémite nazie :
– lois d’exclusion
– déportation de 75 000 Juifs

une

politique

LEÇON Vichy-et-la-collaboration
La France dans la Seconde
Guerre mondiale

La Résistance
Naissance de la Résistance
• la Résistance nait suite à l’appel du général de
Gaulle le 18 juin 1940 en réaction au régime de
Vichy
• depuis Londres, il s’impose comme le chef de la
France libre, aidé par les gouverneurs de certaines
colonies françaises

La Résistance intérieure
• les maquis s’organisent sur le territoire français
pour combattre l’occupant
• les résistants agissent dans la clandestinité au sein
de réseaux et sont fortement réprimés
• en 1943, Jean Moulin organise les différentes
organisations résistantes au sein du Conseil
national de la résistance (CNR)
• les débarquements alliés en Normandie,
Provence

et

l’action

des

forces françaises

de l’intérieur (FFI) permettent
territoire

en

la libération

du

LEÇON La-Résistance
La France dans la Seconde
Guerre mondiale

Vichy
Ville dans laquelle se réfugie le gouvernement français
en 1940. Après l’armistice, le maréchal Pétain en fait la
capitale de la zone libre.

LIEU Vichy
La France dans la Seconde
Guerre mondiale

Le maquis du Vercors
Importante base de la Résistance française, en zone libre
jusqu’en 1942.

LIEU Le-maquis-du-Vercors
La France dans la Seconde
Guerre mondiale

Paris
Entre le 14 juin 1940 et le 24 aout 1944, Paris est
occupée et déclarée ville ouverte, ce qui empêche sa
destruction. Elle devient le siège du commandement
militaire allemand en France.

LIEU Paris
La France dans la Seconde
Guerre mondiale

Nombre de Juifs déportés depuis la
France entre 1942 et 1944
75 000

NOMBRE Nombre-de-Juifs-déportés-depuis-la-France-entre-1942-et-1944
La France dans la Seconde
Guerre mondiale

Nombre de résistants déportés
durant la Seconde Guerre mondiale
70 000

NOMBRE Nombre-de-résistants-déportés-durant-la-Seconde-Guerre-mondiale
La France dans la Seconde
Guerre mondiale

La France dans la Seconde Guerre
mondiale

SCHEMA La-France-dans-la-Seconde-Guerre-mondiale
La France dans la Seconde
Guerre mondiale

Schéma de révision

SCHEMA Schéma-de-révision
La France dans la Seconde
Guerre mondiale

Armistice

Arrêt des combats.

DEFINITION Armistice
La France dans la Seconde
Guerre mondiale

Collaboration

Politique de coopération avec l’Allemagne nazie
pratiquée en France par le régime de Vichy mais
aussi par certains Français de 1940 à 1944.

DEFINITION Collaboration
La France dans la Seconde
Guerre mondiale

Forces françaises de l’intérieur (FFI)

Regroupement de l’ensemble des groupes armés
de la Résistance intérieure à partir de 1944.

DEFINITION Forces-françaises-de-l’intérieur-FFI
La France dans la Seconde
Guerre mondiale

Maquis

Région isolée, souvent en montagne ou dans
des zones à la végétation dense, servant de base
arrière d’opération pour un groupe de résistants.

DEFINITION Maquis
La France dans la Seconde
Guerre mondiale

Milice

Organisation policière créée en 1943 par le régime
de Vichy pour traquer les Juifs, les réfractaires au
STO et les résistants.

DEFINITION Milice
La France dans la Seconde
Guerre mondiale

Régime autoritaire

Régime politique dans lequel une seule personne
concentre beaucoup de pouvoir.

DEFINITION Régime-autoritaire
La France dans la Seconde
Guerre mondiale

Réseau

Organisatoin

clandestine

de

la

Résistance

intérieure menant des activités de sabotage,
de renseignement ou d’évasion.

DEFINITION Réseau
La France dans la Seconde
Guerre mondiale

Résistance

Ensemble des organisations clandestines luttant
contre

l’occupation

allemande

Seconde Guerre mondiale.

pendant

la

DEFINITION Résistance
La France dans la Seconde
Guerre mondiale

Service du travail obligatoire (STO)

Réquisition et envoi de jeunes travailleurs français
vers l’Allemagne, à partir de 1943.

DEFINITION Service-du-travail-obligatoire-STO
La France dans la Seconde
Guerre mondiale

Chronologie

18/06/1940

•

Appel du général de Gaulle

22/06/1940

•

Armistice qui marque la
défaite de la France

24/10/1940

•

Début de la collaboration

05/1943

•

Création du CNR

06/06/1944

•

Débarquement en
Normandie

08/1944

•

Libération de Paris par la
Résistance et la 2e DB

CHRONO La-France-dans-la-Seconde-Guerre-mondiale
La France dans la Seconde
Guerre mondiale

Charles de Gaulle

1890 - 1970

Biographie
Exilé à Londres, il lance un appel à la résistance. À la
Libération, il devient chef du Gouvernement provisoire.

PERSO Charles-de-Gaulle
La France dans la Seconde
Guerre mondiale

Philippe Pétain

1856 - 1951

Biographie
Vainqueur prestigieux à la bataille de Verdun, il est
chef de l’État entre 1940 et 1944. Il s’engage dans la
collaboration avec l’Allemagne.

PERSO Philippe-Pétain
La France dans la Seconde
Guerre mondiale

Jean Moulin

1899 - 1943

Biographie
Ancien préfet, il est nommé par de Gaulle à la tête du
Conseil national de la Résistance. Il est arrêté par la
police allemande et meurt en détention en juillet 1943.

PERSO Jean-Moulin
La France dans la Seconde
Guerre mondiale

Le traumatisme de la défaite
La défaite militaire
• juin 1940 : l’armée française est battue par l’armée
allemande
• 22 juin 1940 : signature de l’armistice qui entraine
l’occupation des deux tiers du pays par l’armée
allemande
• début de l’exode pour des milliers de Français qui
partent en zone libre pour fuir l’occupation

Un nouveau régime politique
• 10 juillet 1940 : les pleins pouvoirs sont donnés au
maréchal Pétain :
– ﬁn de la IIIe République
– début du régime de Vichy

LEÇON Le-traumatisme-de-la-défaite
La France dans la Seconde
Guerre mondiale

Vichy et la collaboration
Un régime fondé sur de nouvelles valeurs
• régime qui rejette les valeurs républicaines et
démocratiques
• régime autoritaire qui repose sur le culte du
maréchal Pétain
• mise en place de la « Révolution nationale » qui
applique la devise « Travail, famille, patrie »

Une collaboration active avec le régime nazi
• une collaboration d’État se met en place
• la Milice est chargée par le régime de Vichy de
traquer les Juifs, résistants et réfractaires au STO
• l’État

français

applique

antisémite nazie :
– lois d’exclusion
– déportation de 75 000 Juifs

une

politique

LEÇON Vichy-et-la-collaboration
La France dans la Seconde
Guerre mondiale

La Résistance
Naissance de la Résistance
• la Résistance nait suite à l’appel du général de
Gaulle le 18 juin 1940 en réaction au régime de
Vichy
• depuis Londres, il s’impose comme le chef de la
France libre, aidé par les gouverneurs de certaines
colonies françaises

La Résistance intérieure
• les maquis s’organisent sur le territoire français
pour combattre l’occupant
• les résistants agissent dans la clandestinité au sein
de réseaux et sont fortement réprimés
• en 1943, Jean Moulin organise les différentes
organisations résistantes au sein du Conseil
national de la résistance (CNR)
• les débarquements alliés en Normandie,
Provence

et

l’action

des

forces françaises

de l’intérieur (FFI) permettent
territoire

en

la libération

du

LEÇON La-Résistance
La France dans la Seconde
Guerre mondiale

Vichy
Ville dans laquelle se réfugie le gouvernement français
en 1940. Après l’armistice, le maréchal Pétain en fait la
capitale de la zone libre.

LIEU Vichy
La France dans la Seconde
Guerre mondiale

Le maquis du Vercors
Importante base de la Résistance française, en zone libre
jusqu’en 1942.

LIEU Le-maquis-du-Vercors
La France dans la Seconde
Guerre mondiale

Paris
Entre le 14 juin 1940 et le 24 aout 1944, Paris est
occupée et déclarée ville ouverte, ce qui empêche sa
destruction. Elle devient le siège du commandement
militaire allemand en France.

LIEU Paris
La France dans la Seconde
Guerre mondiale

Nombre de Juifs déportés depuis la
France entre 1942 et 1944
75 000

NOMBRE Nombre-de-Juifs-déportés-depuis-la-France-entre-1942-et-1944
La France dans la Seconde
Guerre mondiale

Nombre de résistants déportés
durant la Seconde Guerre mondiale
70 000

NOMBRE Nombre-de-résistants-déportés-durant-la-Seconde-Guerre-mondiale
La France dans la Seconde
Guerre mondiale

La France dans la Seconde Guerre
mondiale

SCHEMA La-France-dans-la-Seconde-Guerre-mondiale
La France dans la Seconde
Guerre mondiale

Schéma de révision

SCHEMA Schéma-de-révision
La France dans la Seconde
Guerre mondiale

Armistice

Arrêt des combats.

DEFINITION Armistice
La France dans la Seconde
Guerre mondiale

Collaboration

Politique de coopération avec l’Allemagne nazie
pratiquée en France par le régime de Vichy mais
aussi par certains Français de 1940 à 1944.

DEFINITION Collaboration
La France dans la Seconde
Guerre mondiale

Forces françaises de l’intérieur (FFI)

Regroupement de l’ensemble des groupes armés
de la Résistance intérieure à partir de 1944.

DEFINITION Forces-françaises-de-l’intérieur-FFI
La France dans la Seconde
Guerre mondiale

Maquis

Région isolée, souvent en montagne ou dans
des zones à la végétation dense, servant de base
arrière d’opération pour un groupe de résistants.

DEFINITION Maquis
La France dans la Seconde
Guerre mondiale

Milice

Organisation policière créée en 1943 par le régime
de Vichy pour traquer les Juifs, les réfractaires au
STO et les résistants.

DEFINITION Milice
La France dans la Seconde
Guerre mondiale

Régime autoritaire

Régime politique dans lequel une seule personne
concentre beaucoup de pouvoir.

DEFINITION Régime-autoritaire
La France dans la Seconde
Guerre mondiale

Réseau

Organisatoin

clandestine

de

la

Résistance

intérieure menant des activités de sabotage,
de renseignement ou d’évasion.

DEFINITION Réseau
La France dans la Seconde
Guerre mondiale

Résistance

Ensemble des organisations clandestines luttant
contre

l’occupation

allemande

Seconde Guerre mondiale.

pendant

la

DEFINITION Résistance
La France dans la Seconde
Guerre mondiale

Service du travail obligatoire (STO)

Réquisition et envoi de jeunes travailleurs français
vers l’Allemagne, à partir de 1943.

DEFINITION Service-du-travail-obligatoire-STO
La France dans la Seconde
Guerre mondiale

Chronologie

18/06/1940

•

Appel du général de Gaulle

22/06/1940

•

Armistice qui marque la
défaite de la France

24/10/1940

•

Début de la collaboration

05/1943

•

Création du CNR

06/06/1944

•

Débarquement en
Normandie

08/1944

•

Libération de Paris par la
Résistance et la 2e DB

CHRONO La-France-dans-la-Seconde-Guerre-mondiale
La France dans la Seconde
Guerre mondiale

Charles de Gaulle

1890 - 1970

Biographie
Exilé à Londres, il lance un appel à la résistance. À la
Libération, il devient chef du Gouvernement provisoire.

PERSO Charles-de-Gaulle
La France dans la Seconde
Guerre mondiale

Philippe Pétain

1856 - 1951

Biographie
Vainqueur prestigieux à la bataille de Verdun, il est
chef de l’État entre 1940 et 1944. Il s’engage dans la
collaboration avec l’Allemagne.

PERSO Philippe-Pétain
La France dans la Seconde
Guerre mondiale

Jean Moulin

1899 - 1943

Biographie
Ancien préfet, il est nommé par de Gaulle à la tête du
Conseil national de la Résistance. Il est arrêté par la
police allemande et meurt en détention en juillet 1943.

PERSO Jean-Moulin
La France dans la Seconde
Guerre mondiale

Le traumatisme de la défaite
La défaite militaire
• juin 1940 : l’armée française est battue par l’armée
allemande
• 22 juin 1940 : signature de l’armistice qui entraine
l’occupation des deux tiers du pays par l’armée
allemande
• début de l’exode pour des milliers de Français qui
partent en zone libre pour fuir l’occupation

Un nouveau régime politique
• 10 juillet 1940 : les pleins pouvoirs sont donnés au
maréchal Pétain :
– ﬁn de la IIIe République
– début du régime de Vichy

LEÇON Le-traumatisme-de-la-défaite
La France dans la Seconde
Guerre mondiale

Vichy et la collaboration
Un régime fondé sur de nouvelles valeurs
• régime qui rejette les valeurs républicaines et
démocratiques
• régime autoritaire qui repose sur le culte du
maréchal Pétain
• mise en place de la « Révolution nationale » qui
applique la devise « Travail, famille, patrie »

Une collaboration active avec le régime nazi
• une collaboration d’État se met en place
• la Milice est chargée par le régime de Vichy de
traquer les Juifs, résistants et réfractaires au STO
• l’État

français

applique

antisémite nazie :
– lois d’exclusion
– déportation de 75 000 Juifs

une

politique

LEÇON Vichy-et-la-collaboration
La France dans la Seconde
Guerre mondiale

La Résistance
Naissance de la Résistance
• la Résistance nait suite à l’appel du général de
Gaulle le 18 juin 1940 en réaction au régime de
Vichy
• depuis Londres, il s’impose comme le chef de la
France libre, aidé par les gouverneurs de certaines
colonies françaises

La Résistance intérieure
• les maquis s’organisent sur le territoire français
pour combattre l’occupant
• les résistants agissent dans la clandestinité au sein
de réseaux et sont fortement réprimés
• en 1943, Jean Moulin organise les différentes
organisations résistantes au sein du Conseil
national de la résistance (CNR)
• les débarquements alliés en Normandie,
Provence

et

l’action

des

forces françaises

de l’intérieur (FFI) permettent
territoire

en

la libération

du

LEÇON La-Résistance
La France dans la Seconde
Guerre mondiale

Vichy
Ville dans laquelle se réfugie le gouvernement français
en 1940. Après l’armistice, le maréchal Pétain en fait la
capitale de la zone libre.

LIEU Vichy
La France dans la Seconde
Guerre mondiale

Le maquis du Vercors
Importante base de la Résistance française, en zone libre
jusqu’en 1942.

LIEU Le-maquis-du-Vercors
La France dans la Seconde
Guerre mondiale

Paris
Entre le 14 juin 1940 et le 24 aout 1944, Paris est
occupée et déclarée ville ouverte, ce qui empêche sa
destruction. Elle devient le siège du commandement
militaire allemand en France.

LIEU Paris
La France dans la Seconde
Guerre mondiale

Nombre de Juifs déportés depuis la
France entre 1942 et 1944
75 000

NOMBRE Nombre-de-Juifs-déportés-depuis-la-France-entre-1942-et-1944
La France dans la Seconde
Guerre mondiale

Nombre de résistants déportés
durant la Seconde Guerre mondiale
70 000

NOMBRE Nombre-de-résistants-déportés-durant-la-Seconde-Guerre-mondiale
