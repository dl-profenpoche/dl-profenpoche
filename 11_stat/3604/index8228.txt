Classes grammaticales

Classes grammaticales variables
Les noms
Déﬁnition
Les noms
Un nom désigne un être, un objet ou une idée.
Ex. : Italie, vie
Exemple
Les noms
Noms communs

table

Noms propres

Afrique

NOTION Les-noms
Classes grammaticales

Classes grammaticales variables
Les déterminants
Déﬁnition
Les déterminants
Un déterminant introduit un nom et indique
son genre (masculin ou féminin), son nombre
(singulier ou pluriel), son possesseur, etc.
Ex. : le, un, ce, ses
Exemple
Les déterminants

Article déﬁni

le, la, les

Article indéﬁni

un, une, des

Article partitif

du, de la, des

Déterminant démonstratif

ce, cet, cette, ces

Déterminant possessif

mon, nos, leur

Déterminant indéﬁni

plusieurs, certains, quelque

Déterminant interrogatif ou exclamatif

quel… ?, quelle… !

NOTION Les-déterminants
Classes grammaticales

Classes grammaticales variables
Les adjectifs
Déﬁnition
Les adjectifs
Un adjectif apporte une précision sur le nom qu’il
qualiﬁe.
Ex. : beau, intelligente
Exemple
Les adjectifs
Adjectif qualiﬁcatif

lumineux

Participe passé employé comme adjectif

fatigué

Adjectif verbal

charmant

NOTION Les-adjectifs
Classes grammaticales

Classes grammaticales variables
Les pronoms
Déﬁnition
Les pronoms
Un pronom remplace un nom ou un groupe
nominal.
Ex. : elle, celui-ci, qui
Exemple
Les pronoms
Pronom personnel

je, me, moi, le, la, les, lui, etc.

Pronom démonstratif

celui-ci, celle-là, etc.

Pronom possessif

le mien, le nôtre, etc.

Pronom indéﬁni

quelques-uns, certains, etc.

Pronom interrogatif

qui… ?, lequel… ?, etc.

Pronom relatif

qui, que, quoi, dont, où, lequel et ses composés

Pronom adverbial

en, y

Remarque
Les pronoms
De même que parmi les hommes il existe des
sosies, parmi les mots il existe des homonymes.
Par exemple, il existe un leur déterminant
possessif (Ils parlent à leurs
pronom personnel (Ils leur

élèves) et un leur
parlent). Ils ont la

même apparence, mais ce ne sont pas les mêmes
mots, ils n’ont pas la même classe grammaticale.
Pour ne pas confondre les déterminants et
les pronoms homonymes :
• le déterminant introduit un nom : <em>la
voiture </em>
• le pronom est mis à la place d’un nom ou
d’un GN : la voiture, [je la vois]

NOTION Les-pronoms
Classes grammaticales

Classes grammaticales variables
Les verbes
Déﬁnition
Les verbes
Un verbe exprime une action (marcher, agir,
etc.) ou un état (être, devenir, paraître, sembler,
demeurer, rester, avoir l’air, etc.). Il se conjugue.
Il a un mode et un temps.
Déﬁnition
Verbes attributifs et verbes d’action
• Les verbes

attributifs

se

compose

des verbes d’état (être, sembler, devenir,
paraitre, etc.) et d’autres verbes qui peuvent
être remplacés par être et qui deviennent
alors attributifs.
Ex. : Il se trouve beau. Elle est élue présidente.
Ils vécurent heureux.

•
Déﬁnition
Verbes transitifs et verbes intransitifs
Parmi les verbes d’action, on distingue :
• Les verbes intransitifs, qui se construisent
sans complément direct.

Ils peuvent par

exemple se contruire avec un complément
essentiel de lieu.
Ex. : Je marche. Elle vit à Toulouse.

• Les verbes transitifs, qui se construisent
avec un ou des compléments :
– avec un COD pour les transitifs directs
;
– avec un COI pour les transitifs indirects
;
– avec un COD + un COI ou avec deux COI
pour les verbes à double transitivité.
Ex. : J’ai envoyé un message à Virginie.
Remarque
Certains

verbes

peuvent

être

transitifs

ou

intransitifs, suivant leur construction.
Ex. : Le boulanger travaille sa pâte. ≠ Il travaille.

NOTION Les-verbes
Classes grammaticales

Classes grammaticales invariables
Les adverbes
Déﬁnition
Les adverbes
Un adverbe précise le sens d’un mot ou d’un
groupe de mots.
Ex. : <em>ici, facilement, puis</em>
Exemple
Les adverbes
Adverbe de négation

non, ne… pas, ne… plus, etc.

Adverbe de lieu, de temps, de manière

ici, aujourd’hui, facilement, etc.

Adverbe de comparaison

plus… que, autant… que, etc.

Adverbe de quantité, d’intensité

assez, environ, beaucoup, etc.

Adverbe de liaison

cependant, en effet, puis, etc.

Adverbe interrogatif

où… ?, pourquoi… ?, etc.

NOTION Les-adverbes
Classes grammaticales

Classes grammaticales invariables
Les prépositions
Déﬁnition
Les prépositions
Une préposition relie un mot à son complément.
Ex. : dans, à
Exemple
Les prépositions
à, dans, par, pour, en, vers, avec, de , sans, sous,
sur, contre, au-dessus de, etc.
Remarque
Les prépositions
Pour retenir les principales prépositions, tu peux
apprendre la phrase suivante : « Adam part pour
Anvers avec deux-cents sous ».

NOTION Les-prépositions
Classes grammaticales

Classes grammaticales invariables
Les conjonctions de coordination
Déﬁnition
Les conjonctions de coordination
Une conjonction de coordination relie des mots
ou des groupes de mots qui ont la même classe
grammaticale.
Ex. :<em> mais, ou , et , donc, or, ni, car
</em>
Remarque
Les conjonctions de coordination
Pour retenir les conjonctions de coordination, tu
peux apprendre la phrase suivante : « Mais où est
donc Ornicar ? ».

NOTION Les-conjonctions-de-coordination
Classes grammaticales

Classes grammaticales invariables
Les conjonctions de subordination
Déﬁnition
Les conjonctions de subordination
Une conjonction de subordination introduit une
proposition subordonnée.
Ex. : que, quand, dès que

Exemple
Les conjonctions de subordination
que, quand, comme, si et les composés en « que
» (puisque, lorsque, alors que, si bien que, parce
que, etc.)

NOTION Les-conjonctions-de-subordination
Classes grammaticales

Classes grammaticales invariables
Les interjections
Déﬁnition
Les interjections
Une interjection exprime un sentiment de
l’interlocuteur (la surprise, la colère).
Ex. : oh, hein, ouf, mince

NOTION Les-interjections
Classes grammaticales

Classes grammaticales invariables
Les onomatopées
Déﬁnition
Les onomatopées
Une onomatopée imite un son.
Ex. : crac, boum, plouf

NOTION Les-onomatopées
Classes grammaticales

Classes grammaticales des groupes
de mots
Déﬁnition
Les classes grammaticales des groupes de mots
Pour donner la classe grammaticale d’un groupe
de mots, tu dois identiﬁer le noyau du groupe.
Par exemple, si le noyau du groupe est un nom, il
s’agit d’un groupe nominal (GN).
Exemple
Groupe nominal ou GN

[la voiture qui est garée sur le parking]     noyau = nom

Groupe adjectival

Je suis [prêt à te suivre].noyau = adjectif

Groupe inﬁnitif

Je voudrais [me mettre à l’escrime].noyau = inﬁnitif

underline;”>é

position) J’aimerais [que vous répondiez à quelques questions].        noyau = verbe conjugué

Remarque
Quand un groupe est précédé d’une préposition,
on le précise :
• GN prépositionnel : sur la table, contre la
voiture blanche, pour l’anniversaire de mon
père 
• Groupe inﬁnitif prépositionnel : pour me
détendre, sans me tromper

NOTION Classes-grammaticales-des-groupes-de-mots
Classes grammaticales

Le cas du mot « que »
Déﬁnition
Le mot « que »
Le mot « que » peut appartenir à différentes
classes grammaticales. Il peut être :
• un pronom relatif ou interrogatif
• une conjonction de subordination
• un adverbe, de trois types différents 
– exclamatif
– qui sert à former un adverbe comparatif
– qui sert à former un adverbe négatif
restrictif
• une « béquille du subjonctif »
Exemple
Le mot que
- Pronom relatif (complète un nom) - Pronom interrogatif (complète un verbe)

- L’homme que j’ai croisé est mon voisin.- Que se passe-t-il ?

Conjonction de subordination

Je pense que tu as raison.

- Adverbe exclamatif- Sert à former un adverbe comparatif- Sert à former un adverbe négatif restrictif

- Que de monde !- Tu es plus grande que moi.- Il ne reste qu’ un
mois.

 sert à former un subjonctif d’ordre ou de souhait

Qu ’il vienne me voir !

Rappel
• Une

conjonction

de

subordination

introduit une proposition (subordonnée
conjonctive) qui complète un verbe : Je
pense que tu as raison .
• Un pronom relatif introduit une proposition
(subordonnée

relative)

qui

complète

un nom (pronom, ou GN) : L’homme
que j’ai croisé est mon voisin.

NOTION Le-cas-du-mot-«-que-»
Classes grammaticales

Les classes grammaticales variables
Nom

- nom commun : table- nom propre : Afrique

Déterminant

- article déﬁni (désigne un élément précis) : le, la, les- article indéﬁni :
un, une, des- article partitif : du, de la, des- déterminant démonstratif
: ce, cet, cette, ces- déterminant possessif : mon, nos, leur, etc.déterminant indéﬁni : plusieurs, certains, quelques, etc.- déterminant
interrogatif ou exclamatif : quel… ?, quelle… !

Adjectif

- adjectif qualiﬁcatif : lumineux- participe passé employé comme
adjectif : fatigué- adjectif verbal : charmant

Pronom

- pronom personnel :

je, me, moi, le, la, les, lui, etc.- pronom

démonstratif : celui-ci, celle-là, etc.- pronom possessif : le mien, le
nôtre, etc.- pronom indéﬁni : quelques-uns, certains, etc.- pronom
interrogatif : que… ?, qui… ?, lequel… ?, etc.- pronom relatif : qui, que,
quoi, dont, où, lequel et ses composés- pronom adverbial : en, y
Verbe

- verbe d’action : marcher, agir, etc.- verbe d’état : être, devenir,
paraître, sembler, etc.

SCHEMA Les-classes-grammaticales-variables
Classes grammaticales

Les classes grammaticales
invariables
Adverbe

- de négation : non, ne… pas, ne… plus, etc.- de lieu, temps, manière : ici,
demain, facilement, etc.- de comparaison : plus… que, autant… que,
etc.- de liaison : cependant, en effet, puis, de plus, etc.- interrogatif :
où… ?, pourquoi… ?, etc.

Préposition

à, dans, par, pour, en, vers, avec, de, sans, sous, etc.

Conjonction de coordination

mais, ou , et , donc, or, ni, car

Conjonction de subordination

que, quand, comme, si et les composés en « que » (puisque, lorsque,

Interjection

ah, ouf, zut, pfff, oups, etc.

Onomatopée

<em>crac, boum, plouf </em>

alors que, parce que, etc.)

SCHEMA Les-classes-grammaticales-invariables
Classes grammaticales

Classes grammaticales variables
Les noms
Déﬁnition
Les noms
Un nom désigne un être, un objet ou une idée.
Ex. : Italie, vie
Exemple
Les noms
Noms communs

table

Noms propres

Afrique

NOTION Les-noms
Classes grammaticales

Classes grammaticales variables
Les déterminants
Déﬁnition
Les déterminants
Un déterminant introduit un nom et indique
son genre (masculin ou féminin), son nombre
(singulier ou pluriel), son possesseur, etc.
Ex. : le, un, ce, ses
Exemple
Les déterminants

Article déﬁni

le, la, les

Article indéﬁni

un, une, des

Article partitif

du, de la, des

Déterminant démonstratif

ce, cet, cette, ces

Déterminant possessif

mon, nos, leur

Déterminant indéﬁni

plusieurs, certains, quelque

Déterminant interrogatif ou exclamatif

quel… ?, quelle… !

NOTION Les-déterminants
Classes grammaticales

Classes grammaticales variables
Les adjectifs
Déﬁnition
Les adjectifs
Un adjectif apporte une précision sur le nom qu’il
qualiﬁe.
Ex. : beau, intelligente
Exemple
Les adjectifs
Adjectif qualiﬁcatif

lumineux

Participe passé employé comme adjectif

fatigué

Adjectif verbal

charmant

NOTION Les-adjectifs
Classes grammaticales

Classes grammaticales variables
Les pronoms
Déﬁnition
Les pronoms
Un pronom remplace un nom ou un groupe
nominal.
Ex. : elle, celui-ci, qui
Exemple
Les pronoms
Pronom personnel

je, me, moi, le, la, les, lui, etc.

Pronom démonstratif

celui-ci, celle-là, etc.

Pronom possessif

le mien, le nôtre, etc.

Pronom indéﬁni

quelques-uns, certains, etc.

Pronom interrogatif

qui… ?, lequel… ?, etc.

Pronom relatif

qui, que, quoi, dont, où, lequel et ses composés

Pronom adverbial

en, y

Remarque
Les pronoms
De même que parmi les hommes il existe des
sosies, parmi les mots il existe des homonymes.
Par exemple, il existe un leur déterminant
possessif (Ils parlent à leurs
pronom personnel (Ils leur

élèves) et un leur
parlent). Ils ont la

même apparence, mais ce ne sont pas les mêmes
mots, ils n’ont pas la même classe grammaticale.
Pour ne pas confondre les déterminants et
les pronoms homonymes :
• le déterminant introduit un nom : <em>la
voiture </em>
• le pronom est mis à la place d’un nom ou
d’un GN : la voiture, [je la vois]

NOTION Les-pronoms
Classes grammaticales

Classes grammaticales variables
Les verbes
Déﬁnition
Les verbes
Un verbe exprime une action (marcher, agir,
etc.) ou un état (être, devenir, paraître, sembler,
demeurer, rester, avoir l’air, etc.). Il se conjugue.
Il a un mode et un temps.
Déﬁnition
Verbes attributifs et verbes d’action
• Les verbes

attributifs

se

compose

des verbes d’état (être, sembler, devenir,
paraitre, etc.) et d’autres verbes qui peuvent
être remplacés par être et qui deviennent
alors attributifs.
Ex. : Il se trouve beau. Elle est élue présidente.
Ils vécurent heureux.

•
Déﬁnition
Verbes transitifs et verbes intransitifs
Parmi les verbes d’action, on distingue :
• Les verbes intransitifs, qui se construisent
sans complément direct.

Ils peuvent par

exemple se contruire avec un complément
essentiel de lieu.
Ex. : Je marche. Elle vit à Toulouse.

• Les verbes transitifs, qui se construisent
avec un ou des compléments :
– avec un COD pour les transitifs directs
;
– avec un COI pour les transitifs indirects
;
– avec un COD + un COI ou avec deux COI
pour les verbes à double transitivité.
Ex. : J’ai envoyé un message à Virginie.
Remarque
Certains

verbes

peuvent

être

transitifs

ou

intransitifs, suivant leur construction.
Ex. : Le boulanger travaille sa pâte. ≠ Il travaille.

NOTION Les-verbes
Classes grammaticales

Classes grammaticales invariables
Les adverbes
Déﬁnition
Les adverbes
Un adverbe précise le sens d’un mot ou d’un
groupe de mots.
Ex. : <em>ici, facilement, puis</em>
Exemple
Les adverbes
Adverbe de négation

non, ne… pas, ne… plus, etc.

Adverbe de lieu, de temps, de manière

ici, aujourd’hui, facilement, etc.

Adverbe de comparaison

plus… que, autant… que, etc.

Adverbe de quantité, d’intensité

assez, environ, beaucoup, etc.

Adverbe de liaison

cependant, en effet, puis, etc.

Adverbe interrogatif

où… ?, pourquoi… ?, etc.

NOTION Les-adverbes
Classes grammaticales

Classes grammaticales invariables
Les prépositions
Déﬁnition
Les prépositions
Une préposition relie un mot à son complément.
Ex. : dans, à
Exemple
Les prépositions
à, dans, par, pour, en, vers, avec, de , sans, sous,
sur, contre, au-dessus de, etc.
Remarque
Les prépositions
Pour retenir les principales prépositions, tu peux
apprendre la phrase suivante : « Adam part pour
Anvers avec deux-cents sous ».

NOTION Les-prépositions
Classes grammaticales

Classes grammaticales invariables
Les conjonctions de coordination
Déﬁnition
Les conjonctions de coordination
Une conjonction de coordination relie des mots
ou des groupes de mots qui ont la même classe
grammaticale.
Ex. :<em> mais, ou , et , donc, or, ni, car
</em>
Remarque
Les conjonctions de coordination
Pour retenir les conjonctions de coordination, tu
peux apprendre la phrase suivante : « Mais où est
donc Ornicar ? ».

NOTION Les-conjonctions-de-coordination
Classes grammaticales

Classes grammaticales invariables
Les conjonctions de subordination
Déﬁnition
Les conjonctions de subordination
Une conjonction de subordination introduit une
proposition subordonnée.
Ex. : que, quand, dès que

Exemple
Les conjonctions de subordination
que, quand, comme, si et les composés en « que
» (puisque, lorsque, alors que, si bien que, parce
que, etc.)

NOTION Les-conjonctions-de-subordination
Classes grammaticales

Classes grammaticales invariables
Les interjections
Déﬁnition
Les interjections
Une interjection exprime un sentiment de
l’interlocuteur (la surprise, la colère).
Ex. : oh, hein, ouf, mince

NOTION Les-interjections
Classes grammaticales

Classes grammaticales invariables
Les onomatopées
Déﬁnition
Les onomatopées
Une onomatopée imite un son.
Ex. : crac, boum, plouf

NOTION Les-onomatopées
Classes grammaticales

Classes grammaticales des groupes
de mots
Déﬁnition
Les classes grammaticales des groupes de mots
Pour donner la classe grammaticale d’un groupe
de mots, tu dois identiﬁer le noyau du groupe.
Par exemple, si le noyau du groupe est un nom, il
s’agit d’un groupe nominal (GN).
Exemple
Groupe nominal ou GN

[la voiture qui est garée sur le parking]     noyau = nom

Groupe adjectival

Je suis [prêt à te suivre].noyau = adjectif

Groupe inﬁnitif

Je voudrais [me mettre à l’escrime].noyau = inﬁnitif

underline;”>é

position) J’aimerais [que vous répondiez à quelques questions].        noyau = verbe conjugué

Remarque
Quand un groupe est précédé d’une préposition,
on le précise :
• GN prépositionnel : sur la table, contre la
voiture blanche, pour l’anniversaire de mon
père 
• Groupe inﬁnitif prépositionnel : pour me
détendre, sans me tromper

NOTION Classes-grammaticales-des-groupes-de-mots
Classes grammaticales

Le cas du mot « que »
Déﬁnition
Le mot « que »
Le mot « que » peut appartenir à différentes
classes grammaticales. Il peut être :
• un pronom relatif ou interrogatif
• une conjonction de subordination
• un adverbe, de trois types différents 
– exclamatif
– qui sert à former un adverbe comparatif
– qui sert à former un adverbe négatif
restrictif
• une « béquille du subjonctif »
Exemple
Le mot que
- Pronom relatif (complète un nom) - Pronom interrogatif (complète un verbe)

- L’homme que j’ai croisé est mon voisin.- Que se passe-t-il ?

Conjonction de subordination

Je pense que tu as raison.

- Adverbe exclamatif- Sert à former un adverbe comparatif- Sert à former un adverbe négatif restrictif

- Que de monde !- Tu es plus grande que moi.- Il ne reste qu’ un
mois.

 sert à former un subjonctif d’ordre ou de souhait

Qu ’il vienne me voir !

Rappel
• Une

conjonction

de

subordination

introduit une proposition (subordonnée
conjonctive) qui complète un verbe : Je
pense que tu as raison .
• Un pronom relatif introduit une proposition
(subordonnée

relative)

qui

complète

un nom (pronom, ou GN) : L’homme
que j’ai croisé est mon voisin.

NOTION Le-cas-du-mot-«-que-»
Classes grammaticales

Les classes grammaticales variables
Nom

- nom commun : table- nom propre : Afrique

Déterminant

- article déﬁni (désigne un élément précis) : le, la, les- article indéﬁni :
un, une, des- article partitif : du, de la, des- déterminant démonstratif
: ce, cet, cette, ces- déterminant possessif : mon, nos, leur, etc.déterminant indéﬁni : plusieurs, certains, quelques, etc.- déterminant
interrogatif ou exclamatif : quel… ?, quelle… !

Adjectif

- adjectif qualiﬁcatif : lumineux- participe passé employé comme
adjectif : fatigué- adjectif verbal : charmant

Pronom

- pronom personnel :

je, me, moi, le, la, les, lui, etc.- pronom

démonstratif : celui-ci, celle-là, etc.- pronom possessif : le mien, le
nôtre, etc.- pronom indéﬁni : quelques-uns, certains, etc.- pronom
interrogatif : que… ?, qui… ?, lequel… ?, etc.- pronom relatif : qui, que,
quoi, dont, où, lequel et ses composés- pronom adverbial : en, y
Verbe

- verbe d’action : marcher, agir, etc.- verbe d’état : être, devenir,
paraître, sembler, etc.

SCHEMA Les-classes-grammaticales-variables
Classes grammaticales

Les classes grammaticales
invariables
Adverbe

- de négation : non, ne… pas, ne… plus, etc.- de lieu, temps, manière : ici,
demain, facilement, etc.- de comparaison : plus… que, autant… que,
etc.- de liaison : cependant, en effet, puis, de plus, etc.- interrogatif :
où… ?, pourquoi… ?, etc.

Préposition

à, dans, par, pour, en, vers, avec, de, sans, sous, etc.

Conjonction de coordination

mais, ou , et , donc, or, ni, car

Conjonction de subordination

que, quand, comme, si et les composés en « que » (puisque, lorsque,

Interjection

ah, ouf, zut, pfff, oups, etc.

Onomatopée

<em>crac, boum, plouf </em>

alors que, parce que, etc.)

SCHEMA Les-classes-grammaticales-invariables
Classes grammaticales

Classes grammaticales variables
Les noms
Déﬁnition
Les noms
Un nom désigne un être, un objet ou une idée.
Ex. : Italie, vie
Exemple
Les noms
Noms communs

table

Noms propres

Afrique

NOTION Les-noms
Classes grammaticales

Classes grammaticales variables
Les déterminants
Déﬁnition
Les déterminants
Un déterminant introduit un nom et indique
son genre (masculin ou féminin), son nombre
(singulier ou pluriel), son possesseur, etc.
Ex. : le, un, ce, ses
Exemple
Les déterminants

Article déﬁni

le, la, les

Article indéﬁni

un, une, des

Article partitif

du, de la, des

Déterminant démonstratif

ce, cet, cette, ces

Déterminant possessif

mon, nos, leur

Déterminant indéﬁni

plusieurs, certains, quelque

Déterminant interrogatif ou exclamatif

quel… ?, quelle… !

NOTION Les-déterminants
Classes grammaticales

Classes grammaticales variables
Les adjectifs
Déﬁnition
Les adjectifs
Un adjectif apporte une précision sur le nom qu’il
qualiﬁe.
Ex. : beau, intelligente
Exemple
Les adjectifs
Adjectif qualiﬁcatif

lumineux

Participe passé employé comme adjectif

fatigué

Adjectif verbal

charmant

NOTION Les-adjectifs
Classes grammaticales

Classes grammaticales variables
Les pronoms
Déﬁnition
Les pronoms
Un pronom remplace un nom ou un groupe
nominal.
Ex. : elle, celui-ci, qui
Exemple
Les pronoms
Pronom personnel

je, me, moi, le, la, les, lui, etc.

Pronom démonstratif

celui-ci, celle-là, etc.

Pronom possessif

le mien, le nôtre, etc.

Pronom indéﬁni

quelques-uns, certains, etc.

Pronom interrogatif

qui… ?, lequel… ?, etc.

Pronom relatif

qui, que, quoi, dont, où, lequel et ses composés

Pronom adverbial

en, y

Remarque
Les pronoms
De même que parmi les hommes il existe des
sosies, parmi les mots il existe des homonymes.
Par exemple, il existe un leur déterminant
possessif (Ils parlent à leurs
pronom personnel (Ils leur

élèves) et un leur
parlent). Ils ont la

même apparence, mais ce ne sont pas les mêmes
mots, ils n’ont pas la même classe grammaticale.
Pour ne pas confondre les déterminants et
les pronoms homonymes :
• le déterminant introduit un nom : <em>la
voiture </em>
• le pronom est mis à la place d’un nom ou
d’un GN : la voiture, [je la vois]

NOTION Les-pronoms
Classes grammaticales

Classes grammaticales variables
Les verbes
Déﬁnition
Les verbes
Un verbe exprime une action (marcher, agir,
etc.) ou un état (être, devenir, paraître, sembler,
demeurer, rester, avoir l’air, etc.). Il se conjugue.
Il a un mode et un temps.
Déﬁnition
Verbes attributifs et verbes d’action
• Les verbes

attributifs

se

compose

des verbes d’état (être, sembler, devenir,
paraitre, etc.) et d’autres verbes qui peuvent
être remplacés par être et qui deviennent
alors attributifs.
Ex. : Il se trouve beau. Elle est élue présidente.
Ils vécurent heureux.

•
Déﬁnition
Verbes transitifs et verbes intransitifs
Parmi les verbes d’action, on distingue :
• Les verbes intransitifs, qui se construisent
sans complément direct.

Ils peuvent par

exemple se contruire avec un complément
essentiel de lieu.
Ex. : Je marche. Elle vit à Toulouse.

• Les verbes transitifs, qui se construisent
avec un ou des compléments :
– avec un COD pour les transitifs directs
;
– avec un COI pour les transitifs indirects
;
– avec un COD + un COI ou avec deux COI
pour les verbes à double transitivité.
Ex. : J’ai envoyé un message à Virginie.
Remarque
Certains

verbes

peuvent

être

transitifs

ou

intransitifs, suivant leur construction.
Ex. : Le boulanger travaille sa pâte. ≠ Il travaille.

NOTION Les-verbes
Classes grammaticales

Classes grammaticales invariables
Les adverbes
Déﬁnition
Les adverbes
Un adverbe précise le sens d’un mot ou d’un
groupe de mots.
Ex. : <em>ici, facilement, puis</em>
Exemple
Les adverbes
Adverbe de négation

non, ne… pas, ne… plus, etc.

Adverbe de lieu, de temps, de manière

ici, aujourd’hui, facilement, etc.

Adverbe de comparaison

plus… que, autant… que, etc.

Adverbe de quantité, d’intensité

assez, environ, beaucoup, etc.

Adverbe de liaison

cependant, en effet, puis, etc.

Adverbe interrogatif

où… ?, pourquoi… ?, etc.

NOTION Les-adverbes
Classes grammaticales

Classes grammaticales invariables
Les prépositions
Déﬁnition
Les prépositions
Une préposition relie un mot à son complément.
Ex. : dans, à
Exemple
Les prépositions
à, dans, par, pour, en, vers, avec, de , sans, sous,
sur, contre, au-dessus de, etc.
Remarque
Les prépositions
Pour retenir les principales prépositions, tu peux
apprendre la phrase suivante : « Adam part pour
Anvers avec deux-cents sous ».

NOTION Les-prépositions
Classes grammaticales

Classes grammaticales invariables
Les conjonctions de coordination
Déﬁnition
Les conjonctions de coordination
Une conjonction de coordination relie des mots
ou des groupes de mots qui ont la même classe
grammaticale.
Ex. :<em> mais, ou , et , donc, or, ni, car
</em>
Remarque
Les conjonctions de coordination
Pour retenir les conjonctions de coordination, tu
peux apprendre la phrase suivante : « Mais où est
donc Ornicar ? ».

NOTION Les-conjonctions-de-coordination
Classes grammaticales

Classes grammaticales invariables
Les conjonctions de subordination
Déﬁnition
Les conjonctions de subordination
Une conjonction de subordination introduit une
proposition subordonnée.
Ex. : que, quand, dès que

Exemple
Les conjonctions de subordination
que, quand, comme, si et les composés en « que
» (puisque, lorsque, alors que, si bien que, parce
que, etc.)

NOTION Les-conjonctions-de-subordination
Classes grammaticales

Classes grammaticales invariables
Les interjections
Déﬁnition
Les interjections
Une interjection exprime un sentiment de
l’interlocuteur (la surprise, la colère).
Ex. : oh, hein, ouf, mince

NOTION Les-interjections
Classes grammaticales

Classes grammaticales invariables
Les onomatopées
Déﬁnition
Les onomatopées
Une onomatopée imite un son.
Ex. : crac, boum, plouf

NOTION Les-onomatopées
Classes grammaticales

Classes grammaticales des groupes
de mots
Déﬁnition
Les classes grammaticales des groupes de mots
Pour donner la classe grammaticale d’un groupe
de mots, tu dois identiﬁer le noyau du groupe.
Par exemple, si le noyau du groupe est un nom, il
s’agit d’un groupe nominal (GN).
Exemple
Groupe nominal ou GN

[la voiture qui est garée sur le parking]     noyau = nom

Groupe adjectival

Je suis [prêt à te suivre].noyau = adjectif

Groupe inﬁnitif

Je voudrais [me mettre à l’escrime].noyau = inﬁnitif

underline;”>é

position) J’aimerais [que vous répondiez à quelques questions].        noyau = verbe conjugué

Remarque
Quand un groupe est précédé d’une préposition,
on le précise :
• GN prépositionnel : sur la table, contre la
voiture blanche, pour l’anniversaire de mon
père 
• Groupe inﬁnitif prépositionnel : pour me
détendre, sans me tromper

NOTION Classes-grammaticales-des-groupes-de-mots
Classes grammaticales

Le cas du mot « que »
Déﬁnition
Le mot « que »
Le mot « que » peut appartenir à différentes
classes grammaticales. Il peut être :
• un pronom relatif ou interrogatif
• une conjonction de subordination
• un adverbe, de trois types différents 
– exclamatif
– qui sert à former un adverbe comparatif
– qui sert à former un adverbe négatif
restrictif
• une « béquille du subjonctif »
Exemple
Le mot que
- Pronom relatif (complète un nom) - Pronom interrogatif (complète un verbe)

- L’homme que j’ai croisé est mon voisin.- Que se passe-t-il ?

Conjonction de subordination

Je pense que tu as raison.

- Adverbe exclamatif- Sert à former un adverbe comparatif- Sert à former un adverbe négatif restrictif

- Que de monde !- Tu es plus grande que moi.- Il ne reste qu’ un
mois.

 sert à former un subjonctif d’ordre ou de souhait

Qu ’il vienne me voir !

Rappel
• Une

conjonction

de

subordination

introduit une proposition (subordonnée
conjonctive) qui complète un verbe : Je
pense que tu as raison .
• Un pronom relatif introduit une proposition
(subordonnée

relative)

qui

complète

un nom (pronom, ou GN) : L’homme
que j’ai croisé est mon voisin.

NOTION Le-cas-du-mot-«-que-»
Classes grammaticales

Les classes grammaticales variables
Nom

- nom commun : table- nom propre : Afrique

Déterminant

- article déﬁni (désigne un élément précis) : le, la, les- article indéﬁni :
un, une, des- article partitif : du, de la, des- déterminant démonstratif
: ce, cet, cette, ces- déterminant possessif : mon, nos, leur, etc.déterminant indéﬁni : plusieurs, certains, quelques, etc.- déterminant
interrogatif ou exclamatif : quel… ?, quelle… !

Adjectif

- adjectif qualiﬁcatif : lumineux- participe passé employé comme
adjectif : fatigué- adjectif verbal : charmant

Pronom

- pronom personnel :

je, me, moi, le, la, les, lui, etc.- pronom

démonstratif : celui-ci, celle-là, etc.- pronom possessif : le mien, le
nôtre, etc.- pronom indéﬁni : quelques-uns, certains, etc.- pronom
interrogatif : que… ?, qui… ?, lequel… ?, etc.- pronom relatif : qui, que,
quoi, dont, où, lequel et ses composés- pronom adverbial : en, y
Verbe

- verbe d’action : marcher, agir, etc.- verbe d’état : être, devenir,
paraître, sembler, etc.

SCHEMA Les-classes-grammaticales-variables
Classes grammaticales

Les classes grammaticales
invariables
Adverbe

- de négation : non, ne… pas, ne… plus, etc.- de lieu, temps, manière : ici,
demain, facilement, etc.- de comparaison : plus… que, autant… que,
etc.- de liaison : cependant, en effet, puis, de plus, etc.- interrogatif :
où… ?, pourquoi… ?, etc.

Préposition

à, dans, par, pour, en, vers, avec, de, sans, sous, etc.

Conjonction de coordination

mais, ou , et , donc, or, ni, car

Conjonction de subordination

que, quand, comme, si et les composés en « que » (puisque, lorsque,

Interjection

ah, ouf, zut, pfff, oups, etc.

Onomatopée

<em>crac, boum, plouf </em>

alors que, parce que, etc.)

SCHEMA Les-classes-grammaticales-invariables
Classes grammaticales

Classes grammaticales variables
Les noms
Déﬁnition
Les noms
Un nom désigne un être, un objet ou une idée.
Ex. : Italie, vie
Exemple
Les noms
Noms communs

table

Noms propres

Afrique

NOTION Les-noms
Classes grammaticales

Classes grammaticales variables
Les déterminants
Déﬁnition
Les déterminants
Un déterminant introduit un nom et indique
son genre (masculin ou féminin), son nombre
(singulier ou pluriel), son possesseur, etc.
Ex. : le, un, ce, ses
Exemple
Les déterminants

Article déﬁni

le, la, les

Article indéﬁni

un, une, des

Article partitif

du, de la, des

Déterminant démonstratif

ce, cet, cette, ces

Déterminant possessif

mon, nos, leur

Déterminant indéﬁni

plusieurs, certains, quelque

Déterminant interrogatif ou exclamatif

quel… ?, quelle… !

NOTION Les-déterminants
Classes grammaticales

Classes grammaticales variables
Les adjectifs
Déﬁnition
Les adjectifs
Un adjectif apporte une précision sur le nom qu’il
qualiﬁe.
Ex. : beau, intelligente
Exemple
Les adjectifs
Adjectif qualiﬁcatif

lumineux

Participe passé employé comme adjectif

fatigué

Adjectif verbal

charmant

NOTION Les-adjectifs
Classes grammaticales

Classes grammaticales variables
Les pronoms
Déﬁnition
Les pronoms
Un pronom remplace un nom ou un groupe
nominal.
Ex. : elle, celui-ci, qui
Exemple
Les pronoms
Pronom personnel

je, me, moi, le, la, les, lui, etc.

Pronom démonstratif

celui-ci, celle-là, etc.

Pronom possessif

le mien, le nôtre, etc.

Pronom indéﬁni

quelques-uns, certains, etc.

Pronom interrogatif

qui… ?, lequel… ?, etc.

Pronom relatif

qui, que, quoi, dont, où, lequel et ses composés

Pronom adverbial

en, y

Remarque
Les pronoms
De même que parmi les hommes il existe des
sosies, parmi les mots il existe des homonymes.
Par exemple, il existe un leur déterminant
possessif (Ils parlent à leurs
pronom personnel (Ils leur

élèves) et un leur
parlent). Ils ont la

même apparence, mais ce ne sont pas les mêmes
mots, ils n’ont pas la même classe grammaticale.
Pour ne pas confondre les déterminants et
les pronoms homonymes :
• le déterminant introduit un nom : <em>la
voiture </em>
• le pronom est mis à la place d’un nom ou
d’un GN : la voiture, [je la vois]

NOTION Les-pronoms
Classes grammaticales

Classes grammaticales variables
Les verbes
Déﬁnition
Les verbes
Un verbe exprime une action (marcher, agir,
etc.) ou un état (être, devenir, paraître, sembler,
demeurer, rester, avoir l’air, etc.). Il se conjugue.
Il a un mode et un temps.
Déﬁnition
Verbes attributifs et verbes d’action
• Les verbes

attributifs

se

compose

des verbes d’état (être, sembler, devenir,
paraitre, etc.) et d’autres verbes qui peuvent
être remplacés par être et qui deviennent
alors attributifs.
Ex. : Il se trouve beau. Elle est élue présidente.
Ils vécurent heureux.

•
Déﬁnition
Verbes transitifs et verbes intransitifs
Parmi les verbes d’action, on distingue :
• Les verbes intransitifs, qui se construisent
sans complément direct.

Ils peuvent par

exemple se contruire avec un complément
essentiel de lieu.
Ex. : Je marche. Elle vit à Toulouse.

• Les verbes transitifs, qui se construisent
avec un ou des compléments :
– avec un COD pour les transitifs directs
;
– avec un COI pour les transitifs indirects
;
– avec un COD + un COI ou avec deux COI
pour les verbes à double transitivité.
Ex. : J’ai envoyé un message à Virginie.
Remarque
Certains

verbes

peuvent

être

transitifs

ou

intransitifs, suivant leur construction.
Ex. : Le boulanger travaille sa pâte. ≠ Il travaille.

NOTION Les-verbes
Classes grammaticales

Classes grammaticales invariables
Les adverbes
Déﬁnition
Les adverbes
Un adverbe précise le sens d’un mot ou d’un
groupe de mots.
Ex. : <em>ici, facilement, puis</em>
Exemple
Les adverbes
Adverbe de négation

non, ne… pas, ne… plus, etc.

Adverbe de lieu, de temps, de manière

ici, aujourd’hui, facilement, etc.

Adverbe de comparaison

plus… que, autant… que, etc.

Adverbe de quantité, d’intensité

assez, environ, beaucoup, etc.

Adverbe de liaison

cependant, en effet, puis, etc.

Adverbe interrogatif

où… ?, pourquoi… ?, etc.

NOTION Les-adverbes
Classes grammaticales

Classes grammaticales invariables
Les prépositions
Déﬁnition
Les prépositions
Une préposition relie un mot à son complément.
Ex. : dans, à
Exemple
Les prépositions
à, dans, par, pour, en, vers, avec, de , sans, sous,
sur, contre, au-dessus de, etc.
Remarque
Les prépositions
Pour retenir les principales prépositions, tu peux
apprendre la phrase suivante : « Adam part pour
Anvers avec deux-cents sous ».

NOTION Les-prépositions
Classes grammaticales

Classes grammaticales invariables
Les conjonctions de coordination
Déﬁnition
Les conjonctions de coordination
Une conjonction de coordination relie des mots
ou des groupes de mots qui ont la même classe
grammaticale.
Ex. :<em> mais, ou , et , donc, or, ni, car
</em>
Remarque
Les conjonctions de coordination
Pour retenir les conjonctions de coordination, tu
peux apprendre la phrase suivante : « Mais où est
donc Ornicar ? ».

NOTION Les-conjonctions-de-coordination
Classes grammaticales

Classes grammaticales invariables
Les conjonctions de subordination
Déﬁnition
Les conjonctions de subordination
Une conjonction de subordination introduit une
proposition subordonnée.
Ex. : que, quand, dès que

Exemple
Les conjonctions de subordination
que, quand, comme, si et les composés en « que
» (puisque, lorsque, alors que, si bien que, parce
que, etc.)

NOTION Les-conjonctions-de-subordination
Classes grammaticales

Classes grammaticales invariables
Les interjections
Déﬁnition
Les interjections
Une interjection exprime un sentiment de
l’interlocuteur (la surprise, la colère).
Ex. : oh, hein, ouf, mince

NOTION Les-interjections
Classes grammaticales

Classes grammaticales invariables
Les onomatopées
Déﬁnition
Les onomatopées
Une onomatopée imite un son.
Ex. : crac, boum, plouf

NOTION Les-onomatopées
Classes grammaticales

Classes grammaticales des groupes
de mots
Déﬁnition
Les classes grammaticales des groupes de mots
Pour donner la classe grammaticale d’un groupe
de mots, tu dois identiﬁer le noyau du groupe.
Par exemple, si le noyau du groupe est un nom, il
s’agit d’un groupe nominal (GN).
Exemple
Groupe nominal ou GN

[la voiture qui est garée sur le parking]     noyau = nom

Groupe adjectival

Je suis [prêt à te suivre].noyau = adjectif

Groupe inﬁnitif

Je voudrais [me mettre à l’escrime].noyau = inﬁnitif

underline;”>é

position) J’aimerais [que vous répondiez à quelques questions].        noyau = verbe conjugué

Remarque
Quand un groupe est précédé d’une préposition,
on le précise :
• GN prépositionnel : sur la table, contre la
voiture blanche, pour l’anniversaire de mon
père 
• Groupe inﬁnitif prépositionnel : pour me
détendre, sans me tromper

NOTION Classes-grammaticales-des-groupes-de-mots
Classes grammaticales

Le cas du mot « que »
Déﬁnition
Le mot « que »
Le mot « que » peut appartenir à différentes
classes grammaticales. Il peut être :
• un pronom relatif ou interrogatif
• une conjonction de subordination
• un adverbe, de trois types différents 
– exclamatif
– qui sert à former un adverbe comparatif
– qui sert à former un adverbe négatif
restrictif
• une « béquille du subjonctif »
Exemple
Le mot que
- Pronom relatif (complète un nom) - Pronom interrogatif (complète un verbe)

- L’homme que j’ai croisé est mon voisin.- Que se passe-t-il ?

Conjonction de subordination

Je pense que tu as raison.

- Adverbe exclamatif- Sert à former un adverbe comparatif- Sert à former un adverbe négatif restrictif

- Que de monde !- Tu es plus grande que moi.- Il ne reste qu’ un
mois.

 sert à former un subjonctif d’ordre ou de souhait

Qu ’il vienne me voir !

Rappel
• Une

conjonction

de

subordination

introduit une proposition (subordonnée
conjonctive) qui complète un verbe : Je
pense que tu as raison .
• Un pronom relatif introduit une proposition
(subordonnée

relative)

qui

complète

un nom (pronom, ou GN) : L’homme
que j’ai croisé est mon voisin.

NOTION Le-cas-du-mot-«-que-»
Classes grammaticales

Les classes grammaticales variables
Nom

- nom commun : table- nom propre : Afrique

Déterminant

- article déﬁni (désigne un élément précis) : le, la, les- article indéﬁni :
un, une, des- article partitif : du, de la, des- déterminant démonstratif
: ce, cet, cette, ces- déterminant possessif : mon, nos, leur, etc.déterminant indéﬁni : plusieurs, certains, quelques, etc.- déterminant
interrogatif ou exclamatif : quel… ?, quelle… !

Adjectif

- adjectif qualiﬁcatif : lumineux- participe passé employé comme
adjectif : fatigué- adjectif verbal : charmant

Pronom

- pronom personnel :

je, me, moi, le, la, les, lui, etc.- pronom

démonstratif : celui-ci, celle-là, etc.- pronom possessif : le mien, le
nôtre, etc.- pronom indéﬁni : quelques-uns, certains, etc.- pronom
interrogatif : que… ?, qui… ?, lequel… ?, etc.- pronom relatif : qui, que,
quoi, dont, où, lequel et ses composés- pronom adverbial : en, y
Verbe

- verbe d’action : marcher, agir, etc.- verbe d’état : être, devenir,
paraître, sembler, etc.

SCHEMA Les-classes-grammaticales-variables
Classes grammaticales

Les classes grammaticales
invariables
Adverbe

- de négation : non, ne… pas, ne… plus, etc.- de lieu, temps, manière : ici,
demain, facilement, etc.- de comparaison : plus… que, autant… que,
etc.- de liaison : cependant, en effet, puis, de plus, etc.- interrogatif :
où… ?, pourquoi… ?, etc.

Préposition

à, dans, par, pour, en, vers, avec, de, sans, sous, etc.

Conjonction de coordination

mais, ou , et , donc, or, ni, car

Conjonction de subordination

que, quand, comme, si et les composés en « que » (puisque, lorsque,

Interjection

ah, ouf, zut, pfff, oups, etc.

Onomatopée

<em>crac, boum, plouf </em>

alors que, parce que, etc.)

SCHEMA Les-classes-grammaticales-invariables
