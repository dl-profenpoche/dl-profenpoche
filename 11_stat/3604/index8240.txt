Se raconter, se représenter

Autobiographie

Genre dans lequel l’auteur s’engage à raconter sa
vie en toute sincérité.

DEFINITION Autobiographie
Se raconter, se représenter

Essai littéraire

Œuvre dans laquelle est développée une réﬂexion
personnelle. Le but de l’auteur est de convaincre
le lecteur. L’essai littéraire prend pour sujet les
grandes questions de la vie (ex. : l’amour, la mort,
le pouvoir, le sens de la vie, etc.).

DEFINITION Essai-littéraire
Se raconter, se représenter

Journal intime

Œuvre dans laquelle une personne raconte sa vie,
écrit ses réﬂexions au jour le jour. À priori, un
journal intime n’est pas destiné à être publié. La
personne écrit pour elle-même.

DEFINITION Journal-intime
Se raconter, se représenter

Mémoires

Dans les Mémoires (nom masculin, avec une
majuscule), l’auteur raconte sa propre vie mais
en se concentrant surtout sur les événements
historiques qu’il a vécus, auxquels il a pris part
ou dont il a été témoin. L’objectif est de faire
connaitre l’Histoire « de l’intérieur ».

DEFINITION Mémoires
Se raconter, se représenter

Roman autobiographique

Œuvre dans laquelle l’écrivain s’inspire de sa
vie pour écrire un roman.

Ainsi, la part de

vérité autobiographique peut être plus ou moins
grande.

DEFINITION Roman-autobiographique
Se raconter, se représenter

Chronologie

397 - 401

•

Première autobiographie :
Les Confessions de Saint
Augustin : comment sa
rencontre avec Dieu lui a
permis d’oublier sa vie de
pécheur

1572 - 1592

•

Essais de Montaigne qui
veut se peindre « tel qui est »,
s’obligeant ainsi à modiﬁer
son portrait et donc son
texte durant 20 ans

1739 - 1749

•

Saint-Simon, homme de
cour lors du règne de Louis
XIV et de la Régence, proﬁte
de sa position pour raconter
la vie à la cour du Roi dans
ses Mémoires

1767

•

Les Confessions de
Jean-Jacques Rousseau qui
raconte sa vie pour justiﬁer
ses actes et vaincre la
polémique entourant sa vie
assez marginale

1856 - 1939

•

Freud, fonde la psychanalyse,
interrogeant l’inconscient
dans les troubles de l’esprit,
de la personnalité et du
comportement

1975

•

Le Pacte autobiographique
de Philippe Lejeune théorise
le genre de l’autobiographie

CHRONO Se-raconter-se-représenter
Se raconter, se représenter

Les principales caractéristiques du
récit autobiographique
Les conditions de l’autobiographie
• l’auteur, le narrateur et le personnage (principal)
doivent être la même personne
• l’auteur

doit

faire

preuve

d’honnêteté

et

d’authenticité pour que son récit soit crédible et
garde tout son intérêt

Pourquoi se raconter ?
• pour essayer de mieux se connaitre
– ex. : Montaigne
• pour partager ses sentiments et/ou échapper à la
solitude
– ex. : Charles Juliet, Albert Cohen, Anne Franck
• pour avouer ses fautes et se justiﬁer
– ex. : Rousseau, Saint Augustin
• pour apporter un témoignage
– ex. : Anne Franck, Patrick Modiano, Primo Levi
• pour lutter contre l’oubli, raviver des souvenirs
– ex. : George Perec, George Sand

Comment se raconter ?
• en cherchant l’authenticité et en se livrant en
toute franchise :
– une proximité s’installe entre le narrateur et le
lecteur
– cette franchise peut aussi être une stratégie :
le lecteur doit être conscient de la possibilité
de réécriture de sa vie par l’auteur
• en faisant appel à l’imaginaire :
– le lecteur est entrainé dans l’autoﬁction, un
récit plus ou moins nuancé
– les limites entre vérité et ﬁction sont ﬂoues

LEÇON Les-principales-caractéristiques-du-récit-autobiographique
Se raconter, se représenter

Quels sont les sujets de
l’autobiographie ?
Avant tout : l’auteur-narrateur !
• les

souvenirs

d’enfance

et

d’adolescence

de l’auteur constituent le sujet privilégié des
autobiographies
– ex.

:

Jean-Jacques Rousseau, Nathalie

Sarraute, Simone de Beauvoir commencent
tous leur autobiographie par leur naissance
puis ils narrent les événements principaux de
leurs jeunes années
• la découverte du plaisir de lire apparait souvent
comme un événement primordial de leur vie, cette
rencontre a généralement lieu dans l’enfance
• l’auteur se place parfois comme le témoin
privilégié de l’Histoire et fait ainsi coïncider
son histoire avec la grande Histoire
– ex. : Primo Levi relate ses années passées en
camp de concentration
• l’auteur peut aussi s’atteler au récit de toute sa vie
de manière très détaillée
– ex.

:

Chateaubriand avec ses Mémoires

d’Outre-Tombe (4 tomes et 42 « livres »),
Rousseau avec Les Confessions (4 tomes),
Montaigne avec les Essais (4 tomes travaillés
sur 20 ans)

Mais aussi...
• des combats de vie
– ex. : Primo Levi contre l’oubli et l’antisémitisme
• de grands hommes
– ex. : Napoléon pour Dumas, Louis XIV pour
Saint-Simon
• un hommage aux disparus
– ex. : Albert Cohen et sa mère disparue dont il
restera inconsolable et qu’il racontera aﬁn de
la faire exister encore un peu

LEÇON Quels-sont-les-sujets-de-l’autobiographie
Se raconter, se représenter

Michel de Montaigne
(1533 - 1592)

Biographie
Après avoir voyagé à travers l’Europe, il devient maire de
Bordeaux, se met au service du roi, puis se retire sur ses
terres pour écrire. Il ne cessera, dès lors, de travailler à ses
Essais.

Pensée
Convaincu que « chaque homme porte en soi la forme
de l’humaine condition », il se livre à un long travail
d’analyse de soi.

Oeuvre
• Essais

AUTEUR Michel-de-Montaigne
Se raconter, se représenter

Jean-Jacques Rousseau
(1712 - 1778)

Biographie
Philosophe et écrivain majeur du siècle des Lumières,
son livre intitulé Les Confessions est souvent considéré
comme la première autobiographie moderne.

Oeuvre
• Les Confessions

AUTEUR Jean-Jacques-Rousseau
Se raconter, se représenter

George Sand
(1804 - 1876)

Biographie
Écrivain français née Aurore Dupin, elle a écrit une œuvre
d’une grande diversité dont Histoire de ma vie, une
autobiographie sous forme de lettres.

Oeuvre
• Histoire de ma vie

AUTEUR George-Sand
Se raconter, se représenter

Alexandre Dumas
(1802 - 1870)

Biographie
Écrivain

français,

historiques

et

auteur

de

nombreux

précurseur

au

théâtre

romantique.

Oeuvre
• Les Trois Mousquetaires

du

romans
drame

AUTEUR Alexandre-Dumas
Se raconter, se représenter

Romain Gary
(1914 - 1980)

Biographie
Né à Vilnius dans une famille juive.

Pour échapper

à l’antisémitisme, sa mère et lui émigrent à Nice en
1928. En 1940, il rejoint le général de Gaulle à Londres,
combat dans les Forces aériennes libres et est nommé
Compagnon de la Libération. Après la guerre, il devient
romancier, cinéaste et diplomate.

Oeuvre
• La Promesse de l’aube

AUTEUR Romain-Gary
Se raconter, se représenter

Anne Frank
(1929 - 1945)

Biographie
Jeune ﬁlle juive allemande, réfugiée aux Pays-Bas et
cachée pendant plus de deux ans pour échapper
aux nazis.

Dénoncée et déportée, elle meurt à

Bergen-Belsen.

À son retour de déportation, son

père, seul survivant, découvre le journal qu’elle a tenu
(1942-1944) et le publie.

Oeuvre
• Journal

AUTEUR Anne-Frank
Se raconter, se représenter

Patrick Modiano
(1945 - ...)

Biographie
Écrivain français dont l’œuvre a été couronnée en 2014
par le prix Nobel de littérature. Ses nombreux romans
ont le plus souvent pour cadre Paris et l’Occupation, et
pour thèmes l’absence et la quête de l’identité.

Oeuvre
• Pour que tu ne te perdes pas dans le quartier

AUTEUR Patrick-Modiano
Se raconter, se représenter

Le récit autobiographique : retrouver
sa jeunesse
« À quinze ans, lorsque je me réveillais dans cette
chambre, je tirais les rideaux, et le soleil, les promeneurs
du samedi, les bouquinistes qui ouvraient leurs boîtes,
le passage d’un autobus à plateforme, tout cela
me rassurait.

Une journée comme les autres.

La

catastrophe que je craignais, sans très bien savoir
laquelle, n’avait pas eu lieu.
bureau de mon père [...].

Je descendais dans le
Lui, vêtu de sa robe de

chambre bleue, donnait d’interminables coups de
téléphone. Il me demandait de venir le chercher, en
ﬁn d’après-midi, dans quelque hall d’hôtel où il ﬁxait
ses rendez-vous. Nous dînions à la maison. Ensuite,
nous allions voir un vieux ﬁlm ou manger un sorbet
[...]. Quelquefois nous restions tous les deux dans son
bureau, à écouter des disques ou à jouer aux échecs
[...]. Il m’accompagnait jusqu’à ma chambre et fumait
une dernière cigarette en m’expliquant ses « projets ». »
Patrick Modiano, Livret de famille, 1977.

EXTRAIT Le-récit-autobiographique-retrouver-sa-jeunesse
Se raconter, se représenter

Chercher à se connaitre
Avec les Essais, Montaigne se livre à une
tâche difﬁcile, celle de se raconter. Voici le
texte qu’il place en introduction.
« Je le destine particulièrement à mes parents et à
mes amis, aﬁn que lorsque je ne serai plus, ce qui
ne peut tarder, ils y retrouvent quelques traces de
mon caractère et de mes idées et, par là, conservent
encore plus entière et plus vive la connaissance qu’ils
ont de moi.

Si je m’étais proposé de rechercher la

faveur du public, je me serais mieux attifé et me
présenterais sous une forme étudiée pour produire
meilleur effet ; je tiens, au contraire, à ce qu’on m’y
voie en toute simplicité, tel que je suis d’habitude,
au naturel, sans que mon maintien soit composé
ou que j’use d’artiﬁce, car c’est moi que je dépeins. »
Michel de Montaigne, Essais, Avant-propos, 1580.

EXTRAIT Chercher-à-se-connaitre
Se raconter, se représenter

Un portrait « dans toute sa vérité »
« Voici le seul portrait d’homme, peint exactement
d’après nature et dans toute sa vérité, qui existe et qui
probablement existera jamais. [...]
Je forme une entreprise qui n’eut jamais d’exemple
et dont l’exécution n’aura point d’imitateur.

Je veux

montrer à mes semblables un homme dans toute
la vérité de la nature ; et cet homme ce sera moi. »
Jean-Jacques Rousseau, Les Confessions, Livre premier,
1782.

EXTRAIT Un-portrait-«-dans-toute-sa-vérité-»
Se raconter, se représenter

Autobiographie

Genre dans lequel l’auteur s’engage à raconter sa
vie en toute sincérité.

DEFINITION Autobiographie
Se raconter, se représenter

Essai littéraire

Œuvre dans laquelle est développée une réﬂexion
personnelle. Le but de l’auteur est de convaincre
le lecteur. L’essai littéraire prend pour sujet les
grandes questions de la vie (ex. : l’amour, la mort,
le pouvoir, le sens de la vie, etc.).

DEFINITION Essai-littéraire
Se raconter, se représenter

Journal intime

Œuvre dans laquelle une personne raconte sa vie,
écrit ses réﬂexions au jour le jour. À priori, un
journal intime n’est pas destiné à être publié. La
personne écrit pour elle-même.

DEFINITION Journal-intime
Se raconter, se représenter

Mémoires

Dans les Mémoires (nom masculin, avec une
majuscule), l’auteur raconte sa propre vie mais
en se concentrant surtout sur les événements
historiques qu’il a vécus, auxquels il a pris part
ou dont il a été témoin. L’objectif est de faire
connaitre l’Histoire « de l’intérieur ».

DEFINITION Mémoires
Se raconter, se représenter

Roman autobiographique

Œuvre dans laquelle l’écrivain s’inspire de sa
vie pour écrire un roman.

Ainsi, la part de

vérité autobiographique peut être plus ou moins
grande.

DEFINITION Roman-autobiographique
Se raconter, se représenter

Chronologie

397 - 401

•

Première autobiographie :
Les Confessions de Saint
Augustin : comment sa
rencontre avec Dieu lui a
permis d’oublier sa vie de
pécheur

1572 - 1592

•

Essais de Montaigne qui
veut se peindre « tel qui est »,
s’obligeant ainsi à modiﬁer
son portrait et donc son
texte durant 20 ans

1739 - 1749

•

Saint-Simon, homme de
cour lors du règne de Louis
XIV et de la Régence, proﬁte
de sa position pour raconter
la vie à la cour du Roi dans
ses Mémoires

1767

•

Les Confessions de
Jean-Jacques Rousseau qui
raconte sa vie pour justiﬁer
ses actes et vaincre la
polémique entourant sa vie
assez marginale

1856 - 1939

•

Freud, fonde la psychanalyse,
interrogeant l’inconscient
dans les troubles de l’esprit,
de la personnalité et du
comportement

1975

•

Le Pacte autobiographique
de Philippe Lejeune théorise
le genre de l’autobiographie

CHRONO Se-raconter-se-représenter
Se raconter, se représenter

Les principales caractéristiques du
récit autobiographique
Les conditions de l’autobiographie
• l’auteur, le narrateur et le personnage (principal)
doivent être la même personne
• l’auteur

doit

faire

preuve

d’honnêteté

et

d’authenticité pour que son récit soit crédible et
garde tout son intérêt

Pourquoi se raconter ?
• pour essayer de mieux se connaitre
– ex. : Montaigne
• pour partager ses sentiments et/ou échapper à la
solitude
– ex. : Charles Juliet, Albert Cohen, Anne Franck
• pour avouer ses fautes et se justiﬁer
– ex. : Rousseau, Saint Augustin
• pour apporter un témoignage
– ex. : Anne Franck, Patrick Modiano, Primo Levi
• pour lutter contre l’oubli, raviver des souvenirs
– ex. : George Perec, George Sand

Comment se raconter ?
• en cherchant l’authenticité et en se livrant en
toute franchise :
– une proximité s’installe entre le narrateur et le
lecteur
– cette franchise peut aussi être une stratégie :
le lecteur doit être conscient de la possibilité
de réécriture de sa vie par l’auteur
• en faisant appel à l’imaginaire :
– le lecteur est entrainé dans l’autoﬁction, un
récit plus ou moins nuancé
– les limites entre vérité et ﬁction sont ﬂoues

LEÇON Les-principales-caractéristiques-du-récit-autobiographique
Se raconter, se représenter

Quels sont les sujets de
l’autobiographie ?
Avant tout : l’auteur-narrateur !
• les

souvenirs

d’enfance

et

d’adolescence

de l’auteur constituent le sujet privilégié des
autobiographies
– ex.

:

Jean-Jacques Rousseau, Nathalie

Sarraute, Simone de Beauvoir commencent
tous leur autobiographie par leur naissance
puis ils narrent les événements principaux de
leurs jeunes années
• la découverte du plaisir de lire apparait souvent
comme un événement primordial de leur vie, cette
rencontre a généralement lieu dans l’enfance
• l’auteur se place parfois comme le témoin
privilégié de l’Histoire et fait ainsi coïncider
son histoire avec la grande Histoire
– ex. : Primo Levi relate ses années passées en
camp de concentration
• l’auteur peut aussi s’atteler au récit de toute sa vie
de manière très détaillée
– ex.

:

Chateaubriand avec ses Mémoires

d’Outre-Tombe (4 tomes et 42 « livres »),
Rousseau avec Les Confessions (4 tomes),
Montaigne avec les Essais (4 tomes travaillés
sur 20 ans)

Mais aussi...
• des combats de vie
– ex. : Primo Levi contre l’oubli et l’antisémitisme
• de grands hommes
– ex. : Napoléon pour Dumas, Louis XIV pour
Saint-Simon
• un hommage aux disparus
– ex. : Albert Cohen et sa mère disparue dont il
restera inconsolable et qu’il racontera aﬁn de
la faire exister encore un peu

LEÇON Quels-sont-les-sujets-de-l’autobiographie
Se raconter, se représenter

Michel de Montaigne
(1533 - 1592)

Biographie
Après avoir voyagé à travers l’Europe, il devient maire de
Bordeaux, se met au service du roi, puis se retire sur ses
terres pour écrire. Il ne cessera, dès lors, de travailler à ses
Essais.

Pensée
Convaincu que « chaque homme porte en soi la forme
de l’humaine condition », il se livre à un long travail
d’analyse de soi.

Oeuvre
• Essais

AUTEUR Michel-de-Montaigne
Se raconter, se représenter

Jean-Jacques Rousseau
(1712 - 1778)

Biographie
Philosophe et écrivain majeur du siècle des Lumières,
son livre intitulé Les Confessions est souvent considéré
comme la première autobiographie moderne.

Oeuvre
• Les Confessions

AUTEUR Jean-Jacques-Rousseau
Se raconter, se représenter

George Sand
(1804 - 1876)

Biographie
Écrivain français née Aurore Dupin, elle a écrit une œuvre
d’une grande diversité dont Histoire de ma vie, une
autobiographie sous forme de lettres.

Oeuvre
• Histoire de ma vie

AUTEUR George-Sand
Se raconter, se représenter

Alexandre Dumas
(1802 - 1870)

Biographie
Écrivain

français,

historiques

et

auteur

de

nombreux

précurseur

au

théâtre

romantique.

Oeuvre
• Les Trois Mousquetaires

du

romans
drame

AUTEUR Alexandre-Dumas
Se raconter, se représenter

Romain Gary
(1914 - 1980)

Biographie
Né à Vilnius dans une famille juive.

Pour échapper

à l’antisémitisme, sa mère et lui émigrent à Nice en
1928. En 1940, il rejoint le général de Gaulle à Londres,
combat dans les Forces aériennes libres et est nommé
Compagnon de la Libération. Après la guerre, il devient
romancier, cinéaste et diplomate.

Oeuvre
• La Promesse de l’aube

AUTEUR Romain-Gary
Se raconter, se représenter

Anne Frank
(1929 - 1945)

Biographie
Jeune ﬁlle juive allemande, réfugiée aux Pays-Bas et
cachée pendant plus de deux ans pour échapper
aux nazis.

Dénoncée et déportée, elle meurt à

Bergen-Belsen.

À son retour de déportation, son

père, seul survivant, découvre le journal qu’elle a tenu
(1942-1944) et le publie.

Oeuvre
• Journal

AUTEUR Anne-Frank
Se raconter, se représenter

Patrick Modiano
(1945 - ...)

Biographie
Écrivain français dont l’œuvre a été couronnée en 2014
par le prix Nobel de littérature. Ses nombreux romans
ont le plus souvent pour cadre Paris et l’Occupation, et
pour thèmes l’absence et la quête de l’identité.

Oeuvre
• Pour que tu ne te perdes pas dans le quartier

AUTEUR Patrick-Modiano
Se raconter, se représenter

Le récit autobiographique : retrouver
sa jeunesse
« À quinze ans, lorsque je me réveillais dans cette
chambre, je tirais les rideaux, et le soleil, les promeneurs
du samedi, les bouquinistes qui ouvraient leurs boîtes,
le passage d’un autobus à plateforme, tout cela
me rassurait.

Une journée comme les autres.

La

catastrophe que je craignais, sans très bien savoir
laquelle, n’avait pas eu lieu.
bureau de mon père [...].

Je descendais dans le
Lui, vêtu de sa robe de

chambre bleue, donnait d’interminables coups de
téléphone. Il me demandait de venir le chercher, en
ﬁn d’après-midi, dans quelque hall d’hôtel où il ﬁxait
ses rendez-vous. Nous dînions à la maison. Ensuite,
nous allions voir un vieux ﬁlm ou manger un sorbet
[...]. Quelquefois nous restions tous les deux dans son
bureau, à écouter des disques ou à jouer aux échecs
[...]. Il m’accompagnait jusqu’à ma chambre et fumait
une dernière cigarette en m’expliquant ses « projets ». »
Patrick Modiano, Livret de famille, 1977.

EXTRAIT Le-récit-autobiographique-retrouver-sa-jeunesse
Se raconter, se représenter

Chercher à se connaitre
Avec les Essais, Montaigne se livre à une
tâche difﬁcile, celle de se raconter. Voici le
texte qu’il place en introduction.
« Je le destine particulièrement à mes parents et à
mes amis, aﬁn que lorsque je ne serai plus, ce qui
ne peut tarder, ils y retrouvent quelques traces de
mon caractère et de mes idées et, par là, conservent
encore plus entière et plus vive la connaissance qu’ils
ont de moi.

Si je m’étais proposé de rechercher la

faveur du public, je me serais mieux attifé et me
présenterais sous une forme étudiée pour produire
meilleur effet ; je tiens, au contraire, à ce qu’on m’y
voie en toute simplicité, tel que je suis d’habitude,
au naturel, sans que mon maintien soit composé
ou que j’use d’artiﬁce, car c’est moi que je dépeins. »
Michel de Montaigne, Essais, Avant-propos, 1580.

EXTRAIT Chercher-à-se-connaitre
Se raconter, se représenter

Un portrait « dans toute sa vérité »
« Voici le seul portrait d’homme, peint exactement
d’après nature et dans toute sa vérité, qui existe et qui
probablement existera jamais. [...]
Je forme une entreprise qui n’eut jamais d’exemple
et dont l’exécution n’aura point d’imitateur.

Je veux

montrer à mes semblables un homme dans toute
la vérité de la nature ; et cet homme ce sera moi. »
Jean-Jacques Rousseau, Les Confessions, Livre premier,
1782.

EXTRAIT Un-portrait-«-dans-toute-sa-vérité-»
Se raconter, se représenter

Autobiographie

Genre dans lequel l’auteur s’engage à raconter sa
vie en toute sincérité.

DEFINITION Autobiographie
Se raconter, se représenter

Essai littéraire

Œuvre dans laquelle est développée une réﬂexion
personnelle. Le but de l’auteur est de convaincre
le lecteur. L’essai littéraire prend pour sujet les
grandes questions de la vie (ex. : l’amour, la mort,
le pouvoir, le sens de la vie, etc.).

DEFINITION Essai-littéraire
Se raconter, se représenter

Journal intime

Œuvre dans laquelle une personne raconte sa vie,
écrit ses réﬂexions au jour le jour. À priori, un
journal intime n’est pas destiné à être publié. La
personne écrit pour elle-même.

DEFINITION Journal-intime
Se raconter, se représenter

Mémoires

Dans les Mémoires (nom masculin, avec une
majuscule), l’auteur raconte sa propre vie mais
en se concentrant surtout sur les événements
historiques qu’il a vécus, auxquels il a pris part
ou dont il a été témoin. L’objectif est de faire
connaitre l’Histoire « de l’intérieur ».

DEFINITION Mémoires
Se raconter, se représenter

Roman autobiographique

Œuvre dans laquelle l’écrivain s’inspire de sa
vie pour écrire un roman.

Ainsi, la part de

vérité autobiographique peut être plus ou moins
grande.

DEFINITION Roman-autobiographique
Se raconter, se représenter

Chronologie

397 - 401

•

Première autobiographie :
Les Confessions de Saint
Augustin : comment sa
rencontre avec Dieu lui a
permis d’oublier sa vie de
pécheur

1572 - 1592

•

Essais de Montaigne qui
veut se peindre « tel qui est »,
s’obligeant ainsi à modiﬁer
son portrait et donc son
texte durant 20 ans

1739 - 1749

•

Saint-Simon, homme de
cour lors du règne de Louis
XIV et de la Régence, proﬁte
de sa position pour raconter
la vie à la cour du Roi dans
ses Mémoires

1767

•

Les Confessions de
Jean-Jacques Rousseau qui
raconte sa vie pour justiﬁer
ses actes et vaincre la
polémique entourant sa vie
assez marginale

1856 - 1939

•

Freud, fonde la psychanalyse,
interrogeant l’inconscient
dans les troubles de l’esprit,
de la personnalité et du
comportement

1975

•

Le Pacte autobiographique
de Philippe Lejeune théorise
le genre de l’autobiographie

CHRONO Se-raconter-se-représenter
Se raconter, se représenter

Les principales caractéristiques du
récit autobiographique
Les conditions de l’autobiographie
• l’auteur, le narrateur et le personnage (principal)
doivent être la même personne
• l’auteur

doit

faire

preuve

d’honnêteté

et

d’authenticité pour que son récit soit crédible et
garde tout son intérêt

Pourquoi se raconter ?
• pour essayer de mieux se connaitre
– ex. : Montaigne
• pour partager ses sentiments et/ou échapper à la
solitude
– ex. : Charles Juliet, Albert Cohen, Anne Franck
• pour avouer ses fautes et se justiﬁer
– ex. : Rousseau, Saint Augustin
• pour apporter un témoignage
– ex. : Anne Franck, Patrick Modiano, Primo Levi
• pour lutter contre l’oubli, raviver des souvenirs
– ex. : George Perec, George Sand

Comment se raconter ?
• en cherchant l’authenticité et en se livrant en
toute franchise :
– une proximité s’installe entre le narrateur et le
lecteur
– cette franchise peut aussi être une stratégie :
le lecteur doit être conscient de la possibilité
de réécriture de sa vie par l’auteur
• en faisant appel à l’imaginaire :
– le lecteur est entrainé dans l’autoﬁction, un
récit plus ou moins nuancé
– les limites entre vérité et ﬁction sont ﬂoues

LEÇON Les-principales-caractéristiques-du-récit-autobiographique
Se raconter, se représenter

Quels sont les sujets de
l’autobiographie ?
Avant tout : l’auteur-narrateur !
• les

souvenirs

d’enfance

et

d’adolescence

de l’auteur constituent le sujet privilégié des
autobiographies
– ex.

:

Jean-Jacques Rousseau, Nathalie

Sarraute, Simone de Beauvoir commencent
tous leur autobiographie par leur naissance
puis ils narrent les événements principaux de
leurs jeunes années
• la découverte du plaisir de lire apparait souvent
comme un événement primordial de leur vie, cette
rencontre a généralement lieu dans l’enfance
• l’auteur se place parfois comme le témoin
privilégié de l’Histoire et fait ainsi coïncider
son histoire avec la grande Histoire
– ex. : Primo Levi relate ses années passées en
camp de concentration
• l’auteur peut aussi s’atteler au récit de toute sa vie
de manière très détaillée
– ex.

:

Chateaubriand avec ses Mémoires

d’Outre-Tombe (4 tomes et 42 « livres »),
Rousseau avec Les Confessions (4 tomes),
Montaigne avec les Essais (4 tomes travaillés
sur 20 ans)

Mais aussi...
• des combats de vie
– ex. : Primo Levi contre l’oubli et l’antisémitisme
• de grands hommes
– ex. : Napoléon pour Dumas, Louis XIV pour
Saint-Simon
• un hommage aux disparus
– ex. : Albert Cohen et sa mère disparue dont il
restera inconsolable et qu’il racontera aﬁn de
la faire exister encore un peu

LEÇON Quels-sont-les-sujets-de-l’autobiographie
Se raconter, se représenter

Michel de Montaigne
(1533 - 1592)

Biographie
Après avoir voyagé à travers l’Europe, il devient maire de
Bordeaux, se met au service du roi, puis se retire sur ses
terres pour écrire. Il ne cessera, dès lors, de travailler à ses
Essais.

Pensée
Convaincu que « chaque homme porte en soi la forme
de l’humaine condition », il se livre à un long travail
d’analyse de soi.

Oeuvre
• Essais

AUTEUR Michel-de-Montaigne
Se raconter, se représenter

Jean-Jacques Rousseau
(1712 - 1778)

Biographie
Philosophe et écrivain majeur du siècle des Lumières,
son livre intitulé Les Confessions est souvent considéré
comme la première autobiographie moderne.

Oeuvre
• Les Confessions

AUTEUR Jean-Jacques-Rousseau
Se raconter, se représenter

George Sand
(1804 - 1876)

Biographie
Écrivain français née Aurore Dupin, elle a écrit une œuvre
d’une grande diversité dont Histoire de ma vie, une
autobiographie sous forme de lettres.

Oeuvre
• Histoire de ma vie

AUTEUR George-Sand
Se raconter, se représenter

Alexandre Dumas
(1802 - 1870)

Biographie
Écrivain

français,

historiques

et

auteur

de

nombreux

précurseur

au

théâtre

romantique.

Oeuvre
• Les Trois Mousquetaires

du

romans
drame

AUTEUR Alexandre-Dumas
Se raconter, se représenter

Romain Gary
(1914 - 1980)

Biographie
Né à Vilnius dans une famille juive.

Pour échapper

à l’antisémitisme, sa mère et lui émigrent à Nice en
1928. En 1940, il rejoint le général de Gaulle à Londres,
combat dans les Forces aériennes libres et est nommé
Compagnon de la Libération. Après la guerre, il devient
romancier, cinéaste et diplomate.

Oeuvre
• La Promesse de l’aube

AUTEUR Romain-Gary
Se raconter, se représenter

Anne Frank
(1929 - 1945)

Biographie
Jeune ﬁlle juive allemande, réfugiée aux Pays-Bas et
cachée pendant plus de deux ans pour échapper
aux nazis.

Dénoncée et déportée, elle meurt à

Bergen-Belsen.

À son retour de déportation, son

père, seul survivant, découvre le journal qu’elle a tenu
(1942-1944) et le publie.

Oeuvre
• Journal

AUTEUR Anne-Frank
Se raconter, se représenter

Patrick Modiano
(1945 - ...)

Biographie
Écrivain français dont l’œuvre a été couronnée en 2014
par le prix Nobel de littérature. Ses nombreux romans
ont le plus souvent pour cadre Paris et l’Occupation, et
pour thèmes l’absence et la quête de l’identité.

Oeuvre
• Pour que tu ne te perdes pas dans le quartier

AUTEUR Patrick-Modiano
Se raconter, se représenter

Le récit autobiographique : retrouver
sa jeunesse
« À quinze ans, lorsque je me réveillais dans cette
chambre, je tirais les rideaux, et le soleil, les promeneurs
du samedi, les bouquinistes qui ouvraient leurs boîtes,
le passage d’un autobus à plateforme, tout cela
me rassurait.

Une journée comme les autres.

La

catastrophe que je craignais, sans très bien savoir
laquelle, n’avait pas eu lieu.
bureau de mon père [...].

Je descendais dans le
Lui, vêtu de sa robe de

chambre bleue, donnait d’interminables coups de
téléphone. Il me demandait de venir le chercher, en
ﬁn d’après-midi, dans quelque hall d’hôtel où il ﬁxait
ses rendez-vous. Nous dînions à la maison. Ensuite,
nous allions voir un vieux ﬁlm ou manger un sorbet
[...]. Quelquefois nous restions tous les deux dans son
bureau, à écouter des disques ou à jouer aux échecs
[...]. Il m’accompagnait jusqu’à ma chambre et fumait
une dernière cigarette en m’expliquant ses « projets ». »
Patrick Modiano, Livret de famille, 1977.

EXTRAIT Le-récit-autobiographique-retrouver-sa-jeunesse
Se raconter, se représenter

Chercher à se connaitre
Avec les Essais, Montaigne se livre à une
tâche difﬁcile, celle de se raconter. Voici le
texte qu’il place en introduction.
« Je le destine particulièrement à mes parents et à
mes amis, aﬁn que lorsque je ne serai plus, ce qui
ne peut tarder, ils y retrouvent quelques traces de
mon caractère et de mes idées et, par là, conservent
encore plus entière et plus vive la connaissance qu’ils
ont de moi.

Si je m’étais proposé de rechercher la

faveur du public, je me serais mieux attifé et me
présenterais sous une forme étudiée pour produire
meilleur effet ; je tiens, au contraire, à ce qu’on m’y
voie en toute simplicité, tel que je suis d’habitude,
au naturel, sans que mon maintien soit composé
ou que j’use d’artiﬁce, car c’est moi que je dépeins. »
Michel de Montaigne, Essais, Avant-propos, 1580.

EXTRAIT Chercher-à-se-connaitre
Se raconter, se représenter

Un portrait « dans toute sa vérité »
« Voici le seul portrait d’homme, peint exactement
d’après nature et dans toute sa vérité, qui existe et qui
probablement existera jamais. [...]
Je forme une entreprise qui n’eut jamais d’exemple
et dont l’exécution n’aura point d’imitateur.

Je veux

montrer à mes semblables un homme dans toute
la vérité de la nature ; et cet homme ce sera moi. »
Jean-Jacques Rousseau, Les Confessions, Livre premier,
1782.

EXTRAIT Un-portrait-«-dans-toute-sa-vérité-»
Se raconter, se représenter

Autobiographie

Genre dans lequel l’auteur s’engage à raconter sa
vie en toute sincérité.

DEFINITION Autobiographie
Se raconter, se représenter

Essai littéraire

Œuvre dans laquelle est développée une réﬂexion
personnelle. Le but de l’auteur est de convaincre
le lecteur. L’essai littéraire prend pour sujet les
grandes questions de la vie (ex. : l’amour, la mort,
le pouvoir, le sens de la vie, etc.).

DEFINITION Essai-littéraire
Se raconter, se représenter

Journal intime

Œuvre dans laquelle une personne raconte sa vie,
écrit ses réﬂexions au jour le jour. À priori, un
journal intime n’est pas destiné à être publié. La
personne écrit pour elle-même.

DEFINITION Journal-intime
Se raconter, se représenter

Mémoires

Dans les Mémoires (nom masculin, avec une
majuscule), l’auteur raconte sa propre vie mais
en se concentrant surtout sur les événements
historiques qu’il a vécus, auxquels il a pris part
ou dont il a été témoin. L’objectif est de faire
connaitre l’Histoire « de l’intérieur ».

DEFINITION Mémoires
Se raconter, se représenter

Roman autobiographique

Œuvre dans laquelle l’écrivain s’inspire de sa
vie pour écrire un roman.

Ainsi, la part de

vérité autobiographique peut être plus ou moins
grande.

DEFINITION Roman-autobiographique
Se raconter, se représenter

Chronologie

397 - 401

•

Première autobiographie :
Les Confessions de Saint
Augustin : comment sa
rencontre avec Dieu lui a
permis d’oublier sa vie de
pécheur

1572 - 1592

•

Essais de Montaigne qui
veut se peindre « tel qui est »,
s’obligeant ainsi à modiﬁer
son portrait et donc son
texte durant 20 ans

1739 - 1749

•

Saint-Simon, homme de
cour lors du règne de Louis
XIV et de la Régence, proﬁte
de sa position pour raconter
la vie à la cour du Roi dans
ses Mémoires

1767

•

Les Confessions de
Jean-Jacques Rousseau qui
raconte sa vie pour justiﬁer
ses actes et vaincre la
polémique entourant sa vie
assez marginale

1856 - 1939

•

Freud, fonde la psychanalyse,
interrogeant l’inconscient
dans les troubles de l’esprit,
de la personnalité et du
comportement

1975

•

Le Pacte autobiographique
de Philippe Lejeune théorise
le genre de l’autobiographie

CHRONO Se-raconter-se-représenter
Se raconter, se représenter

Les principales caractéristiques du
récit autobiographique
Les conditions de l’autobiographie
• l’auteur, le narrateur et le personnage (principal)
doivent être la même personne
• l’auteur

doit

faire

preuve

d’honnêteté

et

d’authenticité pour que son récit soit crédible et
garde tout son intérêt

Pourquoi se raconter ?
• pour essayer de mieux se connaitre
– ex. : Montaigne
• pour partager ses sentiments et/ou échapper à la
solitude
– ex. : Charles Juliet, Albert Cohen, Anne Franck
• pour avouer ses fautes et se justiﬁer
– ex. : Rousseau, Saint Augustin
• pour apporter un témoignage
– ex. : Anne Franck, Patrick Modiano, Primo Levi
• pour lutter contre l’oubli, raviver des souvenirs
– ex. : George Perec, George Sand

Comment se raconter ?
• en cherchant l’authenticité et en se livrant en
toute franchise :
– une proximité s’installe entre le narrateur et le
lecteur
– cette franchise peut aussi être une stratégie :
le lecteur doit être conscient de la possibilité
de réécriture de sa vie par l’auteur
• en faisant appel à l’imaginaire :
– le lecteur est entrainé dans l’autoﬁction, un
récit plus ou moins nuancé
– les limites entre vérité et ﬁction sont ﬂoues

LEÇON Les-principales-caractéristiques-du-récit-autobiographique
Se raconter, se représenter

Quels sont les sujets de
l’autobiographie ?
Avant tout : l’auteur-narrateur !
• les

souvenirs

d’enfance

et

d’adolescence

de l’auteur constituent le sujet privilégié des
autobiographies
– ex.

:

Jean-Jacques Rousseau, Nathalie

Sarraute, Simone de Beauvoir commencent
tous leur autobiographie par leur naissance
puis ils narrent les événements principaux de
leurs jeunes années
• la découverte du plaisir de lire apparait souvent
comme un événement primordial de leur vie, cette
rencontre a généralement lieu dans l’enfance
• l’auteur se place parfois comme le témoin
privilégié de l’Histoire et fait ainsi coïncider
son histoire avec la grande Histoire
– ex. : Primo Levi relate ses années passées en
camp de concentration
• l’auteur peut aussi s’atteler au récit de toute sa vie
de manière très détaillée
– ex.

:

Chateaubriand avec ses Mémoires

d’Outre-Tombe (4 tomes et 42 « livres »),
Rousseau avec Les Confessions (4 tomes),
Montaigne avec les Essais (4 tomes travaillés
sur 20 ans)

Mais aussi...
• des combats de vie
– ex. : Primo Levi contre l’oubli et l’antisémitisme
• de grands hommes
– ex. : Napoléon pour Dumas, Louis XIV pour
Saint-Simon
• un hommage aux disparus
– ex. : Albert Cohen et sa mère disparue dont il
restera inconsolable et qu’il racontera aﬁn de
la faire exister encore un peu

LEÇON Quels-sont-les-sujets-de-l’autobiographie
Se raconter, se représenter

Michel de Montaigne
(1533 - 1592)

Biographie
Après avoir voyagé à travers l’Europe, il devient maire de
Bordeaux, se met au service du roi, puis se retire sur ses
terres pour écrire. Il ne cessera, dès lors, de travailler à ses
Essais.

Pensée
Convaincu que « chaque homme porte en soi la forme
de l’humaine condition », il se livre à un long travail
d’analyse de soi.

Oeuvre
• Essais

AUTEUR Michel-de-Montaigne
Se raconter, se représenter

Jean-Jacques Rousseau
(1712 - 1778)

Biographie
Philosophe et écrivain majeur du siècle des Lumières,
son livre intitulé Les Confessions est souvent considéré
comme la première autobiographie moderne.

Oeuvre
• Les Confessions

AUTEUR Jean-Jacques-Rousseau
Se raconter, se représenter

George Sand
(1804 - 1876)

Biographie
Écrivain français née Aurore Dupin, elle a écrit une œuvre
d’une grande diversité dont Histoire de ma vie, une
autobiographie sous forme de lettres.

Oeuvre
• Histoire de ma vie

AUTEUR George-Sand
Se raconter, se représenter

Alexandre Dumas
(1802 - 1870)

Biographie
Écrivain

français,

historiques

et

auteur

de

nombreux

précurseur

au

théâtre

romantique.

Oeuvre
• Les Trois Mousquetaires

du

romans
drame

AUTEUR Alexandre-Dumas
Se raconter, se représenter

Romain Gary
(1914 - 1980)

Biographie
Né à Vilnius dans une famille juive.

Pour échapper

à l’antisémitisme, sa mère et lui émigrent à Nice en
1928. En 1940, il rejoint le général de Gaulle à Londres,
combat dans les Forces aériennes libres et est nommé
Compagnon de la Libération. Après la guerre, il devient
romancier, cinéaste et diplomate.

Oeuvre
• La Promesse de l’aube

AUTEUR Romain-Gary
Se raconter, se représenter

Anne Frank
(1929 - 1945)

Biographie
Jeune ﬁlle juive allemande, réfugiée aux Pays-Bas et
cachée pendant plus de deux ans pour échapper
aux nazis.

Dénoncée et déportée, elle meurt à

Bergen-Belsen.

À son retour de déportation, son

père, seul survivant, découvre le journal qu’elle a tenu
(1942-1944) et le publie.

Oeuvre
• Journal

AUTEUR Anne-Frank
Se raconter, se représenter

Patrick Modiano
(1945 - ...)

Biographie
Écrivain français dont l’œuvre a été couronnée en 2014
par le prix Nobel de littérature. Ses nombreux romans
ont le plus souvent pour cadre Paris et l’Occupation, et
pour thèmes l’absence et la quête de l’identité.

Oeuvre
• Pour que tu ne te perdes pas dans le quartier

AUTEUR Patrick-Modiano
Se raconter, se représenter

Le récit autobiographique : retrouver
sa jeunesse
« À quinze ans, lorsque je me réveillais dans cette
chambre, je tirais les rideaux, et le soleil, les promeneurs
du samedi, les bouquinistes qui ouvraient leurs boîtes,
le passage d’un autobus à plateforme, tout cela
me rassurait.

Une journée comme les autres.

La

catastrophe que je craignais, sans très bien savoir
laquelle, n’avait pas eu lieu.
bureau de mon père [...].

Je descendais dans le
Lui, vêtu de sa robe de

chambre bleue, donnait d’interminables coups de
téléphone. Il me demandait de venir le chercher, en
ﬁn d’après-midi, dans quelque hall d’hôtel où il ﬁxait
ses rendez-vous. Nous dînions à la maison. Ensuite,
nous allions voir un vieux ﬁlm ou manger un sorbet
[...]. Quelquefois nous restions tous les deux dans son
bureau, à écouter des disques ou à jouer aux échecs
[...]. Il m’accompagnait jusqu’à ma chambre et fumait
une dernière cigarette en m’expliquant ses « projets ». »
Patrick Modiano, Livret de famille, 1977.

EXTRAIT Le-récit-autobiographique-retrouver-sa-jeunesse
Se raconter, se représenter

Chercher à se connaitre
Avec les Essais, Montaigne se livre à une
tâche difﬁcile, celle de se raconter. Voici le
texte qu’il place en introduction.
« Je le destine particulièrement à mes parents et à
mes amis, aﬁn que lorsque je ne serai plus, ce qui
ne peut tarder, ils y retrouvent quelques traces de
mon caractère et de mes idées et, par là, conservent
encore plus entière et plus vive la connaissance qu’ils
ont de moi.

Si je m’étais proposé de rechercher la

faveur du public, je me serais mieux attifé et me
présenterais sous une forme étudiée pour produire
meilleur effet ; je tiens, au contraire, à ce qu’on m’y
voie en toute simplicité, tel que je suis d’habitude,
au naturel, sans que mon maintien soit composé
ou que j’use d’artiﬁce, car c’est moi que je dépeins. »
Michel de Montaigne, Essais, Avant-propos, 1580.

EXTRAIT Chercher-à-se-connaitre
Se raconter, se représenter

Un portrait « dans toute sa vérité »
« Voici le seul portrait d’homme, peint exactement
d’après nature et dans toute sa vérité, qui existe et qui
probablement existera jamais. [...]
Je forme une entreprise qui n’eut jamais d’exemple
et dont l’exécution n’aura point d’imitateur.

Je veux

montrer à mes semblables un homme dans toute
la vérité de la nature ; et cet homme ce sera moi. »
Jean-Jacques Rousseau, Les Confessions, Livre premier,
1782.

EXTRAIT Un-portrait-«-dans-toute-sa-vérité-»
