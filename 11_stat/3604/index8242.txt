Agir dans la cité : individu et
pouvoir

Catharsis

Le mot signiﬁe en grec « puriﬁcation ». La tragédie
produit un effet cathartique auprès du spectateur,
dans le sens où la destinée extraordinaire du héros
et la représentation du chaos doivent conduire
à une réﬂexion d’ordre religieux, politique ou
philosophique.

DEFINITION Catharsis
Agir dans la cité : individu et
pouvoir

Mythe

Récit

légendaire

mettant

en

scène

des

êtres surnaturels et des héros qui frappent
l’imagination par leurs actions. Le mythe perdure
à travers les siècles, constituant un élément
fondateur dans la conscience collective.

DEFINITION Mythe
Agir dans la cité : individu et
pouvoir

Tragédie

Pièce de théâtre dont le sujet est le plus
souvent emprunté à un mythe ou une histoire
ancienne.

Les personnages sont soumis à un

destin inéluctable et confrontés à des passions,
d’où découlent des catastrophes qui s’achèvent
par une issue fatale pour le héros.

DEFINITION Tragédie
Agir dans la cité : individu et
pouvoir

Aux sources du mythe
Les œuvres antiques
• les épopées (ex. : la guerre de Troie), les mythes
gréco-latins et les intrigues des grandes familles
au destin tragique (ex. : la famille des Labdacides
et des Atrides) constituent un fondement de notre
culture occidentale
• elles marquent notre conscience collective car elles
proposent des histoires (racontées ou jouées) qui
nous impressionnent et nous font réﬂéchir

Les réécritures
• Anouilh et d’autres dramaturges du XXe siècle
(ex. : Cocteau, Giraudoux, Brecht) reprennent des
œuvres marquantes avec l’idée de les réactualiser
pour

les

inscrire

avec

des problématiques

contemporaines
– ex. : l’intrigue de La guerre de Troie n’aura
pas lieu (Giraudoux, 1935) qui prend des
résonances alarmantes à la veille d’un conﬂit
européen dans le contexte de plus en tendu
de l’entre-deux-guerres
– ex. : l’intrigue d’Antigone (Anouilh, 1944) peut
trouver un écho dans la France occupée et
l’organisation progressive de la Résistance

LEÇON Aux-sources-du-mythe
Agir dans la cité : individu et
pouvoir

Liberté de conscience et de pouvoir
Éduquer la cité et le citoyen
• la tragédie grecque avait pour but d’éduquer le
citoyen en l’amenant à réﬂéchir sur des questions
d’ordre religieux, politique, philosophique
• elle contribuait à assurer les fondements d’une cité
démocratique au plus grand nombre

Représenter l’individu dans la société
• les

œuvres

contemporaines

soulignent

les

rapports de force entre l’individu et le pouvoir
ainsi que le difﬁcile équilibre qui les unit dans leur
mise en pratique
• elles relatent des luttes pour maintenir une liberté
de conscience, et mettent en garde l’individu
face à toute tentative de pouvoir abusif ou
d’embrigadement de la pensée

LEÇON Liberté-de-conscience-et-de-pouvoir
Agir dans la cité : individu et
pouvoir

Sophocle
(496/406 - avant J.-C)

Biographie
Dramaturge grec, connu pour ses tragédie. Il a écrit
plus d’une centaine de pièces mais seules sept nous
sont parvenues. Ses pièces sont considérées comme des
œuvres littéraires majeures.

Oeuvre
• Antigone, 

AUTEUR Sophocle
Agir dans la cité : individu et
pouvoir

Jean Anouilh
(1910 - 1987)

Biographie
Dramaturge français qui puise souvent son inspiration
dans les mythes et tragédies de l’Antiquité. Antigone
est l’une de ses pièces majeures. Elle a été mise en
scène pour la première fois en 1944, sous l’occupation
allemande.

Oeuvre
• Antigone, 

AUTEUR Jean-Anouilh
Agir dans la cité : individu et
pouvoir

Force du destin : Sophocle
Dans le théâtre antique, le chœur a pour
rôle de commenter la pièce pour les
spectateurs et de délivrer le sens de la
tragédie.
LE CHŒUR. - « Quand les dieux ont une fois ébranlé une
maison, il n’est point de désastre qui n’y vienne frapper
les générations tour à tour.
On croirait voir la houle du grand large, quand, poussée
par les vents de Thrace, et par leurs brutales bourrasques,
elle court au-dessus de l’abîme marin et va roulant le
sable noir qu’elle arrache à ses profondeurs, cependant
que, sous les rafales, les caps heurtés de front gémissent
bruyamment.
Ils remontent de loin, les maux que je vois, sous le
toit des Labdacides, toujours, après les morts, s’abattre
sur les vivants, sans qu’aucune génération jamais
libère la suivante : pour les abattre, un dieu est là
qui ne leur laisse aucun répit.

L’espoir attaché à

la seule souche demeurée vivace illuminait tout le
palais d’Œdipe, et voici cet espoir fauché à son tour ! »
Sophocle, Antigone, vers 441 avant J.-C.

EXTRAIT Force-du-destin-Sophocle
Agir dans la cité : individu et
pouvoir

Force du destin : Jean Anouilh
Un décor neutre. Trois portes semblables.
Au lever du rideau, tous les personnages
sont en scène.

Ils bavardent, tricotent,

jouent aux cartes. Le Prologue se détache et
s’avance.
LE PROLOGUE. – « Voilà. Ces personnages vont vous
jouer l’histoire d’Antigone.

Antigone c’est la petite

maigre qui est assise là-bas, et qui ne dit rien.
regarde droit devant elle.

Elle pense.

Elle

Elle pense

qu’elle va être Antigone tout à l’heure, qu’elle va
surgir soudain de la maigre jeune ﬁlle noiraude et
renfermée que personne ne prenait au sérieux dans
la famille et se dresser seule en face du monde, seule
en face de Créon, son oncle, qui est le roi. Elle pense
qu’elle va mourir, qu’elle est jeune et qu’elle aussi,
elle aurait bien aimé vivre. Mais il n’y a rien à faire. »
Jean Anouilh, Antigone, 1946.

EXTRAIT Force-du-destin-Jean-Anouilh
Agir dans la cité : individu et
pouvoir

Catharsis

Le mot signiﬁe en grec « puriﬁcation ». La tragédie
produit un effet cathartique auprès du spectateur,
dans le sens où la destinée extraordinaire du héros
et la représentation du chaos doivent conduire
à une réﬂexion d’ordre religieux, politique ou
philosophique.

DEFINITION Catharsis
Agir dans la cité : individu et
pouvoir

Mythe

Récit

légendaire

mettant

en

scène

des

êtres surnaturels et des héros qui frappent
l’imagination par leurs actions. Le mythe perdure
à travers les siècles, constituant un élément
fondateur dans la conscience collective.

DEFINITION Mythe
Agir dans la cité : individu et
pouvoir

Tragédie

Pièce de théâtre dont le sujet est le plus
souvent emprunté à un mythe ou une histoire
ancienne.

Les personnages sont soumis à un

destin inéluctable et confrontés à des passions,
d’où découlent des catastrophes qui s’achèvent
par une issue fatale pour le héros.

DEFINITION Tragédie
Agir dans la cité : individu et
pouvoir

Aux sources du mythe
Les œuvres antiques
• les épopées (ex. : la guerre de Troie), les mythes
gréco-latins et les intrigues des grandes familles
au destin tragique (ex. : la famille des Labdacides
et des Atrides) constituent un fondement de notre
culture occidentale
• elles marquent notre conscience collective car elles
proposent des histoires (racontées ou jouées) qui
nous impressionnent et nous font réﬂéchir

Les réécritures
• Anouilh et d’autres dramaturges du XXe siècle
(ex. : Cocteau, Giraudoux, Brecht) reprennent des
œuvres marquantes avec l’idée de les réactualiser
pour

les

inscrire

avec

des problématiques

contemporaines
– ex. : l’intrigue de La guerre de Troie n’aura
pas lieu (Giraudoux, 1935) qui prend des
résonances alarmantes à la veille d’un conﬂit
européen dans le contexte de plus en tendu
de l’entre-deux-guerres
– ex. : l’intrigue d’Antigone (Anouilh, 1944) peut
trouver un écho dans la France occupée et
l’organisation progressive de la Résistance

LEÇON Aux-sources-du-mythe
Agir dans la cité : individu et
pouvoir

Liberté de conscience et de pouvoir
Éduquer la cité et le citoyen
• la tragédie grecque avait pour but d’éduquer le
citoyen en l’amenant à réﬂéchir sur des questions
d’ordre religieux, politique, philosophique
• elle contribuait à assurer les fondements d’une cité
démocratique au plus grand nombre

Représenter l’individu dans la société
• les

œuvres

contemporaines

soulignent

les

rapports de force entre l’individu et le pouvoir
ainsi que le difﬁcile équilibre qui les unit dans leur
mise en pratique
• elles relatent des luttes pour maintenir une liberté
de conscience, et mettent en garde l’individu
face à toute tentative de pouvoir abusif ou
d’embrigadement de la pensée

LEÇON Liberté-de-conscience-et-de-pouvoir
Agir dans la cité : individu et
pouvoir

Sophocle
(496/406 - avant J.-C)

Biographie
Dramaturge grec, connu pour ses tragédie. Il a écrit
plus d’une centaine de pièces mais seules sept nous
sont parvenues. Ses pièces sont considérées comme des
œuvres littéraires majeures.

Oeuvre
• Antigone, 

AUTEUR Sophocle
Agir dans la cité : individu et
pouvoir

Jean Anouilh
(1910 - 1987)

Biographie
Dramaturge français qui puise souvent son inspiration
dans les mythes et tragédies de l’Antiquité. Antigone
est l’une de ses pièces majeures. Elle a été mise en
scène pour la première fois en 1944, sous l’occupation
allemande.

Oeuvre
• Antigone, 

AUTEUR Jean-Anouilh
Agir dans la cité : individu et
pouvoir

Force du destin : Sophocle
Dans le théâtre antique, le chœur a pour
rôle de commenter la pièce pour les
spectateurs et de délivrer le sens de la
tragédie.
LE CHŒUR. - « Quand les dieux ont une fois ébranlé une
maison, il n’est point de désastre qui n’y vienne frapper
les générations tour à tour.
On croirait voir la houle du grand large, quand, poussée
par les vents de Thrace, et par leurs brutales bourrasques,
elle court au-dessus de l’abîme marin et va roulant le
sable noir qu’elle arrache à ses profondeurs, cependant
que, sous les rafales, les caps heurtés de front gémissent
bruyamment.
Ils remontent de loin, les maux que je vois, sous le
toit des Labdacides, toujours, après les morts, s’abattre
sur les vivants, sans qu’aucune génération jamais
libère la suivante : pour les abattre, un dieu est là
qui ne leur laisse aucun répit.

L’espoir attaché à

la seule souche demeurée vivace illuminait tout le
palais d’Œdipe, et voici cet espoir fauché à son tour ! »
Sophocle, Antigone, vers 441 avant J.-C.

EXTRAIT Force-du-destin-Sophocle
Agir dans la cité : individu et
pouvoir

Force du destin : Jean Anouilh
Un décor neutre. Trois portes semblables.
Au lever du rideau, tous les personnages
sont en scène.

Ils bavardent, tricotent,

jouent aux cartes. Le Prologue se détache et
s’avance.
LE PROLOGUE. – « Voilà. Ces personnages vont vous
jouer l’histoire d’Antigone.

Antigone c’est la petite

maigre qui est assise là-bas, et qui ne dit rien.
regarde droit devant elle.

Elle pense.

Elle

Elle pense

qu’elle va être Antigone tout à l’heure, qu’elle va
surgir soudain de la maigre jeune ﬁlle noiraude et
renfermée que personne ne prenait au sérieux dans
la famille et se dresser seule en face du monde, seule
en face de Créon, son oncle, qui est le roi. Elle pense
qu’elle va mourir, qu’elle est jeune et qu’elle aussi,
elle aurait bien aimé vivre. Mais il n’y a rien à faire. »
Jean Anouilh, Antigone, 1946.

EXTRAIT Force-du-destin-Jean-Anouilh
Agir dans la cité : individu et
pouvoir

Catharsis

Le mot signiﬁe en grec « puriﬁcation ». La tragédie
produit un effet cathartique auprès du spectateur,
dans le sens où la destinée extraordinaire du héros
et la représentation du chaos doivent conduire
à une réﬂexion d’ordre religieux, politique ou
philosophique.

DEFINITION Catharsis
Agir dans la cité : individu et
pouvoir

Mythe

Récit

légendaire

mettant

en

scène

des

êtres surnaturels et des héros qui frappent
l’imagination par leurs actions. Le mythe perdure
à travers les siècles, constituant un élément
fondateur dans la conscience collective.

DEFINITION Mythe
Agir dans la cité : individu et
pouvoir

Tragédie

Pièce de théâtre dont le sujet est le plus
souvent emprunté à un mythe ou une histoire
ancienne.

Les personnages sont soumis à un

destin inéluctable et confrontés à des passions,
d’où découlent des catastrophes qui s’achèvent
par une issue fatale pour le héros.

DEFINITION Tragédie
Agir dans la cité : individu et
pouvoir

Aux sources du mythe
Les œuvres antiques
• les épopées (ex. : la guerre de Troie), les mythes
gréco-latins et les intrigues des grandes familles
au destin tragique (ex. : la famille des Labdacides
et des Atrides) constituent un fondement de notre
culture occidentale
• elles marquent notre conscience collective car elles
proposent des histoires (racontées ou jouées) qui
nous impressionnent et nous font réﬂéchir

Les réécritures
• Anouilh et d’autres dramaturges du XXe siècle
(ex. : Cocteau, Giraudoux, Brecht) reprennent des
œuvres marquantes avec l’idée de les réactualiser
pour

les

inscrire

avec

des problématiques

contemporaines
– ex. : l’intrigue de La guerre de Troie n’aura
pas lieu (Giraudoux, 1935) qui prend des
résonances alarmantes à la veille d’un conﬂit
européen dans le contexte de plus en tendu
de l’entre-deux-guerres
– ex. : l’intrigue d’Antigone (Anouilh, 1944) peut
trouver un écho dans la France occupée et
l’organisation progressive de la Résistance

LEÇON Aux-sources-du-mythe
Agir dans la cité : individu et
pouvoir

Liberté de conscience et de pouvoir
Éduquer la cité et le citoyen
• la tragédie grecque avait pour but d’éduquer le
citoyen en l’amenant à réﬂéchir sur des questions
d’ordre religieux, politique, philosophique
• elle contribuait à assurer les fondements d’une cité
démocratique au plus grand nombre

Représenter l’individu dans la société
• les

œuvres

contemporaines

soulignent

les

rapports de force entre l’individu et le pouvoir
ainsi que le difﬁcile équilibre qui les unit dans leur
mise en pratique
• elles relatent des luttes pour maintenir une liberté
de conscience, et mettent en garde l’individu
face à toute tentative de pouvoir abusif ou
d’embrigadement de la pensée

LEÇON Liberté-de-conscience-et-de-pouvoir
Agir dans la cité : individu et
pouvoir

Sophocle
(496/406 - avant J.-C)

Biographie
Dramaturge grec, connu pour ses tragédie. Il a écrit
plus d’une centaine de pièces mais seules sept nous
sont parvenues. Ses pièces sont considérées comme des
œuvres littéraires majeures.

Oeuvre
• Antigone, 

AUTEUR Sophocle
Agir dans la cité : individu et
pouvoir

Jean Anouilh
(1910 - 1987)

Biographie
Dramaturge français qui puise souvent son inspiration
dans les mythes et tragédies de l’Antiquité. Antigone
est l’une de ses pièces majeures. Elle a été mise en
scène pour la première fois en 1944, sous l’occupation
allemande.

Oeuvre
• Antigone, 

AUTEUR Jean-Anouilh
Agir dans la cité : individu et
pouvoir

Force du destin : Sophocle
Dans le théâtre antique, le chœur a pour
rôle de commenter la pièce pour les
spectateurs et de délivrer le sens de la
tragédie.
LE CHŒUR. - « Quand les dieux ont une fois ébranlé une
maison, il n’est point de désastre qui n’y vienne frapper
les générations tour à tour.
On croirait voir la houle du grand large, quand, poussée
par les vents de Thrace, et par leurs brutales bourrasques,
elle court au-dessus de l’abîme marin et va roulant le
sable noir qu’elle arrache à ses profondeurs, cependant
que, sous les rafales, les caps heurtés de front gémissent
bruyamment.
Ils remontent de loin, les maux que je vois, sous le
toit des Labdacides, toujours, après les morts, s’abattre
sur les vivants, sans qu’aucune génération jamais
libère la suivante : pour les abattre, un dieu est là
qui ne leur laisse aucun répit.

L’espoir attaché à

la seule souche demeurée vivace illuminait tout le
palais d’Œdipe, et voici cet espoir fauché à son tour ! »
Sophocle, Antigone, vers 441 avant J.-C.

EXTRAIT Force-du-destin-Sophocle
Agir dans la cité : individu et
pouvoir

Force du destin : Jean Anouilh
Un décor neutre. Trois portes semblables.
Au lever du rideau, tous les personnages
sont en scène.

Ils bavardent, tricotent,

jouent aux cartes. Le Prologue se détache et
s’avance.
LE PROLOGUE. – « Voilà. Ces personnages vont vous
jouer l’histoire d’Antigone.

Antigone c’est la petite

maigre qui est assise là-bas, et qui ne dit rien.
regarde droit devant elle.

Elle pense.

Elle

Elle pense

qu’elle va être Antigone tout à l’heure, qu’elle va
surgir soudain de la maigre jeune ﬁlle noiraude et
renfermée que personne ne prenait au sérieux dans
la famille et se dresser seule en face du monde, seule
en face de Créon, son oncle, qui est le roi. Elle pense
qu’elle va mourir, qu’elle est jeune et qu’elle aussi,
elle aurait bien aimé vivre. Mais il n’y a rien à faire. »
Jean Anouilh, Antigone, 1946.

EXTRAIT Force-du-destin-Jean-Anouilh
Agir dans la cité : individu et
pouvoir

Catharsis

Le mot signiﬁe en grec « puriﬁcation ». La tragédie
produit un effet cathartique auprès du spectateur,
dans le sens où la destinée extraordinaire du héros
et la représentation du chaos doivent conduire
à une réﬂexion d’ordre religieux, politique ou
philosophique.

DEFINITION Catharsis
Agir dans la cité : individu et
pouvoir

Mythe

Récit

légendaire

mettant

en

scène

des

êtres surnaturels et des héros qui frappent
l’imagination par leurs actions. Le mythe perdure
à travers les siècles, constituant un élément
fondateur dans la conscience collective.

DEFINITION Mythe
Agir dans la cité : individu et
pouvoir

Tragédie

Pièce de théâtre dont le sujet est le plus
souvent emprunté à un mythe ou une histoire
ancienne.

Les personnages sont soumis à un

destin inéluctable et confrontés à des passions,
d’où découlent des catastrophes qui s’achèvent
par une issue fatale pour le héros.

DEFINITION Tragédie
Agir dans la cité : individu et
pouvoir

Aux sources du mythe
Les œuvres antiques
• les épopées (ex. : la guerre de Troie), les mythes
gréco-latins et les intrigues des grandes familles
au destin tragique (ex. : la famille des Labdacides
et des Atrides) constituent un fondement de notre
culture occidentale
• elles marquent notre conscience collective car elles
proposent des histoires (racontées ou jouées) qui
nous impressionnent et nous font réﬂéchir

Les réécritures
• Anouilh et d’autres dramaturges du XXe siècle
(ex. : Cocteau, Giraudoux, Brecht) reprennent des
œuvres marquantes avec l’idée de les réactualiser
pour

les

inscrire

avec

des problématiques

contemporaines
– ex. : l’intrigue de La guerre de Troie n’aura
pas lieu (Giraudoux, 1935) qui prend des
résonances alarmantes à la veille d’un conﬂit
européen dans le contexte de plus en tendu
de l’entre-deux-guerres
– ex. : l’intrigue d’Antigone (Anouilh, 1944) peut
trouver un écho dans la France occupée et
l’organisation progressive de la Résistance

LEÇON Aux-sources-du-mythe
Agir dans la cité : individu et
pouvoir

Liberté de conscience et de pouvoir
Éduquer la cité et le citoyen
• la tragédie grecque avait pour but d’éduquer le
citoyen en l’amenant à réﬂéchir sur des questions
d’ordre religieux, politique, philosophique
• elle contribuait à assurer les fondements d’une cité
démocratique au plus grand nombre

Représenter l’individu dans la société
• les

œuvres

contemporaines

soulignent

les

rapports de force entre l’individu et le pouvoir
ainsi que le difﬁcile équilibre qui les unit dans leur
mise en pratique
• elles relatent des luttes pour maintenir une liberté
de conscience, et mettent en garde l’individu
face à toute tentative de pouvoir abusif ou
d’embrigadement de la pensée

LEÇON Liberté-de-conscience-et-de-pouvoir
Agir dans la cité : individu et
pouvoir

Sophocle
(496/406 - avant J.-C)

Biographie
Dramaturge grec, connu pour ses tragédie. Il a écrit
plus d’une centaine de pièces mais seules sept nous
sont parvenues. Ses pièces sont considérées comme des
œuvres littéraires majeures.

Oeuvre
• Antigone, 

AUTEUR Sophocle
Agir dans la cité : individu et
pouvoir

Jean Anouilh
(1910 - 1987)

Biographie
Dramaturge français qui puise souvent son inspiration
dans les mythes et tragédies de l’Antiquité. Antigone
est l’une de ses pièces majeures. Elle a été mise en
scène pour la première fois en 1944, sous l’occupation
allemande.

Oeuvre
• Antigone, 

AUTEUR Jean-Anouilh
Agir dans la cité : individu et
pouvoir

Force du destin : Sophocle
Dans le théâtre antique, le chœur a pour
rôle de commenter la pièce pour les
spectateurs et de délivrer le sens de la
tragédie.
LE CHŒUR. - « Quand les dieux ont une fois ébranlé une
maison, il n’est point de désastre qui n’y vienne frapper
les générations tour à tour.
On croirait voir la houle du grand large, quand, poussée
par les vents de Thrace, et par leurs brutales bourrasques,
elle court au-dessus de l’abîme marin et va roulant le
sable noir qu’elle arrache à ses profondeurs, cependant
que, sous les rafales, les caps heurtés de front gémissent
bruyamment.
Ils remontent de loin, les maux que je vois, sous le
toit des Labdacides, toujours, après les morts, s’abattre
sur les vivants, sans qu’aucune génération jamais
libère la suivante : pour les abattre, un dieu est là
qui ne leur laisse aucun répit.

L’espoir attaché à

la seule souche demeurée vivace illuminait tout le
palais d’Œdipe, et voici cet espoir fauché à son tour ! »
Sophocle, Antigone, vers 441 avant J.-C.

EXTRAIT Force-du-destin-Sophocle
Agir dans la cité : individu et
pouvoir

Force du destin : Jean Anouilh
Un décor neutre. Trois portes semblables.
Au lever du rideau, tous les personnages
sont en scène.

Ils bavardent, tricotent,

jouent aux cartes. Le Prologue se détache et
s’avance.
LE PROLOGUE. – « Voilà. Ces personnages vont vous
jouer l’histoire d’Antigone.

Antigone c’est la petite

maigre qui est assise là-bas, et qui ne dit rien.
regarde droit devant elle.

Elle pense.

Elle

Elle pense

qu’elle va être Antigone tout à l’heure, qu’elle va
surgir soudain de la maigre jeune ﬁlle noiraude et
renfermée que personne ne prenait au sérieux dans
la famille et se dresser seule en face du monde, seule
en face de Créon, son oncle, qui est le roi. Elle pense
qu’elle va mourir, qu’elle est jeune et qu’elle aussi,
elle aurait bien aimé vivre. Mais il n’y a rien à faire. »
Jean Anouilh, Antigone, 1946.

EXTRAIT Force-du-destin-Jean-Anouilh
