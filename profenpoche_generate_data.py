#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 13 16:16:02 2018

@author: eisti
"""
from pathlib import Path
import re
from nltk.corpus import stopwords
import profenpocheW2V as W2V
import numpy as np

def flatten_phrase(phrase):
    phrase_flt = [num for elm in phrase for num in elm]
    return phrase_flt

def generate_data(file):
    
    data = file.read()
    caracToEli = ['.', ',', '"', "'", '?', '!', ':', ';', '(', ')', '[', ']', '{', '}', '…']
    for c in caracToEli:
        data = data.replace(c, "")
    data = data.replace("\n", " ")
    stop = set(stopwords.words('french'))
    index = [i for i in data.lower().split() if i not in stop]
    
    
    all_words =[]
    for word in index:
        vecteur, boo = W2V.representation_vectorielle(word)
        if boo:
            vecteur = list(vecteur)
            all_words.append(vecteur)
    
    X= []
    tmp = []
    i = 0
    X_flat = []
    for word in all_words:
        #print("tmp: ",tmp, "i= ", i)
        tmp.append(word)
        i = i +1
        if(i == 5):    
            #print("on est das le if!!"
            X.append(tmp)
            i = 0
            tmp = []
    #print(len(all_phrase))
    #file.name.split("index")[1].split(".txt")[0] #On récupere l'ID du chapitre
    y = [file.name.split("index")[1].split(".txt")[0] for i in range(len(X))]
    for x in X:
        X_flat.append(flatten_phrase(x))
    #print(type(np.zeros(1)))
    return X_flat, y 


X,y = [], []
path = Path('.')
list_dir = list(path.glob('**/*.txt'))
for file in list_dir:
    if re.match(r"index+[0-9]*",file.name):
        with file.open(encoding="utf8") as f:
            print("on attaque le fichier",f.name)
            tmp = generate_data(f)
            for i in range(len(tmp[0])):
                X.append(tmp[0][i])
                y.append(tmp[1][i])
            print(file.name, "is over!")
        f.closed
X = np.asarray(X)