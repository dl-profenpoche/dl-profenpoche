#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 13 09:58:57 2018

@author: eisti
"""
import gensim 
import logging
import numpy as np 
from nltk.corpus import stopwords
from sklearn.decomposition import PCA
from matplotlib import pyplot

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

maths = open('11_stat/21/maths.txt', 'r')
geo = open('11_stat/3566/geo.txt', 'r')
svt = open('11_stat/3567/svt.txt', 'r')
espagnol = open('11_stat/3578/espagnol.txt', 'r')
francais = open('11_stat/3604/francais.txt', 'r')
histoire = open('11_stat/3605/histoire.txt', 'r')
pĥysique = open('11_stat/3625/physique.txt', 'r')

#cours_concat = maths + physique + svt + geographie + histoire + francais + espagnol

'''
L'ordre choisi est arbitraire. Il va influencer les résultats du Word2Vector. 
Cet ordre a été retenu pour son respect de la proximité semantique des matières.
'''

cours_concat = open('11_stat/Cours Concat/cours_concat.txt', 'r')

#Nous retirons les mots vides de sens via la librairie stopwords. 
stop_words = set(stopwords.words('french')) 
line = cours_concat.read()
words = line.split() 
for r in words: 
    if not r in stop_words: 
        appendFile = open('cours_concat_filtered.txt','a') 
        appendFile.write(" "+r) 
        appendFile.close() 

#On obtient le fichier .txt filtré       
cours_concat = open('cours_concat_filtered.txt', 'r')

def read_input(input_file):
    """This method reads the input file which is in gzip format"""
    
    logging.info("reading file {0}...this may take a while".format(input_file))
    
    with input_file as f:
        for i, line in enumerate (f): 

            if (i%10000==0):
                logging.info ("read {0} reviews".format (i))
            # do some pre-processing and return a list of words for each review text
            yield gensim.utils.simple_preprocess (line)
    
#Lecture du corpus (List of List)
corpus = list(read_input(cours_concat))
logging.info("Lecture data terminee")

#Definition du modele Word2Vec 
model = gensim.models.Word2Vec(corpus, size=150, window=20, min_count=8, workers=10) #Taille du vecteur pour chaque mot: 150
#Entrainement
model.train(corpus,total_examples=len(corpus),epochs=10) 

def most_similar(mot):
    res = model.wv.most_similar(positive = mot)
    return print("Les mots proches de {0} sont: {1}".format(mot,res))  

def similarity(mot1, mot2):
    res = model.wv.similarity(mot1, mot2)
    return "La similarité entre {0} et {1} est de {3}".format(mot1, mot2, res)

def plot():
    X = model[model.wv.vocab]
    pca = PCA(n_components=2)
    result = pca.fit_transform(X)
    # create a scatter plot of the projection
    pyplot.scatter(result[:, 0], result[:, 1])
    words = list(model.wv.vocab)
    for i, word in enumerate(words):
        pyplot.annotate(word, xy=(result[i, 0], result[i, 1]))
    return pyplot

#Fonction qui prends un mot en entrée et retourne sa représentation vectorielle
def representation_vectorielle(mot):
    try:
        vecteur = model.wv[mot]
    except: 
        return [np.zeros(1),False]
    return [vecteur,True]
