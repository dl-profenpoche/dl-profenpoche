#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 12 15:31:29 2018

@author: eisti
"""

# imports needed and set up logging
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow import keras
import profenpocheW2V as W2V #Module du Word2Vec reéalisé à partir des données de prof en poche
import profenpoche_generate_data as gendata #Module de génération de données ProfEnPoche adaptées au framework Keras

#Affichage des mots voisins
print(W2V.most_similar("droite"))

#Affichage de la similarité entre 2 mots 
print(W2V.similarity("triangle","rectangle"))
print(W2V.similarity("triangle","rousseau"))

#Représentation du Word2Vec par un nuage de point
W2V.plot().show()

#Affichage des données
print("Le jeu de données contient {0} phrases de 5 mots".format(len(gendata.X)))

#Split du data set 
X_train, X_test, y_train, y_test = train_test_split(gendata.X, gendata.y, test_size=0.20, random_state=42)

#Réalisation du Classifieur 

'''
def classifieur(X_train,y_train,X_test,y_test ):

    print("création du modèle... \n")

    model = keras.Sequential([
    keras.layers.Flatten(input_shape=(150,5)),
    keras.layers.Dense(128, activation=tf.nn.relu),
    keras.layers.Dense(10, activation=tf.nn.softmax)
    ])


    model.summary()
    print("Modèle terminé... \n")


    model.compile(optimizer=tf.train.AdamOptimizer(),
              loss='binary_crossentropy',
              metrics=['accuracy'])

    # x_val = X_train[50000:]
    # partial_x_train = X_train[:50000]

    # y_val = y_train[50000:]
    # partial_y_train = y_train[:50000]


    #train the model
    print("Entrainement du modèle... \n")
    # history = model.fit(partial_x_train,
    #                 partial_y_train,
    #                 epochs=40,
    #                 batch_size=512,
    #                 validation_data=(x_val, y_val),
    #                 verbose=1)

    model.compile(optimizer=tf.train.AdamOptimizer(), 
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

    # results = model.evaluate(X_test, y_test)
    model.fit(X_train, y_train, epochs=5)
    test_loss, test_acc = model.evaluate(X_test, y_test)

    print('Test accuracy:', test_acc)


classifieur(X_train[:150], X_test[:150], y_train[:150], y_test[:150])
'''